=== MP Stacks + Forms ===
Contributors: johnstonphilip
Donate link: http://mintplugins.com/
Tags: message bar, header
Requires at least: 3.5
Tested up to: 3.8.1
Stable tag: 1.0.0.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Put a completely customized form on any page. The form can be set up as a contact email form, or even be used to create WordPress posts for front-end user submission.

== Description ==

Put a completely customized form on any page. The form can be set up as a contact email form, or even be used to create WordPress posts for front-end user submission.

== Installation ==

This section describes how to install the plugin and get it working.

1. Upload the 'mp-stacks-forms’ folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Build Bricks under the “Stacks and Bricks” menu. 
4. Publish your bricks into a “Stack”.
5. Put Stacks on pages using the shortcode or the “Add Stack” button.

== Frequently Asked Questions ==

See full instructions at http://mintplugins.com/doc/mp-stacks

== Screenshots ==


== Changelog ==

= 1.0.0.3 = May 1, 2015
* Proper user IP and error checking for array

= 1.0.0.2 = March 9, 2015
* Activation hook installation function added

= 1.0.0.1 = March 9, 2015
* Added security options for max submissions per day and  submission delay.
* Removed unneeded email field as it moved to a repeater

= 1.0.0.0 = March 8, 2015
* Original release
