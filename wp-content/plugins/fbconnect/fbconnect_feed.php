<?php
    global $fbconnect_page; 
    global $fbconnect_filter;
	global $maxcomments;
	global $showPostTitle;
	global $fbconnect_cachefeed;
	
if (!function_exists('fbconnect_rrmdir')) {
	function fbconnect_rrmdir($dir,$onlycontent = true) {
	    foreach(glob($dir . '/*') as $file) {
	        if(is_dir($file))
	            fbconnect_rrmdir($file,false);
	        else
	            unlink($file);
	    }
		if (!$onlycontent){
	    	rmdir($dir);
		}
	}
	
    function fbconnect_lastfiledir($directory, $recursive = false, $listDirs = false, $listFiles = true, $exclude = '') {
        $arrayItems = array();
        $skipByExclude = false;
		if (file_exists($directory)){
	        $handle = opendir($directory);
			$lastmod = 0;
			$lastfilepath = "";
	        if ($handle) {
	            while (false !== ($file = readdir($handle))) {
	            preg_match("/(^(([\.]){1,2})$|(\.(svn|git|md|tmp))|(Thumbs\.db|\.DS_STORE))$/iu", $file, $skip);
	            if($exclude){
	                preg_match($exclude, $file, $skipByExclude);
	            }
	            if (!$skip && !$skipByExclude) {
	                if (is_dir($directory. DIRECTORY_SEPARATOR . $file)) {
	                    if($recursive) {
	                        $arrayItems = fbconnect_lastfiledir($directory. DIRECTORY_SEPARATOR . $file, $recursive, $listDirs, $listFiles, $exclude);
							if ($lastmod < $arrayItems["lastmod"]){
								$lastmod = $arrayItems["lastmod"];
								$lastfilepath = $arrayItems["lastfilepath"];
								$lastfilename = $arrayItems["lastfilename"];
							}
	                    }
	                    if($listDirs){
	                        $file = $directory . DIRECTORY_SEPARATOR . $file;
	                        $arrayItems[] = $file;
	                    }
	                } else {
	                    if($listFiles){
	                        $file2 = $directory . DIRECTORY_SEPARATOR . $file;
							$lastfilemod = filemtime($file2);
							if ($lastmod < $lastfilemod){
								$lastmod = $lastfilemod;
								$lastfilepath = $file2;
								$lastfilename = $file;
							}
	                    }
	                }
	            }
	        }
	        closedir($handle);
	        }
			$arrayItems["lastmod"] = $lastmod;
			$arrayItems["lastfilemod"] = $lastfilepath;
			$arrayItems["lastfilename"] = $lastfilename;
	        return $arrayItems;
	       }else{
	        return "";	
	       }
    }
 }


		
	if (isset($_REQUEST["fbconnect_readfeed"]) ){
		$postid = "0";
		if (isset($_REQUEST["postid"])){
			$postid = $_REQUEST["postid"];
		}
		$pathdir = "../../feedcache/".$postid;
		$filearray = fbconnect_lastfiledir($pathdir);
		$file = $filearray["lastfilemod"];
		if ($file!=""){
			if (isset($_REQUEST["fbconnect_feedid"]) && $_REQUEST["fbconnect_feedid"]!="" && $filearray["lastfilename"]==$_REQUEST["fbconnect_feedid"]){
				exit;
			}
			echo '<span style="display:none;">'. $filearray["lastfilename"].'</span>';
			echo "<script>fbconnect_feedid='".$filearray["lastfilename"]."';</script>";
			$feed = file_get_contents($file);
			echo $feed;
			exit;
		}else{
			echo "File not found ".$pathdir;
			$fbconnect_cachefeed = true;
		}
		//exit;
	}

	if (! defined('WP_CONTENT_DIR')){
		require_once("../../../wp-config.php");
	}
	
	if (isset($_REQUEST["fbconnect_cleancachefeed"])){
		$pathdir = WP_CONTENT_DIR."/feedcache";
		fbconnect_rrmdir($pathdir);
		echo "Cache cleaned";
		exit;
	}

	
	if (isset($_REQUEST["fbconnect_page"])){
		$fbconnect_page = $_REQUEST["fbconnect_page"];
	}
	
	if (isset($_REQUEST["fbconnect_cachefeed"])){
		$fbconnect_cachefeed = true;
	}
	
	if ($fbconnect_cachefeed){
		ob_start();
	}
	
	if (!isset($maxcomments)){
		$maxcomments=5;
	}
	global $fbconnect_avatarsize;
	if (!isset($fbconnect_avatarsize) || $fbconnect_avatarsize==""){
		$fbconnect_avatarsize=30;
	}
	global $fbconnect_showusername;
	if (!isset($fbconnect_showusername)){
		$fbconnect_showusername = true;
	}		
	if (!isset($fbconnect_page))
		$fbconnect_page =0;
	
	global $comment_post_ID;

	if ($fbconnect_filter=="fbAllComments" || $fbconnect_filter=="fbAllFriendsComments"){
		$comment_post_ID="";
	}else if (!isset($comment_post_ID) && isset($_REQUEST["postid"])){
		$comment_post_ID = $_REQUEST["postid"];
	}else if (!isset($comment_post_ID)){
		$comment_post_ID = WPfbConnect_Logic::get_status_postid();
		if ($comment_post_ID=="")
			$comment_post_ID=fb_get_option('fb_wall_page');	//<----- LEER DE CONFIGURACIÓN
	}
	
		
	$user = wp_get_current_user();
	
	if ($fbconnect_filter=="fbFriendsComments" || $fbconnect_filter=="fbAllFriendsComments"){
		$count = WPfbConnect_Logic::count_post_friends_comments($user->ID,$comment_post_ID);
	}else{ 
		$count = WPfbConnect_Logic::count_post_comments($comment_post_ID);
	}
	
	if ($count > $maxcomments){
			$nav=1;
			$pages=ceil($count/$maxcomments)-1;
	}else {
		$nav=0;
		$pages=0;
	}
	

	$limit=$maxcomments*$fbconnect_page;
	$cursorposition = $limit+$maxcomments;
	if ($cursorposition > $count){
		$cursorposition = $count;
	}
	//echo '<div id="commentscount">{'.$cursorposition.'/'.$count.' comments}</div>';	
	if ($fbconnect_filter=="fbFriendsComments" || $fbconnect_filter=="fbAllFriendsComments"){
		$comments = WPfbConnect_Logic::get_post_friends_comments($user->ID,$maxcomments,$comment_post_ID,$limit);
	}else{
		$comments = WPfbConnect_Logic::get_post_comments($maxcomments,$comment_post_ID,$limit);
	}
	/* This variable is for alternating comment background */
	$oddcomment = 'class="fbalt" ';

	$fbconnect_nextpage = 0;
    if ($fbconnect_page < $pages){ 
	 $fbconnect_nextpage = $fbconnect_page+1;
	}
?>

<?php if ($comments) : ?>
	<ol style="list-style: none !important;margin:0px;">
	<?php global $comment;?>
	<?php foreach ($comments as $comment) : ?>

		<li style="list-style: none !important;" <?php echo $oddcomment; ?> id="comment-<?php comment_ID() ?>">
			<div class="fb_userpic">
			<?php
			echo get_avatar( $comment, $fbconnect_avatarsize ); ?>
			</div>
			
			<?php if ($fbconnect_showusername){
			?>
			<cite class="fbauthorwidget"><?php _e('By', 'fbconnect'); ?> <?php comment_author_link() ?></cite>
			<br/>
			<?php } ?>
			
			<small class="commentmetadata">
				<?php comment_date('d/m/Y') ?> <?php comment_time() ?>
			</small>
			<span class="commenttitle">
			<?php if ($showPostTitle && ($fbconnect_filter=="fbAllFriendsComments" || $fbconnect_filter=="fbAllComments")){ ?>
			<b><a href="<?php echo get_permalink($comment->comment_post_ID) ?>" rel="bookmark" title="Permanent Link to <?php echo $comment->post_title;?>"><?php echo $comment->post_title;?></a></b>
			<br/>
			<?php } ?>
			</span>
			<span class="commentbody">
			<?php 
			comment_text();
			?>
			</span>
			<?php
			if (!isset($_REQUEST["fbconnect_cachefeed"]) && !$fbconnect_cachefeed && current_user_can('manage_options')){
			?>
			<a href="#" style="float:right;" onclick='cargaCapaAjax("<?php echo FBCONNECT_PLUGIN_URL."/fbconnect_ajax.php?fbconnect_show_feed=true&postid=".$comment_post_ID."&trash_status=".$comment->comment_ID; ?>","",false,"#commentsfeed","","","","",false);'>delete comment</a>
			<?php } ?>
		</li>

	<?php
		/* Changes every other comment to a different class */
		$oddcomment = ( $oddcomment != 'class="fbalt" ' ) ? 'class="fbalt" ' : 'class="fbalt2" ';
	?>

	<?php endforeach; /* end for each comment */ ?>

	</ol>
<?php endif; ?>
<?php
echo '<input type="hidden" name="fbconnect_pageleft" id="fbconnect_pageleft" value="'.(($fbconnect_page > 0) ? ($fbconnect_page-1) : $fbconnect_page).'" />';
echo '<input type="hidden" name="fbconnect_pageright" id="fbconnect_pageright" value="'.(($fbconnect_page < $pages) ? ($fbconnect_page+1) : 0 ).'" />';

	$pathdir = WP_CONTENT_DIR."/feedcache/".$comment_post_ID;

	$filearray = fbconnect_lastfiledir($pathdir);
	$fbconnect_cachefilename = "";
	if($filearray!="" ){		
		$file = $filearray["lastfilemod"];
		$fbconnect_cachefilename = $filearray["lastfilename"];
		$fbconnect_new_comments = 'false';
		if (isset($_REQUEST["fbconnect_feedid"]) && $_REQUEST["fbconnect_feedid"]!="" && $filearray["lastfilename"]!=$_REQUEST["fbconnect_feedid"]){
			$fbconnect_new_comments = 'true';
		}
		if (!$fbconnect_cachefeed){?>
			<span style="display:none;">'. <?php echo $filearray["lastfilename"];?>.'</span>
			<script>
			fbconnect_feedid='<?php echo $filearray["lastfilename"];?>';
			fbconnect_new_comments= <?php echo $fbconnect_new_comments;?>;
			</script>
			<?php
		}
	}
	
	if ($fbconnect_cachefeed){
		$bufferoutput=ob_get_contents();
		$subdir = $comment_post_ID;
		if ($subdir ==""){
			$subdir = "0";
		}
		$pathdir = WP_CONTENT_DIR."/feedcache/".$subdir;
		//fbconnect_rrmdir($pathdir);
		if (!is_dir($pathdir)) {
			mkdir($pathdir);
		}
		$filepath = $pathdir."/feed_".date('U')."_".rand(0,9999);
		file_put_contents($filepath.".tmp", $bufferoutput);
  		rename($filepath.".tmp",$filepath.".txt");
		ob_end_clean();
	} 
	

?>