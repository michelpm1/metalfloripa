<?php
 class RecommendationsBar extends WP_Widget {
 	function RecommendationsBar() 
 	{
		$widget_ops = array('description' => __('Faceboook Recommendations Bar', 'fbconnect'));
		$this->WP_Widget('RecommendationsBar', __('Recommendations Bar'), $widget_ops);
 		
 	}

 	function widget($args, $instance) {
		$title = esc_attr($instance['title']);		
		$trigger = esc_attr($instance['trigger']);	
		$read_time = esc_attr($instance['read_time']);	
		$side = esc_attr($instance['side']);	
		$site = esc_attr($instance['site']);	
		$num_recommendations = intval($instance['num_recommendations']);
		$max_age = intval($instance['max_age']);
		$action = esc_attr($instance['action']);	
	
		extract($args);
		
		if (!$num_recommendations){
			$num_recommendations = 2;
		}
	
		echo '<fb:recommendations-bar side="'.$side.'" action="'.$action.'" max_age="'.$max_age.'" read_time="'.$read_time.'" trigger="'.$trigger.'" side="'.$side.'" site="'.$site.'"></fb:recommendations-bar>';
		
		
 		
 	}
 	
	// When Widget Control Form Is Posted
	function update($new_instance, $old_instance) {
		if (!isset($new_instance['submit'])) {
			return false;
		}
		
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);		
		$instance['trigger'] = esc_attr($new_instance['trigger']);	
		$instance['read_time'] = esc_attr($new_instance['read_time']);	
		$instance['side'] = esc_attr($new_instance['side']);	
		$instance['site'] = esc_attr($new_instance['site']);	
		$instance['action'] = esc_attr($new_instance['action']);	
		$instance['num_recommendations'] = intval($new_instance['num_recommendations']);
		$instance['max_age'] = intval($new_instance['max_age']);
		
		return $instance;
	}
 	
 	// DIsplay Widget Control Form
	function form($instance) {
		global $wpdb;
		$instance = wp_parse_args((array) $instance);
		$title = esc_attr($instance['title']);		
		$trigger  = esc_attr($instance['trigger']);	
		$read_time = esc_attr($instance['read_time']);	
		$side = esc_attr($instance['side']);	
		$site = esc_attr($instance['site']);	
		$action = esc_attr($instance['action']);	
		$num_recommendations = intval($instance['num_recommendations']);
		$max_age = intval($instance['max_age']);
		
		if ($trigger==""){
			$trigger = "50%";
		}
		if ($read_time==""){
			$read_time="30";
		}
		if ($action==""){
			$action="like";
		}
		if ($side==""){
			$side="right";
		}
		if($max_age=="" || $max_age==0){
			$max_age = 180;
		}
		if ($site==""){
			$site=str_replace("http://","",fb_get_option('siteurl'));
		}
		if (!isset($num_recommendations) || $num_recommendations =="")
			$num_recommendations  = 2;
			
		
		?>

		
	 	<label class="fblabelform" for="<?php echo $this->get_field_id('site'); ?>"><?php _e('Site:', 'fbconnect'); ?>
				<input class="widefat" id="<?php echo $this->get_field_id('site'); ?>" name="<?php echo $this->get_field_name('site'); ?>" type="text" value="<?php echo $site; ?>" />
		</label>	 
		<label class="fblabelform" for="<?php echo $this->get_field_id('trigger'); ?>"><?php _e('Trigger (onvisible, X%):', 'fbconnect'); ?>
				<input class="widefat" id="<?php echo $this->get_field_id('trigger'); ?>" name="<?php echo $this->get_field_name('trigger'); ?>" type="text" value="<?php echo $trigger; ?>" />
		</label>
		<label class="fblabelform" for="<?php echo $this->get_field_id('read_time'); ?>"><?php _e('Read time (seconds):', 'fbconnect'); ?>
				<input class="widefat" id="<?php echo $this->get_field_id('read_time'); ?>" name="<?php echo $this->get_field_name('read_time'); ?>" type="text" value="<?php echo $read_time; ?>" />
		</label>
		<label class="fblabelform" for="<?php echo $this->get_field_id('side'); ?>"><?php _e('Side of the screen:', 'fbconnect'); ?>
			<SELECT style="width: 100%;" id="<?php echo $this->get_field_id('side');?>" name="<?php echo $this->get_field_name('side');?>">
				<?php 
				echo '<OPTION ';
				if ($side=="left") echo "SELECTED";
				echo ' VALUE="left">Left side</OPTION>';
				echo ' <OPTION ';
				echo '<OPTION ';
				if ($side=="right") echo "SELECTED";
				echo ' VALUE="right">Right side</OPTION>';
				?>
			</SELECT>
		</label>
		<label class="fblabelform" for="<?php echo $this->get_field_id('action'); ?>"><?php _e('Action:', 'fbconnect'); ?>
			<SELECT style="width: 100%;" id="<?php echo $this->get_field_id('action');?>" name="<?php echo $this->get_field_name('action');?>">
			<?php 
			echo '<OPTION ';
			if ($action=="like") echo "SELECTED";
			echo ' VALUE="like">Like</OPTION>';
			echo ' <OPTION ';
			echo '<OPTION ';
			if ($action=="recommend") echo "SELECTED";
			echo ' VALUE="recommend">Recommend</OPTION>';
			?>
			</SELECT>
		</label>
		<label class="fblabelform" for="<?php echo $this->get_field_id('num_recommendations'); ?>"><?php _e('Number of recomendations:', 'fbconnect'); ?>
			<input class="widefat" id="<?php echo $this->get_field_id('num_recommendations'); ?>" name="<?php echo $this->get_field_name('num_recommendations'); ?>" type="text" value="<?php echo $num_recommendations;?>" />
		</label>
		<label class="fblabelform" for="<?php echo $this->get_field_id('max_age'); ?>"><?php _e('Max age (1-180 days):', 'fbconnect'); ?>
			<input class="widefat" id="<?php echo $this->get_field_id('max_age'); ?>" name="<?php echo $this->get_field_name('max_age'); ?>" type="text" value="<?php echo $max_age;?>" />
		</label>
		<input type="hidden" id="<?php echo $this->get_field_id('submit'); ?>" name="<?php echo $this->get_field_name('submit'); ?>" value="1" />
	<?php
	}	
	
 }

add_action('widgets_init', 'RecommendationsBar_init');
function RecommendationsBar_init() {
	register_widget('RecommendationsBar');
}
 
?>