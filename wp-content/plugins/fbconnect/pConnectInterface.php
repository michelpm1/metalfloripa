<?php
/**
 * @author: Javier Reyes Gomez (http://www.sociable.es)
 * @date: 05/10/2008
 * @license: GPLv2
 */

		global $wp_version, $fbconnect,$fb_reg_formfields;

			if ( isset($_POST['info_update']) ) {
				check_admin_referer('wp-fbconnect-info_update');

				$error = '';
				update_option( 'p_login_enabled', $_POST['p_login_enabled'] );
				update_option( 'p_api_key', $_POST['p_api_key'] );
				update_option( 'p_appId', $_POST['p_appId'] );
				update_option( 'p_api_secret', $_POST['p_api_secret'] );
				
				if ($error !== '') {
					echo '<div class="error"><p><strong>'.__('At least one of PayPal Connector options was NOT updated', 'fbconnect').'</strong>'.$error.'</p></div>';
				} else {
					echo '<div class="updated"><p><strong>'.__('PayPal Connector options updated', 'fbconnect').'</strong></p></div>';
				}

			
			}
			
			// Display the options page form
			$siteurl = fb_get_option('home');
			if( substr( $siteurl, -1, 1 ) !== '/' ) $siteurl .= '/';
			?>
			<div class="wrap">
				<h2>
					<img src="<?php echo FBCONNECT_PLUGIN_URL;?>/images/paypal-20.png"/>
					<?php _e('PayPal Configuration', 'fbconnect') ?></h2>

				<form method="post">


					<h3><?php _e('PayPal Application Configuration', 'fbconnect') ?></h3>
     				<table class="form-table" cellspacing="2" cellpadding="5" width="100%">
     					<tr valign="top">
							<th style="width: 33%" scope="row"><?php _e('Enable PayPal login:', 'fbconnect') ?></th>
							<td>
								<p><input type="checkbox" name="p_login_enabled" id="p_login_enabled" <?php
								if( fb_get_option('p_login_enabled')) echo 'checked="checked"'
								?> />
							</td>
						</tr>
						<tr valign="top">
							<th style="width: 33%" scope="row"><?php _e('PayPal App. Config.', 'fbconnect') ?></th>
							<td>
							<a href="https://developer.paypal.com/webapps/developer/applications/myapps" target="_blank"><?php _e('Create a new PayPal Application', 'fbconnect') ?></a><br/>
							<br/><?php _e('Your PayPal return URL (copy and paste in the PayPal Application configuration):', 'fbconnect') ?>
							<br/><input type="text" name="g_callback" id="g_callback" size="50" value="<?php echo WPpConnect_Logic::getRedirectUrl();?>"/>
							</td>
						</tr>

						<tr valign="top">
							<th style="width: 33%" scope="row"><label for="p_appId"><?php _e('PayPal client ID:', 'fbconnect') ?></label></th>
							<td>
							<input type="text" name="p_appId" id="p_appId" size="50" value="<?php echo fb_get_option('p_appId');?>"/>
							</td>
						</tr>
						<tr valign="top">
							<th style="width: 33%" scope="row"><label for="p_api_secret"><?php _e('PayPal client secret:', 'fbconnect') ?></label></th>
							<td>
							<input type="text" name="p_api_secret" size="50" id="p_api_secret" value="<?php echo fb_get_option('p_api_secret');?>"/>
							</td>
						</tr>
						<tr valign="top" >
							<th style="width: 33%" scope="row"><label for="p_api_key"><?php _e('Developer API Key:', 'fbconnect') ?></label></th>
							<td>
							<input type="text" name="p_api_key" id="p_api_key" size="50" value="<?php echo fb_get_option('p_api_key');?>"/>
							</td>
						</tr>							

     				</table>

					
					<?php wp_nonce_field('wp-fbconnect-info_update'); ?>
					
     				<p class="submit"><input class="button-primary" type="submit" name="info_update" value="<?php _e('Update Configuration', 'fbconnect') ?> &raquo;" /></p>
     			</form>
				
			</div>
    			