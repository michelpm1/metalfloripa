<?php

if (! defined('FB_CANVAS_URL')){
	$canvasUrl= fb_get_option('fb_canvas_url');
	$canvasUrl = sslFBConnectFilter($canvasUrl); 
    define('FB_CANVAS_URL',$canvasUrl);
}

if (isset($_REQUEST["fb_facebookapp_mode"])){
	
	$_SESSION["fb_facebookapp_mode"]= $_REQUEST["fb_facebookapp_mode"];
}
if (isset($_REQUEST["fburef"])){
	$_SESSION["fburef"] = $_REQUEST["fburef"];
}
//print_r($_SESSION);
global $oldsiteurl;
global $oldhomeurl;
$oldsiteurl = fb_get_option('siteurl');
$oldhomeurl = fb_get_option('home');


if (isset($_REQUEST["signed_request"])){
	list($encoded_sig, $payload) = explode('.', $_REQUEST["signed_request"], 2);
    $signed_request = json_decode(base64_decode(strtr($payload, '-_', '+/')), true);
	//$signed_request = fb_getSignedRequest();

	if (isset($signed_request["app_data"])){
		global $fb_signed_request_appdata;
		$fb_signed_request_appdata = json_decode($signed_request["app_data"]);
	}
	$_SESSION["fbsignedrequest"] = $signed_request;	
}
//if(isset($_REQUEST["smpchanneltype"]) && $_REQUEST["smpchanneltype"]!="" && isset($_REQUEST["fbtabpage"])) {
if(isset($_REQUEST["smpchanneltype"]) && $_REQUEST["smpchanneltype"]!="tab" && $_REQUEST["smpchanneltype"]!="appcanvas") {
	define ('FBCONNECT_CANVAS', $_REQUEST["smpchanneltype"]);

	/*if(fb_get_option('fb_tab_home')==""){
		add_filter('option_show_on_front','fbChangeCanvasFront');
	}*/
	if(fb_get_option('fb_tab_home')!="" || fb_get_option('fb_tab_nofan_home')!=""){
		add_filter('posts_where', 'fb_ChangeCanvasFrontPostID' );
		add_action('pre_get_posts','fb_pre_get_posts');
	}
	
//}elseif ((isset($_SESSION["fb_facebookapp_mode"]) && $_SESSION["fb_facebookapp_mode"]=="on") || isset($_REQUEST["fb_sig_in_canvas"])){
}else if(isset($_REQUEST["fb_sig_in_profile_tab"]) || (isset($signed_request) && isset($signed_request["page"])) || (isset($signed_request) && isset($_REQUEST["fbtabpage"]) && $_REQUEST["fbtabpage"]!="")) {
	define ('FBCONNECT_CANVAS', "tab");
	global $pagetabinfo;
	global $fb_isnewfollower;
	$fb_isnewfollower = 0;
	
	//print_r($signed_request);
	if (isset($signed_request["page"]) &&  $signed_request["page"]!=""){
		$pagetabinfo = $signed_request["page"];
	}elseif(isset($_REQUEST["fbtabpage"]) && $_REQUEST["fbtabpage"]!=""){
		$pagetabinfo["id"] = $_REQUEST["fbtabpage"];
		$pagetabinfo["liked"] = 0;
	}
	
	if (isset($_REQUEST["isFbFan"]) && ($_REQUEST["isFbFan"]=="true" || $_REQUEST["isFbFan"]=="1")){
			$pagetabinfo["liked"] = 1;
	}
			
	if ($_SESSION["pagetabinfo"]!=""){

		$idpage = $pagetabinfo["id"];
		
		if ($pagetabinfo["liked"]==1 && isset($_SESSION["pagetabinfo"][$idpage]) && $_SESSION["pagetabinfo"][$idpage]["liked"]!= 1){ //Es un nuevo fan
			$fb_isnewfollower = 1;
		}else{
			$fb_isnewfollower = 0;
		}
	}

	$_SESSION["pagetabinfo"][$idpage] = $pagetabinfo;
	
	if (isset($_REQUEST["fb_sig_in_profile_tab"])){
		add_filter('pre_option_siteurl','changeCanvasSiteURL');
		add_filter('pre_option_home','changeCanvasSiteURL');

	}

	if(fb_get_option('fb_tab_home')==""){
		add_filter('option_show_on_front','fbChangeCanvasFront');
	}
	if(fb_get_option('fb_tab_home')!="" || fb_get_option('fb_tab_nofan_home')!=""){
		add_filter('posts_where', 'fb_ChangeCanvasFrontPostID' );
		add_action('pre_get_posts','fb_pre_get_posts');
	}
	
}elseif(isset($_REQUEST["smpchanneltype"]) && $_REQUEST["smpchanneltype"]=="web"){
	define ('FBCONNECT_CANVAS', "web");
}elseif ( isset($_REQUEST["signed_request"]) || (isset($_REQUEST["fb_app_mode"]) && $_REQUEST["fb_app_mode"]=="on") || (isset($_SESSION["fb_facebookapp_mode"]) && $_SESSION["fb_facebookapp_mode"]=="on" )){
		
	define ('FBCONNECT_CANVAS', "appcanvas");

	if (isset($_REQUEST["fb_sig_in_canvas"]) && (!isset($_SESSION["fb_facebookapp_mode"]) || $_SESSION["fb_facebookapp_mode"]=="off")){
		add_filter('pre_option_siteurl','changeCanvasSiteURL');
		add_filter('pre_option_home','changeCanvasSiteURL');
		//add_action( 'set_auth_cookie', array( 'WPfbConnect_Logic', 'fb_set_auth_cookie' ),100,5 ); 
		//add_action( 'set_logged_in_cookie', array( 'WPfbConnect_Logic', 'fb_set_logged_in_cookie' ),100,5 ); 

	}

	$auth_url = fb_getLoginUrl();
	/*if( fb_get_option('fb_canvas_redirect') ){
		fb_redirect_canvas();
	}*/
	//OLD SDK
	if (isset($_REQUEST["fb_sig_in_canvas"])){
	 	fb_require_login(fb_get_option('fb_permsToRequestOnConnect'));
	}
	
	/* Da problemas hacer wp_get_current_user dentro de fb_get_loggedin_user, porque no se ha terminado de cargar wp
	 * $fb_user = fb_get_loggedin_user();
	
	if (fb_get_option('fb_canvas_request_auth') && isset($_REQUEST["signed_request"]) && ($fb_user==null || $fb_user=="")){
		echo("<script> top.location.href='" . $auth_url . "'</script>");
		exit;
	}*/
	
	if(fb_get_option('fb_canvas_home')==""){
		add_filter('option_show_on_front','fbChangeCanvasFront');
	}else{
		add_filter('posts_where', 'fb_ChangeCanvasFrontPostID' );
		add_action('pre_get_posts','fb_pre_get_posts');
	}
	
}else{
	define ('FBCONNECT_CANVAS', "web");
	
	/*$adminnologed = WP_ADMIN==1; 
	if (isset($_REQUEST["fbconnect_noredirect"]) || (isset($_REQUEST["fbconnect_action"]) && $_REQUEST["fbconnect_action"]=="fbconnect_redirect")){
		//No redirect
	}else{
		if(fb_get_option('fb_canvas_redirect')=="nosite" && strrpos($_SERVER['PHP_SELF'],"wp-login")===false && !$adminnologed ){
			print_r($_SERVER);	
			fb_redirect_canvas();
		}
	}*/
}


if(!function_exists('fb_ChangeCanvasFrontPostID')):
	function fb_ChangeCanvasFrontPostID($where){
		global $fbhome;
		if ($fbhome!="" && FBCONNECT_CANVAS!="web"){
				return "AND wp_posts.ID =".$fbhome;
		}
		return $where;
	}
endif;

global $fbhome;


function fb_redirect_canvas($posts){
	global $pagenow,
	$is_lynx, $is_gecko, $is_winIE, $is_macIE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone, $is_IE,
	$is_apache, $is_IIS, $is_iis7;
	
	$adminnologed = (WP_ADMIN==1 || current_user_can('manage_options')); 

	if(!isset($_REQUEST["fbconnect_action"]) && (is_home() || is_single())){
		if (!$is_lynx && !$is_gecko && !$is_winIE && !$is_macIE && !$is_opera && !$is_NS4 && !$is_safari && !$is_chrome && !$is_iphone && !$is_IE){
			//No edirect
		}else if (FBCONNECT_CANVAS=="web" && (wp_is_mobile() || $adminnologed)){
			//No redirect
		}else if ( FBCONNECT_CANVAS=="tab" || FBCONNECT_CANVAS=="widget" || (isset($_REQUEST["fbconnect_action"]) && $_REQUEST["fbconnect_action"]=="fbconnect_redirect")){
			//No redirect
		}else{

			if( fb_get_option('fb_canvas_redirect')=="nosite" || fb_get_option('fb_canvas_redirect')=="tab" ){
				if ( !is_home() && $posts!="" && count($posts)>0) {

					$post = $posts[0];
			
					if ($postid==""){
						$postid = $post->ID;
					}

					if (FBCONNECT_CANVAS=="appcanvas" && wp_is_mobile() ){
						$redirect = get_permalink($postid);	
					}else{
						$page_url = get_post_meta($postid, 'smp_fbconnect_access_page_url', true);
						if ($page_url==""){
							$page_url = get_post_meta($postid, '_fbconnect_access_page_url', true);
						}
						if ($page_url==""){
							$page_url = fb_get_option("fb_fanpage_url");	
						}
						
						$appid = get_post_meta($postid, 'workspace_appid', true);
						if ($appid==""){
							$appid = fb_get_option("fb_appId");
						}
						if ($page_url!=""){
							$redirect = WPfbConnect_Logic::add_urlParam($page_url,"sk=app_".$appid);
						}
						if (count($posts)==1){
							$params = array('id' => $postid);						
							$encodedParams = urlencode(json_encode($params));
							$redirect = WPfbConnect_Logic::add_urlParam($redirect,"app_data",$encodedParams);
						}
					}
				}else if ( $posts!="" && count($posts)>0) {
					if (FBCONNECT_CANVAS=="appcanvas" && wp_is_mobile() ){
						$redirect = get_site_url();	
					}else{
						$appid = fb_get_option("fb_appId");
						$page_url = fb_get_option("fb_fanpage_url");
						if ($page_url!=""){
							$redirect = WPfbConnect_Logic::add_urlParam($page_url,"sk=app_".$appid);
						}else{
							$page_url =="";
						}
					}
				}
			
				if ($redirect!=""){
					if ($_SERVER["QUERY_STRING"]!=""){
						$redirect = $redirect."&".$_SERVER["QUERY_STRING"];
					}
				 //header('Location: '.$redirect);
	
				?>
				<script>
				top.location = "<?php echo $redirect;?>";
				</script>
				<?php 
				exit;
				}else{
					echo "<!-- Redirección del canvas no configurada -->";
					exit;
				}
			}elseif(fb_get_option('fb_canvas_redirect')=="site" ){
				global $oldsiteurl;
				//header('Location: '.$oldsiteurl);
				?>
				<script>
				top.location = "<?php echo $oldsiteurl;?>";
				</script>
				<?php 
				exit;
			}
		}
		}
	return $posts;
}
add_filter( 'the_posts', fb_redirect_canvas, 1 );

/**
* disable feed
*/
function fb_disable_feed() {
wp_die( __('No feed available,please visit our <a href="'. get_bloginfo('url') .'">homepage</a>!') );
exit;
}

if(fb_get_option('fb_disable_feed')!="" ){
	add_action('do_feed', 'fb_disable_feed', 1);
	add_action('do_feed_rdf', 'fb_disable_feed', 1);
	add_action('do_feed_rss', 'fb_disable_feed', 1);
	add_action('do_feed_rss2', 'fb_disable_feed', 1);
	add_action('do_feed_atom', 'fb_disable_feed', 1);
	remove_action( 'wp_head', 'feed_links', 2 ); 
	remove_action('wp_head','feed_links_extra', 3);
}	
	
if(!function_exists('fb_pre_get_posts')):
function fb_pre_get_posts($query){
	global $pagetabinfo;
	global $fbhome;
	global $post;

	if (isset($query) && $query->query_vars["post_type"]!="nav_menu_item" ){
		if( FBCONNECT_CANVAS=="tab" && (!isset($pagetabinfo["liked"]) || !$pagetabinfo["liked"] || $pagetabinfo["id"]!=fb_get_option('fb_fanpage_id') )  && fb_get_option('fb_tab_nofan_home')!="" && fb_get_option('fb_nofans_thick')==""){
	
			if ($fbhome==""){
				$fbhome = fb_get_option('fb_tab_nofan_home');
			}
			$query->query_vars["post_type"] = "page";
			//$query->is_single = '1';
			$query->is_page = '1';
			$query->is_singular = '1';
			$query->is_home ='';
		}elseif(isset($query) && FBCONNECT_CANVAS=="tab" && (!isset($pagetabinfo["liked"]) || !$pagetabinfo["liked"])  && fb_get_option('fb_tab_nofan_home')!="" && fb_get_option('fb_nofans_thick')==""){
	
			if ($fbhome==""){
				$fbhome = fb_get_option('fb_tab_nofan_home');
			}
			$query->query_vars["post_type"] = "page";
			//$query->is_single = '1';
			$query->is_page = '1';
			$query->is_singular = '1';
			$query->is_home ='';
		}elseif($query->is_home && isset($query) && FBCONNECT_CANVAS=="tab" && fb_get_option('fb_tab_home')!=""){
			
			if ($fbhome==""){
				$fbhome = fb_get_option('fb_tab_home');
			}	
			$query->query_vars["post_type"] = "page";
			//$query->is_single = '1';
			$query->is_page = '1';
			$query->is_singular = '1';
			$query->is_home ='';
		}elseif($query->is_home && isset($query) && FBCONNECT_CANVAS=="appcanvas" && fb_get_option('fb_tab_home')!=""){
	
			if ($fbhome==""){
				$fbhome = fb_get_option('fb_canvas_home');
			}
			$query->query_vars["post_type"] = "page";
			//$query->is_single = '1';
			$query->is_page = '1';
			$query->is_singular = '1';
			$query->is_home ='';
		}
	}
	return $query;
}
endif;
/*
if(!function_exists('fbaddfacebooktags') && fb_get_option('fb_tags_enabled')):
	function fbaddfacebooktags(){
		register_taxonomy( 'facebooktags', array('post','page'), array( 'hierarchical' => false, 'label' => 'Facebook tags', 'query_var' => true, 'rewrite' => true ) );
	}

	add_action( 'init', fbaddfacebooktags,100 ); 
endif;	
*/
//if (  (isset($_GET['login_mode']) && $_GET['login_mode']=="themeform") || FBCONNECT_CANVAS=="appcanvas"){
//	add_action( 'init', array( 'WPfbConnect_Logic', 'wp_login_fbconnect' ),100 ); 
//}

//if  ( (FBCONNECT_CANVAS=="web") && fb_get_option('fb_show_reg_form') || fb_get_option('fb_show_reg_form')!="") {

		//wp_enqueue_script('jquery');
		//wp_enqueue_script('jquery-form');
		//wp_enqueue_script("thickbox");
//	}


add_action('the_content', 'fbconnect_access_control' );

/*if(WP_ADMIN==1 && fb_get_option('fb_use_fbtheme')!=""){
		include_once WP_THEME_DIR.'/'.fb_get_option('fb_use_fbtheme').'/functions.php';
}*/

if(!function_exists('fbChangeCanvasFront')):
	function fbChangeCanvasFront($optionval){
		if (fb_get_option('fb_canvas_home')==""){
			return "posts";
		}else{
			return $optionval;
		}
	}	
endif;
/*

if(!function_exists('fbconnect_add_sidebar')):
	function fbconnect_add_sidebar($numbar=""){
		register_sidebar(array(
	        'id' => 'facebook-bar'.$numbar,
	        'name' => 'Facebook bar '.$numbar,
	        'before_widget' => '<li id="%1$s" class="widget %2$s">',
	        'after_widget' => '</li>',
	        'before_title' => '<h2 class="widgettitle">',
	        'after_title' => '</h2>',
    	));
	}
	fbconnect_add_sidebar();
	fbconnect_add_sidebar(2);
endif;
*/
	
function changeCanvasSiteURL(){
	return fb_get_option('fb_canvas_url');
}

if(!function_exists('get_fbconnect_stylesheet')):
function get_fbconnect_stylesheet($stylesheet){
	if(WP_ADMIN==1 && isset($_REQUEST["fbselectstyle"])){
		return $_REQUEST["fbselectstyle"];
	}else{
		global $post;
		global $pagetabinfo;
		global $imagen_fondo_header_headerfootertab;
		global $imagen_fondo_footer_headerfootertab;
		global $main_post_menu;
		
		$postheme = "";
		//echo "post:".$post->ID;	
		if (isset($post) && $post!=""){
			$postheme = get_post_meta($post->ID, '_fb_custom_style', true);
		}

		if (isset($_REQUEST["fbtabpage"]) && $_REQUEST["fbtabpage"]!=""){
			$pageid = $_REQUEST["fbtabpage"];
		}elseif (isset($pagetabinfo) && isset($pagetabinfo["id"])){
			$pageid = $pagetabinfo["id"];
		}
		if($postheme=="" && $pageid!=""){
			global $fbwstheme;
			if ($fbwstheme=="")
				$fbwstheme = getWorkspaceByFBPageId($pageid);

			if ($fbwstheme!=""){
				$postheme = get_post_meta($fbwstheme->postId, '_fb_custom_style', true);
				$imagen_fondo_header_headerfootertab = get_post_meta($fbwstheme->postId, 'imagen_fondo_header_headerfootertab', true);
				$imagen_fondo_footer_headerfootertab = get_post_meta($fbwstheme->postId, 'imagen_fondo_footer_headerfootertab', true);
				$main_post_menu = get_post_meta($fbwstheme->ID, "main_post_menu", true);
			}
		}

		if ($postheme!=""){
			return $postheme;
		}else if(fb_get_option('fb_use_fbtheme_style')!="" && (FBCONNECT_CANVAS!="web" || (isset($_SESSION["fb_facebookapp_mode"]) && $_SESSION["fb_facebookapp_mode"]=="on") )) {
		    return fb_get_option('fb_use_fbtheme_style');		
		} else {
			return $stylesheet;
	  	}
	}
}
endif;	
	

function getWorkspaceByFBPageId($pageId){					
	global $wpdb;

	$workspace = wp_cache_get($pageId, 'workspacespages');

	if ($workspace === false){			
		$query="SELECT * FROM sj_workspaces,".$wpdb->posts." WHERE ID=postId AND fbpageid=%s";
		//echo $query;
		$resultado = $wpdb->get_results($wpdb->prepare($query,$pageId));
		if(isset($resultado) && count($resultado)>0){
			$workspace = $resultado[0];
			wp_cache_add($workspace->postId, $workspace, 'workspaces');
			wp_cache_add($workspace->fbpageid, $workspace, 'workspacespages');
			return $resultado[0];
		}else{
			wp_cache_add($pageId, "", 'workspacespages');
		}
		return null;
	}else{
		return $workspace;
	}
		
}
	
if(!function_exists('get_fbconnect_body_class')):
function get_fbconnect_body_class($class){
	if(FBCONNECT_CANVAS=="tab") {
		$class[] = "facebooktab";
	    return $class;
	}elseif(FBCONNECT_CANVAS=="appcanvas") {	
		$class[] = "facebookcanvas";
	    return $class;
	} else {
		return $class;
  	}
}
endif;

add_filter('stylesheet', 'get_fbconnect_stylesheet');  	  
//add_filter('template', 'get_fbconnect_template');
add_filter('body_class', 'get_fbconnect_body_class');


if(!function_exists('get_fbconnect_theme')):
function get_fbconnect_theme($theme){

	if(WP_ADMIN==1 && isset($_REQUEST["fbselecttheme"])){
		//echo " select theme:".$_REQUEST["fbselecttheme"];
		return $_REQUEST["fbselecttheme"];
	}else{
		global $post;
		global $pagetabinfo;
		$postheme = "";
		
		if (isset($post) && $post!=""){
			$postheme = get_post_meta($post->ID, '_fb_custom_theme', true);
		}
		$pageid = "";
		if (isset($_REQUEST["fbtabpage"]) && $_REQUEST["fbtabpage"]!=""){
			$pageid = $_REQUEST["fbtabpage"];
		}elseif (isset($pagetabinfo) && isset($pagetabinfo["id"])){
			$pageid = $pagetabinfo["id"];
		}
		
		if($postheme=="" && $pageid!=""){
			global $fbwstheme;
			if ($fbwstheme==""){
				$fbwstheme = getWorkspaceByFBPageId($pageid);
			}
			if ($fbwstheme!=""){
				$postheme = get_post_meta($fbwstheme->postId, '_fb_custom_theme', true);
			}
		}
	
		if ($postheme!=""){
			return $postheme;
		}elseif(fb_get_option('fb_use_fbtheme')!="" && (FBCONNECT_CANVAS!="web" || (isset($_SESSION["fb_facebookapp_mode"]) && $_SESSION["fb_facebookapp_mode"]=="on")) ) {
		    return fb_get_option('fb_use_fbtheme');		
		} else {
			return $theme;
	  	}
	}
}
endif;	

//get_themes();

// No es necesario cambiar las rutas del direcotrio raiz themes
//add_filter('theme_root', 'get_fbconnect_theme_root');
//add_filter('theme_root_uri', 'get_fbconnect_theme_root_uri');

add_filter( 'template', 'get_fbconnect_theme' );	

if(!function_exists('get_fbconnect_option_stylesheet')):
function get_fbconnect_option_stylesheet($stylesheet){
	return get_fbconnect_stylesheet($stylesheet);
	/*if(fb_get_option('fb_use_fbtheme')!="" && (FBCONNECT_CANVAS!="web" || (isset($_SESSION["fb_facebookapp_mode"]) && $_SESSION["fb_facebookapp_mode"]=="on")) ) {
	    return fb_get_option('fb_use_fbtheme');		
	} else {
		return $stylesheet;
  	}*/
}
endif;
add_filter( 'option_stylesheet', 'get_fbconnect_option_stylesheet' );


if(!function_exists('get_fbconnect_option_theme')):
function get_fbconnect_option_theme($stylesheet){
	return get_fbconnect_theme($stylesheet);
	/*if(fb_get_option('fb_use_fbtheme')!="" && (FBCONNECT_CANVAS!="web" || (isset($_SESSION["fb_facebookapp_mode"]) && $_SESSION["fb_facebookapp_mode"]=="on")) ) {
	    return fb_get_option('fb_use_fbtheme_name');		
	} else {
		return $stylesheet;
  	}*/
}
endif;
add_filter( 'option_current_theme', 'get_fbconnect_option_theme' );
/*if(!function_exists('get_fbconnect_stylesheet_directory_uri')):
function get_fbconnect_stylesheet_directory_uri($stylesheet_dir_uri, $stylesheet, $theme_root_uri){
	if(fb_get_option('fb_use_fbtheme')!="" && FBCONNECT_CANVAS!="web") {
		echo "get_fbconnect_bloginfo_url ".$stylesheet_dir_uri." ". $stylesheet." ".$theme_root_uri;
	    return $output;		
	} else {
		echo "get_fbconnect_bloginfo_url2 ".$stylesheet_dir_uri." ". $stylesheet." ".$theme_root_uri;
		return $output;
  	}
}
endif;
add_filter('stylesheet_directory_uri','get_fbconnect_stylesheet_directory_uri',1,3);
*/
  

add_filter('cancel_comment_reply_link', 'fb_cancel_comment_reply_link',1,3);
if(!function_exists('fb_cancel_comment_reply_link')):
function fb_cancel_comment_reply_link($alink,$link, $text){
	if (FBCONNECT_CANVAS!="web") {
		$style = isset($_GET['replytocom']) ? '' : ' style="display:none;"';
		$newlink = '<a rel="nofollow" id="cancel-comment-reply-link" href="' .fb_get_option('fb_canvas_url').'/'. $link . '"' . $style . '>' . $text . '</a>';
		return $newlink;
	}else{
		return $alink;
	}
}
endif;	

add_filter('video_send_to_editor_url', 'set_fbconnect_video_tag',1,3);
if(!function_exists('set_fbconnect_video_tag')):
function set_fbconnect_video_tag($html, $href, $title){
		// Construct the attachment array

	if (isset($_REQUEST["post_id"]) && $_REQUEST["post_id"]!=""){
		$attachment = array(
			'post_mime_type' => 'application/x-shockwave-flash',
			'guid' => $href,
			'post_parent' => $_REQUEST["post_id"],
			'post_title' => $title,
			'post_content' => $href,
		);
	
		// Save the data
		$id = wp_insert_attachment($attachment, false, $_REQUEST["post_id"]);
		
		update_post_meta($_REQUEST["post_id"] , 'fbconnect_video_src', $href);
		update_post_meta($_REQUEST["post_id"] , 'fbconnect_video_id', $id);
		
	}
	return '[socialvideo title="'.$title.'" width="580" height="360" cwidth="520" cheight="320"]'.$href.'[/socialvideo]';
}
endif;	

if(function_exists('add_shortcode')):
	add_shortcode( 'socialvideo', 'shortcode_socialvideo' );
endif;	

function shortcode_socialvideo( $atts, $content = '' ) {
		global $post;
		$origatts = $atts;
		
		if ( is_feed() )
			return '<p><a href="' . get_permalink( $post->ID ) . '">Ver video</a></p>';

		// Set any missing $atts items to the defaults
		$atts = shortcode_atts(array(
			'title'          => 'Video',
			'width'          => '580',
			'height'         => '360',
			'cwidth'          => '520',
			'cheight'         => '330',
		), $atts);

		
		//REYES
		//$imgurl = WPfbConnect_Logic::get_post_image($post_id);
		
		if (FBCONNECT_CANVAS=="web"){
			$video_code = '<div class="vvqbox vvqvideo" style="margin-bottom:50px;width:' . $atts['width'] . 'px;height:' . $atts['height'] . 'px;">';
			//$video_code .='<fb:serverfbml style="width: '.$atts['width'].'px;"> <script type="text/fbml"> <fb:fbml>';
			//$video_code .='<fb:swf swfbgcolor="FFF"  waitforclick="false" swfsrc="'. $content .'" imgsrc="'.$imgurl.'" width="' . $atts['width'] . '" height="' . $atts['height'] . '"/>';
			$video_code .='<iframe title="'.$atts['title'].'" width="'.$atts['width'].'" height="'.$atts['height'].'" src="'.$content.'" frameborder="0" allowfullscreen></iframe>';
		}else{
			$video_code = '<div class="vvqbox vvqvideo" style="margin-bottom:50px;width:' . $atts['cwidth'] . 'px;height:' . $atts['cheight'] . 'px;">';
			$video_code .='<iframe title="'.$atts['title'].'" width="'.$atts['cwidth'].'" height="'.$atts['cheight'].'" src="'.$content.'" frameborder="0" allowfullscreen></iframe>';
		}
		
		
		
	/*	if (FBCONNECT_CANVAS=="web"){
			$video_code .='</fb:fbml></script></fb:serverfbml>';
		}*/
		$video_code .='</div>';
		return $video_code;
}

/*if(!function_exists('fb_comment_post_redirect')):
function fb_comment_post_redirect($location,$comment){
	if (FBCONNECT_CANVAS=="appcanvas") {
		echo '<fb:redirect url="'.$location.'"/>';
		exit;
	}else{
			return $location;
	}
}
endif;	

add_filter('comment_post_redirect', 'fb_comment_post_redirect',10,2);	
*/

// Access control
if(!function_exists('fbconnect_access_control')):
function fbconnect_access_control($content) {
	global $post;
	if (isset($post) && $post!=""){
	
		$post_id = $post;
		if (is_object($post_id)){
			$post_id = $post_id->ID;
		}
		$fbconnect_access_page_check = htmlspecialchars(stripcslashes(get_post_meta($post_id, '_fbconnect_access_page_check', true)));
		if($fbconnect_access_page_check){
			$fbconnect_access_pageid = htmlspecialchars(stripcslashes(get_post_meta($post_id, '_fbconnect_access_pageid', true)));
			$fbconnect_access_page_name = htmlspecialchars(stripcslashes(get_post_meta($post_id, '_fbconnect_access_page_name', true)));
			$fbconnect_access_page_url = htmlspecialchars(stripcslashes(get_post_meta($post_id, '_fbconnect_access_page_url', true)));
			$fbconnect_access_page_thick = htmlspecialchars(stripcslashes(get_post_meta($post_id, '_fbconnect_access_page_thick', true)));
			
			//$fbconnect_access_userid = htmlspecialchars(stripcslashes(get_post_meta($post_id, '_fbconnect_access_userid', true)));
		
			//if ( isset($fbconnect_access_userid) && $fbconnect_access_userid!=""){
			//	return check_access_by_user($fbconnect_access_userid , $content);
			//}else
			if ($fbconnect_access_page_thick=="" && isset($fbconnect_access_pageid) && $fbconnect_access_pageid!=""){
				return check_access_by_page($fbconnect_access_pageid ,$fbconnect_access_page_url, $content);
			}
		}
	}
	return $content;
}
endif;	

if(!function_exists('check_access_by_page')):	
	function check_access_by_page($page_id ,$fbconnect_access_page_url, $content){
		global $post;
		$fb_user = fb_get_loggedin_user();
		if ( $fb_user ){
			try{
				$isfan = fb_pages_isFan($page_id, "".$fb_user);
			}catch(Exception $e){
				$isfan = false;
				$fb_user = 0;
			}
	
		}else{
			$isfan = false;
		}
		
		if ($post!=""){
			$texto = get_post_meta($post->ID , '_fbconnect_access_page_text', true);
			$textopie = get_post_meta($post->ID , '_fbconnect_access_page_text_footer', true);
		}
		
		if($texto==""){
			$texto = fb_get_option('fb_nofans_thick_headtext');
		}
		if($textopie==""){
			$textopie = fb_get_option('fb_nofans_thick_footertext');
		}
		
		if ($fb_user && $isfan) {
			return $content;
		}else{
			$siteUrl= fb_get_option('siteurl');
			$taburl = $siteUrl."/?fbconnect_action=tab&usethick=false&postid=".$post->ID;
			
			$newcontent='<div id="fbaccesstable" class="accesstable">';
			$newcontent.='<script>fb_loadaccesstab(\''.$taburl.'\');</script>';
			
			/*$newcontent.= $texto;
			$newcontent.= '<div id="fanboxlikecontainer">';		
			$newcontent.= '<div id="fanboxlike">';
						
			if($fb_user==""){
				//$newcontent.='To access this page, you must be <a href="javascript:login_fan(\''.$page_id.'\');">logged in with Facebook</a> and be a fan of <a target="_blank" href="'.$fbconnect_access_page_url.'">our Facebook page</a>:';
				$newcontent.='<a id="fbbuttonfan" class="fb_button fb_button_xlarge" onclick="login_fan(\''.$page_id.'\',\''.$post->ID.'\',true)">';
				$newcontent.='<span class="fb_button_text">'.__("Log In").'</span>';
				$newcontent.='</a>';
				
			}elseif(!$isfan){
				$newcontent.='<fb:like-box profile_id="'.$page_id.'" show_faces="false" stream="false" header="false"></fb:like-box>';
			}
			$newcontent.='<script>';
			$newcontent.='var fb_likereload = true;';
			$newcontent.='</script>';
			$newcontent.='</div>';
			$newcontent.='</div>';
			$newcontent.= $textopie;*/
			$newcontent.='</div>';
			return $newcontent;
		}
	}
endif;	

if(!function_exists('check_access_by_user')):	
	function check_access_by_user($private_user , $content){
		$fb_user = fb_get_loggedin_user();
		if ( $fb_user ){
			$amigos = fb_friends_areFriends("".$fb_user, $private_user);
			$arefriends = ($amigos[0]["are_friends"]==1);
		}else{
			$arefriends == false;
		}
	
		if ( ($fb_user && $arefriends) || $fb_user == $private_user) {
			return $content;
		}else{
			$newcontent="<br/><br/>";
			$newcontent.='<table class="accesstable">';
			$newcontent.='<tr><td style="border:0 !important;">';
			$newcontent.='<img src="'.FBCONNECT_PLUGIN_URL.'/images/locked.png" alt="Locked"/>';
			$newcontent.='</td><td style="border:0 !important;">';
			$newcontent.='To access this page, you must be <a href="javascript:login_facebook();">logged in with Facebook</a> and be a friend of: ';
			$newcontent.='<span><fb:profile-pic size="square" width=30 height=30 uid="'.$private_user.'" linked="true" /></span> &nbsp;<span><fb:name uid="'.$private_user.'" /></span>';
			$newcontent.='</td></tr></table>';
			$newcontent.='<br/><br/>';
			return $newcontent;
		}
	}	
endif;		

if(!function_exists('fbconnect_getvideo_post')):		
function fbconnect_getvideo_post($post_id){
	$files = get_children("post_parent=$post_id&post_type=attachment&post_mime_type=application/x-shockwave-flash");
	if ($files!="" && count($files)>0){
		foreach($files as $num=>$file){
			$videourl = $file->guid;
			return $file;
		}
	}
}
endif;

if(!function_exists('fbconnect_getvideourl_post')):		
function fbconnect_getvideourl_post($post_id){
	$videourl = get_post_meta($post_id , 'fbconnect_video_src', true);
	if(isset($videourl) && $videourl!=""){
		return $videourl;
	}
	$file = fbconnect_getvideo_post($post_id);
	$videourl = $file->guid;
	return $videourl;
}
endif;
					
?>