<?php
/**
 * @author: Javier Reyes Gomez (http://www.sociable.es)
 * @date: 05/10/2008
 * @license: GPLv2
 */

		global $wp_version, $fbconnect,$fb_reg_formfields;

			// if we're posted back an update, let's set the values here
			if ( isset($_POST['info_update']) ) {
				check_admin_referer('wp-fbconnect-info_update');
				if (isset($_POST['fb_use_fbtheme_name']) && $_POST['fb_use_fbtheme_name']!=""){
					$themes = get_themes();
					$theme = $themes[$_POST['fb_use_fbtheme_name']];

					if ($theme!=""){
						update_option('fb_use_fbtheme_name', $_POST['fb_use_fbtheme_name']);
						update_option('fb_use_fbtheme',$theme["Template"]);
						update_option('fb_use_fbtheme_style', $theme["Stylesheet"]);
					}
				}else{
					update_option('fb_use_fbtheme_name', "");
					update_option('fb_use_fbtheme',"");
					update_option('fb_use_fbtheme_style', "");
				}

				$error = '';
				//update_option( 'fb_wall_page', $_POST['fb_wall_page'] );
				//update_option( 'desactivar_header_headerfootertab', $_POST['desactivar_header_headerfootertab'] );
				//update_option( 'desactivar_footer_headerfootertab', $_POST['desactivar_footer_headerfootertab'] );
				update_option( 'fb_short_urls', $_POST['fb_short_urls'] );
				update_option( 'fb_canvas_url',$_POST['fb_canvas_url'] );
				update_option( 'fb_canvas_request_auth',$_POST['fb_canvas_request_auth'] );
				update_option( 'fb_canvas_redirect',$_POST['fb_canvas_redirect'] );
				update_option( 'fb_disable_feed',$_POST['fb_disable_feed'] );
				//update_option( 'fb_custom_user_profile',$_POST['fb_custom_user_profile'] );
				//update_option( 'fb_custom_reg_form',$_POST['fb_custom_reg_form'] );
				update_option( 'fb_bloguri_base',$_POST['fb_bloguri_base'] );
				update_option( 'fb_canvas_home',$_POST['fb_canvas_home'] );
				update_option( 'fb_tab_home',$_POST['fb_tab_home'] );
				update_option( 'fb_tab_nofan_home',$_POST['fb_tab_nofan_home'] );
				update_option( 'fb_nofans_thick',$_POST['fb_nofans_thick'] );
				update_option( 'fb_nofans_thick_headtext',$_POST['fb_nofans_thick_headtext'] );
				update_option( 'fb_nofans_thick_footertext',$_POST['fb_nofans_thick_footertext'] );
				update_option( 'fb_nofans_thick_width',$_POST['fb_nofans_thick_width'] );
				update_option( 'fb_nofans_thick_height',$_POST['fb_nofans_thick_height'] );
				update_option( 'fb_nofans_thick_top',$_POST['fb_nofans_thick_top'] );
				update_option( 'fb_shorturl_user',$_POST['fb_shorturl_user'] );
				update_option( 'fb_shorturl_key',$_POST['fb_shorturl_key'] );
				
				
			}
			
			// Display the options page form
			$siteurl = get_option('home');
			if( substr( $siteurl, -1, 1 ) !== '/' ) $siteurl .= '/';
			?>

			<script type='text/javascript'>
				function callbackSelectPage(pageid,pagename,pageurl){
					jQuery("#fb_fanpage_id").attr("value",pageid);
					jQuery("#fb_fanpage_name").attr("value",pagename);
					jQuery("#fb_fanpage_url").attr("value",pageurl);
					tb_remove();
					jQuery("#pagepic").html('<fb:profile-pic uid="'+pageid+'" linked="true" />');
					FB.XFBML.parse();
				}
				
				function selectFBPage(){
						tb_show("Select Page", "<?php echo get_option('siteurl'); ?>?fbconnect_action=userpages&height=450&width=630&callback=callbackSelectPage", "");
				}
			</script>				
			<div class="wrap">
				<h2><?php _e('Facebook Configuration', 'fbconnect') ?>: <?php _e('Page tab & canvas', 'fbconnect') ?></h2>

				<form method="post">

     				<table class="form-table" cellspacing="2" cellpadding="5" width="100%">
						<tr valign="top">
							<th style="width: 33%" scope="row"><?php _e('Facebook canvas/tab theme:', 'fbconnect') ?></th>
							<td>
								<?php
								//$dir = FBCONNECT_PLUGIN_PATH . '/themes';
								?>
								<SELECT id="fb_use_fbtheme_name" name="fb_use_fbtheme_name">
								<OPTION <?php if (get_option('fb_use_fbtheme_name')=="") echo "SELECTED";?> VALUE=""></OPTION>
								<?php 
								$themes = get_themes();
								foreach($themes as $name => $theme){
									if ($name==get_option('fb_use_fbtheme_name')){
										$selected = "SELECTED";
									}else{
										$selected = "";
									} 	
								?>
								<OPTION <?php echo $selected;?> VALUE="<?php echo $name;?>"><?php echo $name;?></OPTION>
								<?php 
								}
								?>
								</SELECT>
								<label for="fb_use_fbtheme"><?php _e('Select the theme to be used on facebook canvas a page tab.', 'fbconnect') ?></label></p>
									<?php
									if (get_option('fb_use_fbtheme')!=""){
										echo '<a class="button" href="./themes.php?page=custom-header&fb_app_mode=on">Edit header »</a> ';
										echo '<a class="button" href="./themes.php?page=custom-background&fb_app_mode=on">Edit background »</a> ';
										echo '<a class="button" href="./widgets.php">Edit widgets »</a>';
										/*if (isset($_SESSION["fb_facebookapp_mode"]) && $_SESSION["fb_facebookapp_mode"]=="on" ){ 
											echo '<a class="button" href="./themes.php?fb_facebookapp_mode=off">Deactivate Facebook template admin mode »</a>';
										}else{
											echo '<a class="button" href="./themes.php?fb_facebookapp_mode=on">Activate Facebook template admin mode »</a>';
										}*/
									}
								?>
							</td>
						</tr>
						<!--
						<tr valign="top">
							<th style="width: 33%" scope="row"><?php _e('Hide theme header:', 'fbconnect') ?></th>
							<td>
								<p><input type="checkbox" name="desactivar_header_headerfootertab" id="desactivar_header_headerfootertab" <?php
								if( get_option('desactivar_header_headerfootertab') ) echo 'checked="checked"'
								?> />
							</td>
						</tr>
						<tr valign="top">
							<th style="width: 33%" scope="row"><?php _e('Hide theme footer:', 'fbconnect') ?></th>
							<td>
								<p><input type="checkbox" name="desactivar_footer_headerfootertab" id="desactivar_footer_headerfootertab" <?php
								if( get_option('desactivar_footer_headerfootertab') ) echo 'checked="checked"'
								?> />
							</td>
						</tr>
						-->
						<tr valign="top">
							<th style="width: 33%" scope="row"><label for="fb_canvas_url"><?php _e('Facebook App. Canvas:', 'fbconnect') ?></label></th>
							<td>
							<input type="text" name="fb_canvas_url" id="fb_canvas_url" size="50" value="<?php echo get_option('fb_canvas_url');?>"/>
							</td>
						</tr>
						<tr valign="top">
							<th style="width: 33%" scope="row"><?php _e('Request canvas user Auth.:', 'fbconnect') ?></th>
							<td>
								<p><input type="checkbox" name="fb_canvas_request_auth" id="fb_canvas_request_auth" <?php
								if( get_option('fb_canvas_request_auth') ) echo 'checked="checked"'
								?> />
							</td>
						</tr>
						<tr valign="top">
							<th style="width: 33%" scope="row"><?php _e('Redirect canvas to:', 'fbconnect') ?></th>
							<td>
								<select name="fb_canvas_redirect" id="fb_canvas_redirect">
								<option value="" <?php if( get_option('fb_canvas_redirect')=="") echo 'selected="selected"'; ?>>No redirect</option>
								<option value="tab" <?php if( get_option('fb_canvas_redirect')=='tab') echo 'selected="selected"'; ?>>Redirect to Facebook page tab</option>
								<option value="site" <?php if( get_option('fb_canvas_redirect')=='site' ) echo 'selected="selected"'; ?>>Redirect canvas to site</option>
								<option value="nosite" <?php if( get_option('fb_canvas_redirect')=='nosite' ) echo 'selected="selected"'; ?>>Redirect canvas and site to tab</option>
								</select>
							</td>
						</tr>
						<tr valign="top">
							<th style="width: 33%" scope="row"><?php _e('Disable Wordpress RSS feeds:', 'fbconnect') ?></th>
							<td>
								<p><input type="checkbox" name="fb_disable_feed" id="fb_disable_feed" <?php
								if( get_option('fb_disable_feed') ) echo 'checked="checked"'
								?> />
							</td>
						</tr>
						<tr valign="top">
							<th style="width: 33%" scope="row"><label for="fb_bloguri_base"><?php _e('Blog URI base:', 'fbconnect') ?></label></th>
							<td>
							<input type="text" name="fb_bloguri_base" id="fb_bloguri_base" size="50" value="<?php echo get_option('fb_bloguri_base');?>"/>
							</td>
						</tr>
						
						<?php 
						?>
						<tr valign="top">
							<th style="width: 33%" scope="row"><?php _e('Home page wall:', 'fbconnect') ?></th>
							<td>
								<p>
								<?php 
								wp_dropdown_pages(array('selected' => get_option('fb_wall_page') , 'name' => 'fb_wall_page', 'show_option_none' => __(' '), 'sort_column'=> 'post_title,menu_order')); 
								?>	
								<label for="fb_wall_page"><?php _e('The comments writed on the home will be associated to the selected page.', 'fbconnect') ?></label></p>

							</td>
						</tr>
						
						
						<?php 
						?>
						
						<tr valign="top">
							<th style="width: 33%" scope="row"><label for="fb_canvas_home"><?php _e('Facebook canvas home:', 'fbconnect') ?></label></th>
							<td>
							<?php wp_dropdown_pages(array('selected' => get_option('fb_canvas_home') , 'name' => 'fb_canvas_home', 'show_option_none' => __(' '), 'sort_column'=> 'post_title,menu_order'));?>
							<label for="fb_canvas_home"><?php _e('Default facebook canvas home page', 'fbconnect') ?></label>
							</td>
						</tr>
						<tr valign="top">
							<th style="width: 33%" scope="row"><label for="fb_tab_home"><?php _e('Facebook tab home:', 'fbconnect') ?></label></th>
							<td>
							<?php wp_dropdown_pages(array('selected' => get_option('fb_tab_home') , 'name' => 'fb_tab_home', 'show_option_none' => __(' '), 'sort_column'=> 'post_title,menu_order'));?>
							<label for="fb_tab_home"><?php _e('Default facebook tab home page', 'fbconnect') ?></label>
							</td>
						</tr>
						<tr valign="top">
							<th style="width: 33%" scope="row"><label for="fb_tab_home"><?php _e('Facebook no fan tab:', 'fbconnect') ?></label></th>
							<td>
							<?php wp_dropdown_pages(array('selected' => get_option('fb_tab_nofan_home') , 'name' => 'fb_tab_nofan_home', 'show_option_none' => __(' '), 'sort_column'=> 'post_title,menu_order'));?>
							<label for="fb_tab_home"><?php _e('Default facebook no fan tab page', 'fbconnect') ?></label>
							</td>
						</tr>
						<tr valign="top">
							<th style="width: 33%" scope="row"><?php _e('No fans thickBox:', 'fbconnect') ?></th>
							<td>
								<p><input type="checkbox" name="fb_nofans_thick" id="fb_nofans_thick" <?php
								if( get_option('fb_nofans_thick') ) echo 'checked="checked"'
								?> />
								<label for="fb_nofans_thick_headtext"><?php _e('Use a ThickBox to lock tab/canvas view.', 'fbconnect') ?></label></p>
								<label for="fb_nofans_thick_headtext"><?php _e('Head text:', 'fbconnect') ?></label><br/>
								<input type="text" size="50" name="fb_nofans_thick_headtext" id="fb_nofans_thick_headtext" value="<?php echo get_option('fb_nofans_thick_headtext'); ?>"/>
								<br/>
								<label for="fb_nofans_thick_footertext"><?php _e('Footer text:', 'fbconnect') ?></label><br/>
								<input type="text" size="50" name="fb_nofans_thick_footertext" id="fb_nofans_thick_footertext" value="<?php echo get_option('fb_nofans_thick_footertext'); ?>"/>
								<br/>
								<div style="float:left;">
								<label for="fb_nofans_thick_width"><?php _e('width:', 'fbconnect') ?></label><br/>
								<input type="text" size="5" name="fb_nofans_thick_width" id="fb_nofans_thick_width" value="<?php echo get_option('fb_nofans_thick_width'); ?>"/>
								</div>
								<div style="float:left;">
								<label for="fb_nofans_thick_height"><?php _e('height:', 'fbconnect') ?></label><br/>
								<input type="text" size="5" name="fb_nofans_thick_height" id="fb_nofans_thick_height" value="<?php echo get_option('fb_nofans_thick_height'); ?>"/>
								</div>
								<div style="float:left;">
								<label for="fb_nofans_thick_top"><?php _e('top:', 'fbconnect') ?></label><br/>
								<input type="text" size="5" name="fb_nofans_thick_top" id="fb_nofans_thick_top" value="<?php echo get_option('fb_nofans_thick_top'); ?>"/>
								</div>
								<br/>
							</td>
						</tr>
						<tr valign="top">
							<th style="width: 33%" scope="row"><?php _e('Short URLs:', 'fbconnect') ?></th>
							<td>
								<p>
								<?php 
								 $fb_short_urls = get_option('fb_short_urls');
								?>	
								<SELECT id="fb_short_urls" name="fb_short_urls">
								<OPTION <?php if ($fb_short_urls=="bit.ly") echo "SELECTED";?> VALUE="bit.ly">bit.ly</OPTION>';
								<OPTION <?php if ($fb_short_urls=="snurl") echo "SELECTED";?> VALUE="snurl">snurl</OPTION>';
								<OPTION <?php if ($fb_short_urls=="tinyurl") echo "SELECTED";?> VALUE="tinyurl">tinyurl</OPTION>';
								<OPTION <?php if ($fb_short_urls=="twitter friendly") echo "SELECTED";?> VALUE="twitter friendly">twitter friendly plugin</OPTION>';
								<OPTION <?php if ($fb_short_urls=="WordpressPermalink") echo "SELECTED";?> VALUE="WordpressPermalink">Wordpress Permalink</OPTION>';
								</SELECT>
								<label for="fb_short_urls"><?php _e('Select a short URL engine.', 'fbconnect') ?></label></p>
							</td>
						</tr>
						
						<tr valign="top">
							<th style="width: 33%" scope="row"><label for="fb_shorturl_user"><?php _e('Short URL API user:', 'fbconnect') ?></label></th>
							<td>
							<input type="text" name="fb_shorturl_user" id="fb_shorturl_user" size="50" value="<?php echo get_option('fb_shorturl_user');?>"/>
							</td>
						</tr>
						
						<tr valign="top">
							<th style="width: 33%" scope="row"><label for="fb_shorturl_key"><?php _e('Short URL API key:', 'fbconnect') ?></label></th>
							<td>
							<input type="text" name="fb_shorturl_key" id="fb_shorturl_key" size="50" value="<?php echo get_option('fb_shorturl_key');?>"/>
							<a href="https://bitly.com/a/your_api_key" target="_blank">Bitly API</a>
							</td>
						</tr>
     				</table>

					
					<?php wp_nonce_field('wp-fbconnect-info_update'); ?>
					
     				<p class="submit"><input class="button-primary" type="submit" name="info_update" value="<?php _e('Update Configuration', 'fbconnect') ?> &raquo;" /></p>
     			</form>