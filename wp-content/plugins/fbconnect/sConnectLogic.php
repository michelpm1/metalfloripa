<?php
/**
 * @author: Javier Reyes Gomez (http://www.sociable.es)
 * @date: 05/10/2008
 * @license: GPLv2
 */

if (!class_exists('WPsConnect_Logic')):

/**
 * Basic logic for spotify.
 */
class WPsConnect_Logic {
public $spotifyoauthclient;
public $spotifyapi;
public $scope;
	
function init($token=""){
	try{
		$clientid = fb_get_option('s_appId');
		$secret = fb_get_option('s_api_secret');
		$this->spotifyoauthclient = new SpotifyWebAPI\Session($clientid, $secret, WPsConnect_Logic::getRedirectUrl() );
		$this->spotifyapi = new SpotifyWebAPI\SpotifyWebAPI();
		$perms = fb_get_option('s_permsToRequestOnConnect');
		if ($perms==""){
			$perms = "user-read-private";
		}
		$this->scope = explode(",",$perms);
		
		if($token!=""){
			$this->spotifyapi->setAccessToken( $token );
		}else if (isset($_SESSION['s_access_token'])) {
			  $this->spotifyapi->setAccessToken($_SESSION['s_access_token']);
		}
	}catch(Exception $e){
		print_r($e);
	}
}	

function setOfflineToken(){
	$userspool = get_option('s_offline_users_pool');	
	$userpoolarray = explode(",",$userspool);
	$userinfo = WPfbConnect_Logic::get_lastwpuser_data($userpoolarray,"spotify");

	$tokens = unserialize($userinfo[0]->access_token);
	$token = $tokens["token"];
	$refreshtoken =	$tokens["refreshtoken"];
	$lastupdate = $tokens["lastrefresh"];
	$expiration = $tokens["expiration"];
	$this->spotifyoauthclient->setRefreshToken($refreshtoken);
	
	if ($timetotal < date('U')){
		$okrefresh =$this->spotifyoauthclient->refreshToken();
		if ($okrefresh){
			$tokens["token"] = $this->spotifyoauthclient->getAccessToken();
			$tokens["refreshtoken"] = $this->spotifyoauthclient->getRefreshToken();
			$tokens["lastrefresh"] = date('U');
			$tokens["expiration"] = $this->spotifyoauthclient->getExpires();
			$this->spotifyapi->setAccessToken( $tokens["token"] );		
			WPfbConnect_Logic::set_useroffline($userinfo[0]->ID,serialize($tokens),1,"spotify");
		}else{
			echo "ERROR REFRESH TOKEN";
			exit;
		}
	}else{
		$this->spotifyapi->setAccessToken( $token );
	}
}

function getRedirectUrl(){
		$siteUrl= fb_get_option('siteurl');
		if (!WPfbConnect_Logic::endsWith("/",$siteUrl)){
			$siteUrl = $siteUrl . "/";
		}
		$siteUrl .= "oauth-login-spotify/";	
		return $siteUrl;
}


function user_getInfo(){
	  $meprofile = $this->spotifyapi->me();
	  $userinfo = array();
	  $userinfo['uid'] = $meprofile->id;
	  //$userinfo['username'] = $meprofile->email;
	  $userinfo['website'] = $meprofile->link;
	  $userinfo['about_me'] = "";
	  $userinfo['email'] = $meprofile->email;
	  if (isset($meprofile->external_urls) && isset($meprofile->external_urls->spotify)){
	  	$userinfo['profile_url'] = $meprofile->external_urls->spotify;
	  }
	  $userinfo['name'] = $meprofile->display_name;
	  $userinfo['first_name'] = "";
	  $userinfo['last_name'] = "";
	  if (isset($meprofile->images) && count($meprofile->images)>0){
	  	$userinfo['thumbnail'] = $meprofile->images[0]->url;
	  }
	  $userinfo['locale'] = $meprofile->country;
	  return $userinfo;

}

function createAuthUrl(){
	$authUrl = $this->spotifyoauthclient->getAuthorizeUrl(array(
        'scope' => $this->scope
    ));
	return $authUrl; 
}


function wp_login() {
		global $wp_version,$new_fb_user;

		if ( isset($_REQUEST["fbconnect_action"]) && ($_REQUEST["fbconnect_action"]=="delete_user" || $_REQUEST["fbconnect_action"]=="postlogout" || $_REQUEST["fbconnect_action"]=="logout")){
			return;
		}
		
		$self = basename( $GLOBALS['pagenow'] );
	
			
		$user = wp_get_current_user();
		if (isset($user) && $user->ID==0){
			$user = "";	
		}
		$pospoty = strpos($_SERVER["REQUEST_URI"],"oauth-login-spotify");
		//if ( isset($_GET['oauth_login']) && $_GET['oauth_login']=="spotify" && (!is_user_logged_in() || $user->fbconnect_userid == "" || $user->fbconnect_userid == 0)) { //Intenta hacer login estando registrado en facebook
		if ($pospoty !== false &&  isset($_GET['code']) && (!is_user_logged_in() || $user->fbconnect_userid == "" || $user->fbconnect_userid == 0)) { //Intenta hacer login estando registrado en facebook

			if (isset($_GET['code'])) {

			  $this->spotifyoauthclient->requestToken($_GET['code']);
			  $this->spotifyapi->setAccessToken($this->spotifyoauthclient->getAccessToken());
			  $_SESSION['s_access_token'] = $this->spotifyoauthclient->getAccessToken();
			  //echo "ANTES:".$this->spotifyoauthclient->getAccessToken();
			  //header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
			}
			if (isset($_SESSION['s_access_token'])) {
			   $this->spotifyapi->setAccessToken($_SESSION['s_access_token']);
			
			
				require_once(ABSPATH . WPINC . '/registration.php');
				$usersinfo = $this->user_getInfo();
				if (!isset($usersinfo) || $usersinfo["uid"]==""){
					WPfbConnect::log("[fbConnectLogic::wp_login_fbconnect] fb_user_getInfo ERROR: ".$fb_user,FBCONNECT_LOG_ERR);
					return;	
				}
				$fb_user = $usersinfo["uid"];
				$_SESSION["facebook_usersinfo"] = $usersinfo;
				$_SESSION["fbconnect_netid"] = "spotify";

				$wpid = "";
				$fbwpuser = WPfbConnect_Logic::get_userbyFBID($usersinfo["uid"],"spotify");
				if ($fbwpuser =="" && $usersinfo["email"]!=""){
					$fbwpuser = WPfbConnect_Logic::get_userbyEmail($usersinfo["email"]);
				}

				//echo "LEER:".$fb_user;
				//print_r($fbwpuser);
				$wpid = "";
				$new_fb_user= false;
	
				if(is_user_logged_in() && $fbwpuser && $user->ID==$fbwpuser->ID && ($user->fbconnect_userid =="" || $user->fbconnect_userid =="0")){ // Encuentra por email el usuario y no está asociado al de FB
					WPfbConnect_Logic::set_userid_fbconnect($user->ID,$fb_user,"spotify");
					$wpid = $user->ID;
				}else if(FBCONNECT_CANVAS!="web" && is_user_logged_in() && $fbwpuser && $user->ID != $fbwpuser->ID){ // El usuario FB está asociado a un usaurio WP distinto al logeado
					$wpid = $fbwpuser->ID;
				}elseif(is_user_logged_in() && !$fbwpuser && ($user->fbconnect_userid =="" || $user->fbconnect_userid =="0")){ // El usuario WP no está asociado al de FB
					WPfbConnect_Logic::set_userid_fbconnect($user->ID,$fb_user,"spotify");
					$wpid = $user->ID;
				}elseif (!is_user_logged_in() && $fbwpuser && ($fbwpuser->fbconnect_userid =="" || $fbwpuser->fbconnect_userid =="0")){
					WPfbConnect_Logic::set_userid_fbconnect($fbwpuser->ID,$fb_user,"spotify");
					$wpid = $fbwpuser->ID;	
				}elseif(!is_user_logged_in() && $fbwpuser && ($fbwpuser->fbconnect_userid ==$fb_user)){
					$wpid = $fbwpuser->ID;	
				}elseif (($fbwpuser && $fbwpuser->fbconnect_userid != $fb_user) || (!is_user_logged_in() && !$fbwpuser) || (!$fbwpuser && is_user_logged_in() && $user->fbconnect_userid != $fb_user)){
					if(isset($usersinfo) && $usersinfo!=""){
						$username = WPfbConnect_Logic::fbusername_generator($usersinfo["uid"],$usersinfo['username'],$usersinfo["name"],$usersinfo["last_name"],"S");
		
						$user_data = array();
						$user_data['user_login'] = $username;
						
						$user_data['user_pass'] = substr( md5( uniqid( microtime() ).$_SERVER["REMOTE_ADDR"] ), 0, 15);
	
						$user_data['user_nicename'] = $username;
						$user_data['display_name'] = $usersinfo["name"];
	
						$user_data['user_url'] = $usersinfo["profile_url"];
						//$user_data['user_email'] = $usersinfo["proxied_email"];
						$user_data['user_email'] = "";
						
						if ($usersinfo["email"]!=""){
							$user_data['user_email'] = $usersinfo["email"];
						}
						//Permitir email en blanco y duplicado
						define ( 'WP_IMPORTING', true);

						$wpid = wp_insert_user($user_data);
						if ( !is_wp_error($wpid) ) {
							update_usermeta( $wpid, "first_name", $usersinfo["first_name"] );
							update_usermeta( $wpid, "last_name", $usersinfo["last_name"] );
	
							if (isset($usersinfo["sex"]) && $usersinfo["sex"] != ""){
								update_usermeta( $wpid, "sex", $usersinfo["sex"] );
							}
							WPfbConnect_Logic::set_userid_fbconnect($wpid,$fb_user,"spotify");
							$new_fb_user= true;
						}else{ // no ha podido insertar el usuario
							return;
						}
					}
					
				}else{
					return;
				}

	
				$userdata = WPfbConnect_Logic::get_userbyFBID($fb_user,"spotify");
				
				WPfbConnect_Logic::set_lastlogin_fbconnect($userdata->ID);
				global $current_user;
	
				$current_user = null;
		
	
				WPfbConnect_Logic::fb_set_current_user($userdata);
				
				//Update thumbnail
				if($userdata!="" && $usersinfo["thumbnail"]!=""){
					update_usermeta($userdata->ID, 'thumbnail', $usersinfo["thumbnail"]);
				}
	
				global $userdata;
				if (isset($userdata) && $userdata!=""){
					$userdata->fbconnect_userid = $fb_user;
					$userdata->fbconnect_netid = "spotify";
				}
				
				//Store user token
				if (fb_get_option('fb_storeUserAcessToken')!="" ){
					//echo "<BR/>TOKEN1:".serialize($_SESSION['s_access_token']);
					$tokens = array();
					$tokens["token"] = $_SESSION['s_access_token'];
					$tokens["refreshtoken"] = $this->spotifyoauthclient->getRefreshToken();
					$tokens["lastrefresh"] = date('U');
					$tokens["expiration"] = $this->spotifyoauthclient->getExpires(); //es segundos, 3600
					WPfbConnect_Logic::set_useroffline($userdata->ID,serialize($tokens),1,"spotify");
				}

				header('Content-type: text/html');
				if ($new_fb_user){
					$new_fb_usertxt = "true";
				}else{
					$new_fb_usertxt = "false";
				}
				$terms = get_user_meta($userdata->ID, "terms", true);
				?>
				<html>
				<body>
				<script>
					window.opener.fb_refreshlogininfo('spotify','<?php echo $fb_user;?>','<?php echo $userdata->ID;?>',<?php echo $new_fb_usertxt;?>,'<?php echo $terms;?>');
					window.close();
				</script>
				</body>
				</html>
				<?php
				exit;
				//Cache friends
				/*WPfbConnect_Logic::get_connected_friends();
				if (fb_get_option('fb_permsToRequestOnConnect')!="" ){
					if (strrpos(fb_get_option('fb_permsToRequestOnConnect'),"offline_access")===false){
						//Not found
					}elseif($userdata!=""){
						$token = fb_get_access_token();
						//update_usermeta( $userdata->ID, "access_token", $token );
						WPfbConnect_Logic::set_useroffline($userdata->ID,$token,1);
						
					}
				}*/
				
			}else{
				header('Content-type: text/html');
				echo "ERROR NO ACCESS TOKEN";
				exit;
			}
		}
	}
	
} 
endif; // end if-class-exists test

try{
	require_once FBCONNECT_PLUGIN_PATH.'/spotify/SpotifyWebAPIException.php';
	require_once FBCONNECT_PLUGIN_PATH.'/spotify/Request.php';
	require_once FBCONNECT_PLUGIN_PATH.'/spotify/Session.php';
	require_once FBCONNECT_PLUGIN_PATH.'/spotify/SpotifyWebAPI.php';
	global $sLogic;
	$sLogic = new WPsConnect_Logic;
	$sLogic->init();
}catch(Exception $e){
	print_r($e);
}
?>
