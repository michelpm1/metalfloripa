<?php
/**
 * @author: Javier Reyes Gomez (http://www.sociable.es)
 * @date: 05/10/2008
 * @license: GPLv2
 */

		global $wp_version, $fbconnect,$fb_reg_formfields;

			if ( isset($_POST['info_update']) ) {
				check_admin_referer('wp-fbconnect-info_update');

				$error = '';
				update_option( 'i_login_enabled', $_POST['i_login_enabled'] );
				update_option( 'i_permsToRequestOnConnect', $_POST['i_permsToRequestOnConnect'] );
				update_option( 'i_api_key', $_POST['i_api_key'] );
				update_option( 'i_appId', $_POST['i_appId'] );
				update_option( 'i_api_secret', $_POST['i_api_secret'] );
				
				if ($error !== '') {
					echo '<div class="error"><p><strong>'.__('At least one of Instagram Connector options was NOT updated', 'fbconnect').'</strong>'.$error.'</p></div>';
				} else {
					echo '<div class="updated"><p><strong>'.__('Instagram Connector options updated', 'fbconnect').'</strong></p></div>';
				}

			
			}
			
			// Display the options page form
			$siteurl = fb_get_option('home');
			if( substr( $siteurl, -1, 1 ) !== '/' ) $siteurl .= '/';
			?>
			<div class="wrap">
				<h2>
					<img src="<?php echo FBCONNECT_PLUGIN_URL;?>/images/instagram.png"/>
					<?php _e('Instagram Configuration', 'fbconnect') ?></h2>

				<form method="post">


					<h3><?php _e('Instagram Application Configuration', 'fbconnect') ?></h3>
     				<table class="form-table" cellspacing="2" cellpadding="5" width="100%">
     					<tr valign="top">
							<th style="width: 33%" scope="row"><?php _e('Enable Instagram login:', 'fbconnect') ?></th>
							<td>
								<p><input type="checkbox" name="i_login_enabled" id="i_login_enabled" <?php
								if( fb_get_option('i_login_enabled')) echo 'checked="checked"'
								?> />
							</td>
						</tr>
						<tr valign="top">
							<th style="width: 33%" scope="row"><label for="i_permsToRequestOnConnect"><?php _e('Perms to request:', 'fbconnect') ?></label></th>
							<td>
							<input type="text" name="i_permsToRequestOnConnect" id="i_permsToRequestOnConnect" size="50" value="<?php echo fb_get_option('i_permsToRequestOnConnect');?>"/>
							<?php _e('Perms to request, comma separated list: (basic,likes,comments,relationships).', 'fbconnect') ?>
							</td>
						</tr>
						<tr valign="top">
							<th style="width: 33%" scope="row"><?php _e('Instagram App. Config.', 'fbconnect') ?></th>
							<td>
							<a href="http://instagr.am/developer/register/" target="_blank"><?php _e('Create a new Instagram Application', 'fbconnect') ?></a><br/>
							<br/><?php _e('Your Instagram redirect URI (copy and paste in the Instagram Application configuration):', 'fbconnect'); ?>
							<br/><input type="text" name="i_callback" id="i_callback" size="50" value="<?php echo WPiConnect_Logic::getRedirectUrl();?>"/>
							</td>
						</tr>

						<tr valign="top">
							<th style="width: 33%" scope="row"><label for="i_appId"><?php _e('Instagram App ID:', 'fbconnect') ?></label></th>
							<td>
							<input type="text" name="i_appId" id="i_appId" size="50" value="<?php echo fb_get_option('i_appId');?>"/>
							</td>
						</tr>
						<tr valign="top">
							<th style="width: 33%" scope="row"><label for="i_api_secret"><?php _e('Instagram client secret:', 'fbconnect') ?></label></th>
							<td>
							<input type="text" name="i_api_secret" size="50" id="i_api_secret" value="<?php echo fb_get_option('i_api_secret');?>"/>
							</td>
						</tr>
						

     				</table>

					
					<?php wp_nonce_field('wp-fbconnect-info_update'); ?>
					
     				<p class="submit"><input class="button-primary" type="submit" name="info_update" value="<?php _e('Update Configuration', 'fbconnect') ?> &raquo;" /></p>
     			</form>
				
			</div>
