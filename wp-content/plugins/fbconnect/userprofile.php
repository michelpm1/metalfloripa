<?php

if( fb_get_option('fb_connect_avatar_link')!=""){
	exit;
}
	global $userprofile;
	$userprofile = WPfbConnect_Logic::get_user();
	$sizeimg="small";
	if(fb_get_option('fb_connect_use_thick')){
			//$sizeimg="square";
			$style='fbthickprofile';
	}
?>	
	
<div class="fbconnect_userprofile <?php echo $style;?>">
	<div class="fbconnect_userpicmain">
		<?php
		    $fbuser = $userprofile->fbconnect_userid;
			$netid = $userprofile->fbconnect_netid;
			$size = 100;
			echo get_avatar($userprofile->ID,100);
		?>
	</div>
	
	<div class="fbconnect_userprofileinfo">
		<span class="fbprofileinfofield">
			<span class="fbprofileinfofieldlabel">
			<?php _e('Name:', 'fbconnect') ?>
			</span>
			<span class="fbprofileinfofieldvalue">
			<?php echo $userprofile->display_name; ?>
			</span>
		</span>
		<span class="fbprofileinfofield">
			<span class="fbprofileinfofieldlabel">
				<?php _e('Member since:', 'fbconnect') ?>
			</span>
			<span class="fbprofileinfofieldvalue">
				<?php echo $userprofile->user_registered; ?>
			</span>
		</span>		
		<span class="fbprofileinfofield">
			<span class="fbprofileinfofieldlabel">
				<?php _e('Website URL:', 'fbconnect') ?>
			</span>
			<span class="fbprofileinfofieldvalue">
				<a href="<?php echo $userprofile->user_url; ?>" rel="external nofollow"><?php echo $userprofile->user_url; ?></a>
			</span>
		</span>	
		<span class="fbprofileinfofield">
			<span class="fbprofileinfofieldlabel">
				<?php _e('About me:', 'fbconnect') ?>
			</span>
			<span class="fbprofileinfofieldvalue">
				<?php echo $userprofile->description; ?>
			</span>
		</span>
			<?php 
			$user = wp_get_current_user();
			if ($user!="" && $user->ID!=0 && $userprofile->ID==$user->ID) : ?>
			<span class="fbprofileinfofield">
				<a target="_blank" href="<?php echo fb_get_option('siteurl')."/?fbconnect_action=delete_user"; ?>">[delete profile]</a>
			</span>
			<?php endif; ?>
		
	</div>
</div>
