<?php
/**
 * @author: Javier Reyes Gomez (http://www.sociable.es)
 * @date: 23/12/2008
 * @license: GPLv2
 */
//error_reporting(E_ALL & ~E_DEPRECATED);

if (! defined('FBCONNECT_PLUGIN_PATH'))
	require_once("../../../wp-config.php");

$user = wp_get_current_user();
$fb_user = fb_get_loggedin_user();

if ( $user=="" || $user->ID==0 ) {
	if (isset($_REQUEST["postid"]) && $_REQUEST["postid"]!=""){
	$textohead = get_post_meta($_REQUEST["postid"], 'fbconnect_access_login_text', true);
	}
?> 
	<div id="fbloginpopup">
		<div class="headlogin">
			<?php echo $textohead;?>
		</div>
<?php 
		$loginbutton = "xlarge";
		$fb_hide_edit_profile = true;
		$fb_hide_invite_link = true;
		$fb_breaklinesprofile = "on";
		$fb_showwplogin="on";
		$fb_users_can_register = fb_get_option('fb_users_can_register');
		if (!$fb_users_can_register){
			$fb_showwplogin="off";
		}
		$fblogintext = __("Log in with your social platform", 'fbconnect');
		$fbloginwordpresstext = __("Log In with your user/password", 'fbconnect');
		$fb_mainavatarsize = 30;
		if(isset($_REQUEST["loginreload"]) && $_REQUEST["loginreload"]=="true"){
			$fb_loginreload = true;
		}elseif(isset($_REQUEST["loginreload"]) && $_REQUEST["loginreload"]=="false"){
			$fb_loginreload = false;
		}

		include FBCONNECT_PLUGIN_PATH."/fbconnect_widget_login.php";
		?>
		
	</div>

	<script>
		fb_centerthickbox("#TB_window");
	</script>
<?php }else{ 
global $new_fb_user;
?>
<script>
	fb_netid = "<?php echo $user->fbconnect_netid;?>";
	fb_userid = "<?php echo $user->fbconnect_userid;?>";
	wp_userid = "<?php echo $user->ID;?>";
	fb_user_terms = "<?php echo get_user_meta($user->ID, "terms", true);?>";
	fb_isNewUser = "<?php echo $new_fb_user;?>";
	fb_refreshlogininfo(fb_netid,fb_userid,<?php echo $user->ID;?>,fb_isNewUser,fb_user_terms);

</script>
<?php } ?>