<?php
/*
 Plugin Name: Social Layer (Facebook OpenGraph)
 Plugin URI: http://www.sociable.es/facebook-connect
 Description: Facebook, Google ,Instagram and Twitter login, share and widgets. 
 Author: Javier Reyes
 Author URI: http://www.sociable.es/
 Version: 6.5
 License: GPL (http://www.fsf.org/licensing/licenses/info/GPLv2.html) 
 */
if (strpos($_SERVER["SCRIPT_NAME"],basename(__FILE__)) !== false) {
  echo "Direct load not allowed";
  exit;
}

global $fbconnect_widgets;
$fbconnect_widgets = array();
$fbconnect_widgets["FacebookConnector"] = "Main login widget.";
$fbconnect_widgets["FanBox"] = "Facebook like/fan box.";
$fbconnect_widgets["ActivityRecommend"] = "Facebook activity and recomendations.";
$fbconnect_widgets["Recommend"] = "Facebook Recomendations.";
$fbconnect_widgets["LastFriends"] = "Last registered friends.";
$fbconnect_widgets["LastUsers"] = "Last registered users.";
$fbconnect_widgets["FriendsFeed"] = "Friends feed.";
$fbconnect_widgets["CommentsFeed"] = "Comments feed.";
$fbconnect_widgets["RecommendationsBar"] = "Facebook Recommendations Bar";

add_filter('option_siteurl', 'sslFBConnectFilter');
add_filter('option_home', 'sslFBConnectFilter');
add_filter('option_url', 'sslFBConnectFilter');
add_filter('option_wpurl', 'sslFBConnectFilter');
add_filter('option_stylesheet_url', 'sslFBConnectFilter');
add_filter('option_template_url', 'sslFBConnectFilter');

function fb_get_option($option, $default = false ) {
	$pos = strpos($option, "fb_");
	$pos2 = strpos($option, "t_");
	$pos3 = strpos($option, "g_");
	$pos4 = strpos($option, "tuenti_");
	$pos5 = strpos($option, "sjworkspaces_");
	$pos6 = strpos($option, "lin_");
	
	
	if ($pos !== false || $pos2 !== false || $pos3 !== false || $pos4 !== false || $pos5 !== false || $pos6 !== false) {
	
	 
	 	$alloptions = wp_load_alloptions();
	
		if ( isset( $alloptions[$option] ) ) {
			return maybe_unserialize($alloptions[$option]);
		} else {
			return "";
		}
	}else{
		return get_option($option, $default);
	}
}

function sslFBConnectFilter($value) {
	if(fb_get_option('fb_ssllinkrewrite')!="" && is_ssl() ) {
		$value = preg_replace('|/+$|', '', $value);
		$value = preg_replace('|http://|', 'https://', $value);
		//echo "value:".$value;
	}
	return $value;
}

define('FBCONNECT_LOG_EMERG',    1);     /** System is unusable */
define('FBCONNECT_LOG_ERR',      2);     /** Error conditions */
define('FBCONNECT_LOG_WARNING',  3);     /** Warning conditions */
define('FBCONNECT_LOG_INFO',     4);     /** Informational */
define('FBCONNECT_LOG_DEBUG',    5);     /** Debug-level messages */

define('FBCONNECT_LOG_LEVEL', fb_get_option('fb_connect_log_level')); 

define ( 'FBCONNECT_PLUGIN_REVISION', 244); 

define ( 'FBCONNECT_DB_REVISION', 26);
define ( 'FBCONNECT_TICKWIDTH', 435);
define ( 'FBCONNECT_TICKHEIGHT', 400);

if (! defined('WP_CONTENT_DIR'))
    define('WP_CONTENT_DIR', ABSPATH . 'wp-content');

if (! defined('WP_THEME_DIR'))
    define('WP_THEME_DIR', WP_CONTENT_DIR . '/themes');


define('WP_CONTENT_URL_FB', fb_get_option('siteurl') . '/wp-content');

if (! defined('WP_PLUGIN_DIR'))
    define('WP_PLUGIN_DIR', WP_CONTENT_DIR . '/plugins');

define('WP_PLUGIN_URL_FB', WP_CONTENT_URL_FB . '/plugins');
	
define ('FBCONNECT_PLUGIN_BASENAME', plugin_basename(dirname(__FILE__)));
define ('FBCONNECT_PLUGIN_PATH', WP_PLUGIN_DIR."/".FBCONNECT_PLUGIN_BASENAME);
define ('FBCONNECT_PLUGIN_PATH_STYLE', FBCONNECT_PLUGIN_PATH."/fbconnect.css");
define ('FBCONNECT_PLUGIN_PATH_LOG', WP_PLUGIN_DIR."/".FBCONNECT_PLUGIN_BASENAME."/Log/fbconnectwp.txt");
define ('FBCONNECT_PLUGIN_URL_LOG', WP_PLUGIN_URL_FB."/".FBCONNECT_PLUGIN_BASENAME."/Log/fbconnectwp.txt");
define ('FBCONNECT_PLUGIN_URL', WP_PLUGIN_URL_FB."/".FBCONNECT_PLUGIN_BASENAME);
define ('FBCONNECT_PLUGIN_URL_IMG', WP_PLUGIN_URL_FB."/".FBCONNECT_PLUGIN_BASENAME."/images");
define ('FBCONNECT_PLUGIN_LANG', FBCONNECT_PLUGIN_BASENAME."/lang");
define ('FBCONNECT_PAGE_URL', "http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]);

global $fb_reg_formfields;
$fb_reg_formfields = array("name","nickname","email","password","sex","birthdate","user_url","location_city","location_state","location_country","location_zip","about","company_name","phone","terms","twitter","locale");

global $fb_social_login_networks;
$fb_social_login_networks = array("email","facebook","google","twitter","linkedin","instagram","paypal","spotify");

global $fb_social_login_networks_short;
$fb_social_login_networks_short = array("email"=>"e","facebook"=>"fb","google"=>"g","twitter"=>"t","linkedin"=>"lin","instagram"=>"i","paypal"=>"p","spotify"=>"s");
//$fb_social_login_networks_short = array("email"=>"e","facebook"=>"fb","google"=>"g","twitter"=>"t","linkedin"=>"lin","instagram"=>"i","paypal"=>"p");

global $fb_social_login_networks_enabled;
$fb_social_login_networks_enabled = array("email"=>false,"facebook"=>false,"google"=>false,"twitter"=>false,"linkedin"=>false,"instagram"=>false,"paypal"=>false,"spotify"=>false);
//$fb_social_login_networks_enabled = array("email"=>false,"facebook"=>false,"google"=>false,"twitter"=>false,"linkedin"=>false,"instagram"=>false,"paypal"=>false);

global $fb_count_social_login_networks_enabled;
//set_include_path( dirname(__FILE__) . PATH_SEPARATOR . get_include_path() );   

if  (!class_exists('WPfbConnect')):
class WPfbConnect {
	
	function WPfbConnect() {
		
	}

	function log($msg,$level=1){
		if (FBCONNECT_LOG_LEVEL!=-1 && FBCONNECT_LOG_LEVEL!="" && $level<= FBCONNECT_LOG_LEVEL){
			$text_level = array('EMERG','ERROR','WARN ','INFO ','DEBUG');
			$date = date('d/m/Y H:i:s'); 
			$msg = "[".$date."] [".$text_level[$level-1]."] ".$msg."\n"; 
			$resp = error_log($msg, 3, FBCONNECT_PLUGIN_PATH_LOG);
		}elseif($level<3 && $level>-1){
			$resp = error_log($msg);
		}
	}

	function netId() {
		$fbconnect_netid = $_SESSION["fbconnect_netid"];

		if ($fbconnect_netid==""){
			return "facebook";
		}else{
			return $fbconnect_netid;
		}
	}
	
	function textdomain() {
		load_plugin_textdomain('fbconnect', PLUGINDIR ."/".FBCONNECT_PLUGIN_LANG);
	}

	function table_prefix() {
		global $wpdb;
		return isset($wpdb->base_prefix) ? $wpdb->base_prefix : $wpdb->prefix;
	}

	function comments_table_name() { global $wpdb; return $wpdb->comments; }
	function usermeta_table_name() { global $wpdb; return $wpdb->usermeta; }
	function users_table_name() { global $wpdb; return $wpdb->users; }
	function friends_table_name() 
	{ 
		global $wpdb; 
		$aux=$wpdb->users;

        $pos = strpos($aux, "users");
        return substr($aux, 0, $pos) . 'fb_friends'; 
	}
	
	function lastlogin_table_name()
	{
		global $wpdb; 
		$aux=$wpdb->users;

        $pos = strpos($aux, "users");
        return substr($aux, 0, $pos) . 'fb_lastlogin'; 

	}
}
endif;



if ( ! session_id() ){
	 session_start();
}

require_once(FBCONNECT_PLUGIN_PATH.'/fbConnectLogic.php');

require_once(FBCONNECT_PLUGIN_PATH.'/fbConnectInterface.php');

require_once(FBCONNECT_PLUGIN_PATH.'/pro/fbConnectCorePro.php');

define('WP_THEME', fb_get_option('siteurl') . '/wp-content/themes');
define('WP_THEME_FB', fb_get_option('siteurl') . '/wp-content/themes');

if (! defined('WP_CURRENT_THEME_PATH'))
    define('WP_CURRENT_THEME_PATH', get_template_directory());
		
define('WP_CURRENT_THEME_URL_FB', get_bloginfo('stylesheet_directory'));

	
WPfbConnect_Logic::updateplugin();

add_action('init', 'fb_load_scripts',1);

function fb_load_scripts(){
	wp_enqueue_script('jquery',"","","",false);
	
	if(file_exists (WP_CURRENT_THEME_PATH.'/fbconnect.js')){
		wp_enqueue_script('fbconnect_script', sslFBConnectFilter(WP_CURRENT_THEME_URL_FB.'/fbconnect.js'),array(),FBCONNECT_PLUGIN_REVISION); 
	}else{
		wp_enqueue_script('fbconnect_script', sslFBConnectFilter(FBCONNECT_PLUGIN_URL.'/fbconnect.js'),array(),FBCONNECT_PLUGIN_REVISION); 
	}
	$fbconnect_i10n = array();
	$fbconnect_i10n["close"] = __("Close","fbconnect");
	$fbconnect_i10n["accept"] = __("Aceptar","fbconnect");
	
	wp_localize_script('fbconnect_script', 'fbconnect_data', $fbconnect_i10n );
	
	if (fb_get_option('tw_add_post_head_share')!="" || fb_get_option('tw_add_post_share')!="") {
		wp_enqueue_script("twitter","https://platform.twitter.com/widgets.js");
	}
	
	if (fb_get_option('li_add_post_head_share')!="" || fb_get_option('li_add_post_share')!="") {
		wp_enqueue_script("linkedin","https://platform.linkedin.com/in.js");
	}
	
	if (fb_get_option('fb_add_post_head_google1')!="" || fb_get_option('fb_add_post_google1')!="") {
		wp_enqueue_script("plusone","https://apis.google.com/js/plusone.js");
	}
	
	if (fb_get_option('fb_add_post_head_pinterest')!="" || fb_get_option('fb_add_post_pinterest')!="") {
		wp_enqueue_script("pinterest","https://assets.pinterest.com/js/pinit.js","","1",true);
	}
	add_thickbox();
}

if (!function_exists('fbconnect_title')):
function fbconnect_title($title) {
	if($_REQUEST['fbconnect_action']=="community"){
		return __('Community', 'fbconnect')." - ".$title;
	}else if($_REQUEST['fbconnect_action']=="myhome"){
		$userprofile = WPfbConnect_Logic::get_user();
		return $userprofile->display_name." - ".$title;
	}else if($_REQUEST['fbconnect_action']=="invite"){
		return _e('Invite your friends', 'fbconnect')." - ".$title;
	}
		
	return $title;
}
endif;

/*
Ver rewrite.php
if (!function_exists('fbconnect_add_custom_urls')):
function fbconnect_add_custom_urls() {
  add_rewrite_rule('(userprofile)/[/]?([0-9]*)[/]?([0-9]*)$', 
  'index.php?fbconnect_action=myhome&fbuserid=$matches[2]&var2=$matches[3]');
  add_rewrite_tag('%fbuserid%', '[0-9]+');
  add_rewrite_tag('%var2%', '[0-9]+');
}
endif;
*/

// -- Register actions and filters -- //

add_filter('wp_title', 'fbconnect_title');

add_filter('get_comment_author_url', array('WPfbConnect_Logic', 'get_comment_author_url'));
add_filter('get_avatar_comment_types', array('WPfbConnect_Logic', 'get_avatar_comment_types'));

add_filter('get_comment_author_link', array('WPfbConnect_Logic','fbc_remove_nofollow'));
if (fb_get_option('fb_hide_wpcomments') || fb_get_option('fb_show_fbcomments')){
	add_filter('comments_template', array('WPfbConnect_Logic','fbc_comments_template'));
}

add_action('the_content', array( 'WPfbConnect_Interface', 'add_fbshare' ),9 );

//add_filter('get_the_excerpt', array( 'WPfbConnect_Interface','remove_share'), 1); 

add_action('save_post', array( 'WPfbConnect_Interface','fbconnect_save_post'));

if(fb_get_option('fb_add_wpmain_image')){
	if(function_exists('add_theme_support')):
		add_theme_support( 'post-thumbnails' );	
	endif;
}
add_action( 'init', array( 'WPfbConnect','textdomain') ,1 ); // load textdomain

register_activation_hook(FBCONNECT_PLUGIN_BASENAME.'/fbConnectCore.php', array('WPfbConnect_Logic', 'activate_plugin'));
register_deactivation_hook(FBCONNECT_PLUGIN_BASENAME.'/fbConnectCore.php', array('WPfbConnect_Logic', 'deactivate_plugin'));

add_action( 'admin_menu', array( 'WPfbConnect_Interface', 'add_admin_panels' ) );

add_filter('language_attributes', array('WPfbConnect_Logic', 'html_namespace'));
add_filter('get_avatar', array('WPfbConnect_Logic', 'fb_get_avatar'),10,4);

add_action( 'logout_url', array( 'WPfbConnect_Logic', 'fb_logout'),1);

function fb_login_validation(){
	global $fb_social_login_networks_short;
	global $fb_social_login_networks_enabled;
	global $fb_count_social_login_networks_enabled;
	$fb_count_social_login_networks_enabled = 0;
	foreach ($fb_social_login_networks_short as $keynet=>$shortnet){
		if ($shortnet=="e"){
			$enabled = fb_get_option('fb_users_can_register');
		}else if ($shortnet=="fb"){
			$enabled = fb_get_option('fb_facebooklogin_enabled');
			WPfbConnect_Logic::wp_login_fbconnect();
		}else{
			$enabled = fb_get_option($shortnet.'_login_enabled');
			if ($enabled || WP_ADMIN==1){
				require_once FBCONNECT_PLUGIN_PATH.'/'.$shortnet.'ConnectLogic.php';
				global ${$shortnet.'Logic'};
				if (${$shortnet.'Logic'}!=""){
					${$shortnet.'Logic'}->wp_login();
				}
			}
			/*if (WP_ADMIN==1){
				require_once(FBCONNECT_PLUGIN_PATH.'/'.$shortnet.'ConnectInterface.php');
			}*/
		}
		if ($enabled){
			$fb_social_login_networks_enabled[$keynet] = true;
			$fb_count_social_login_networks_enabled++;
		}
	}
}

add_action( 'init', fb_login_validation, 1 );

function fb_filter_query( $request ) {
    $dummy_query = new WP_Query();  // the query isn't run if we don't pass any query vars
    $dummy_query->parse_query( $request );
//print_r($dummy_query);
    // this is the actual manipulation; do whatever you need here
/*    if ( $dummy_query->is_home() )
        $request['category_name'] = 'news';*/
//fb_login_validation();
    return $request;
}
add_filter( 'request', 'fb_filter_query' );

if (WP_ADMIN!=1){
	add_action( 'init', 'fb_remove_admin_bar' ,1 ); 
}else{
	add_action( 'admin_menu', array( 'WPfbConnect_Interface', 'fbconnect_add_main_img_box' ),1);
}

if(!function_exists('fb_remove_admin_bar')):
function fb_remove_admin_bar(){
	if (fb_get_option('fb_removeadminbar') && current_user_can('subscriber') && WP_ADMIN!=1){
	    add_filter( 'show_admin_bar', '__return_false' );
		show_admin_bar(false);
		wp_deregister_script('admin-bar');
		wp_deregister_style('admin-bar');
		remove_filter('wp_head','wp_admin_bar');
		remove_filter('wp_footer','wp_admin_bar');
		remove_filter('admin_head','wp_admin_bar');
		remove_filter('admin_footer','wp_admin_bar');
		remove_filter('wp_head','wp_admin_bar_class');
		remove_filter('wp_footer','wp_admin_bar_class');
		remove_filter('admin_head','wp_admin_bar_class');
		remove_filter('admin_footer','wp_admin_bar_class');
		remove_action('wp_head','wp_admin_bar_render',1000);
		remove_filter('wp_head','wp_admin_bar_render',1000);
		remove_action('wp_footer','wp_admin_bar_render',1000);
		remove_filter('wp_footer','wp_admin_bar_render',1000);
		remove_action('admin_head','wp_admin_bar_render',1000);
		remove_filter('admin_head','wp_admin_bar_render',1000);
		remove_action('admin_footer','wp_admin_bar_render',1000);
		remove_filter('admin_footer','wp_admin_bar_render',1000);
		remove_action('init','wp_admin_bar_init');
		remove_filter('init','wp_admin_bar_init');
		remove_action('wp_head','wp_admin_bar_css');
		remove_action('wp_head','wp_admin_bar_dev_css');
		remove_action('wp_head','wp_admin_bar_rtl_css');
		remove_action('wp_head','wp_admin_bar_rtl_dev_css');
		remove_action('admin_head','wp_admin_bar_css');
		remove_action('admin_head','wp_admin_bar_dev_css');
		remove_action('admin_head','wp_admin_bar_rtl_css');
		remove_action('admin_head','wp_admin_bar_rtl_dev_css');
		remove_action('wp_footer','wp_admin_bar_js');
		remove_action('wp_footer','wp_admin_bar_dev_js');
		remove_action('admin_footer','wp_admin_bar_js');
		remove_action('admin_footer','wp_admin_bar_dev_js');
		remove_action('wp_ajax_adminbar_render','wp_admin_bar_ajax_render');
		remove_filter('wp_ajax_adminbar_render','wp_admin_bar_ajax_render');
		remove_action('personal_options','_admin_bar_pref');
		remove_filter('personal_options','_admin_bar_pref');
		remove_action('personal_options','_get_admin_bar_pref');
		remove_filter('personal_options','_get_admin_bar_pref');
		remove_filter('locale','wp_admin_bar_lang');
		remove_filter('admin_footer','wp_admin_bar_render');
	}
}
endif;

// Comment filtering
add_action( 'comment_post', array( 'WPfbConnect_Logic', 'comment_fbconnect' ), 5 );

add_filter( 'comment_post_redirect', array( 'WPfbConnect_Logic', 'comment_post_redirect'), 0, 2);
if( fb_get_option('fb_enable_approval') ) {
	add_filter( 'pre_comment_approved', array('WPfbConnect_Logic', 'comment_approval'));
}


// include internal stylesheet
add_action( 'wp_head', array( 'WPfbConnect_Interface', 'style'),100);
add_action( 'login_head', array( 'WPfbConnect_Interface', 'style'),1);

if( fb_get_option('fb_enable_commentform') ) {
	add_action( 'comment_form', array( 'WPfbConnect_Interface', 'comment_form'), 10);
	add_action( 'comment_form_must_log_in_after', array( 'WPfbConnect_Interface', 'comment_form'), 10);
}

add_action( 'admin_init', 'fb_admin_init' ); 
add_action( 'admin_init', array( 'WPfbConnect_Logic', 'wp_login_fbconnect' ),1 ); 	
if(!function_exists('fb_admin_init')):
	function fb_admin_init(){
		wp_enqueue_script('jquery');
		wp_enqueue_script('jquery-ui-selectable');
		//wp_enqueue_script('jquery-form');
		wp_enqueue_script("thickbox");
		
		// Add only in Rich Editor mode
	   /*if ( get_user_option('rich_editing') == 'true') {
    	 add_filter("mce_external_plugins", "add_fb_tinymce_plugin");
	     add_filter('mce_buttons', 'register_fb_button');
	   }*/
	}
endif;

add_filter( 'single_template', array( 'WPfbConnect_Interface', 'fbconnect_single_template'));
add_filter( 'page_template', array( 'WPfbConnect_Interface', 'fbconnect_single_template'));
add_filter('user_contactmethods', 'fb_set_user_contactmethods');

if(!function_exists('fb_set_user_contactmethods')):
function fb_set_user_contactmethods($contactslist){
	$fb_form_fields = fb_get_option('fb_form_fields');
	$fb_form_fields_bool = array();
	global $fb_reg_formfields;
	if ($fb_reg_formfields){
		foreach($fb_reg_formfields as $field){
		 	$pos = strrpos($fb_form_fields, ";".$field.";");
			if (!is_bool($pos)) {
				$fb_form_fields_bool[$field] = $field;
			}
		}
	}
	$fulllist = array_merge($contactslist,$fb_form_fields_bool);
	unset($fulllist["email"]);
	unset($fulllist["name"]);
	return $fulllist;
}
endif;

if(!function_exists('register_fb_button')):
function register_fb_button($buttons) {
   array_push($buttons, "separator", "fbconnect");
   return $buttons;
}
endif;

add_action( 'login_form', 'fb_wp_login_form');

if(!function_exists('fb_wp_login_form')):
function fb_wp_login_form() {
	global $fb_in_loginform;
	$fb_in_loginform = true;
	//echo '<hr style="clear: both; margin-bottom: 1.0em; border: 0; border-top: 1px solid #999; height: 1px;" />';
	echo '<div style="margin-top:5px;margin-bottom:20px;">';
	echo '<div class="fbconnect_widget_divclass">';
	echo '<div style="margin-bottom:3px;with:100%">Or login with your social platform:</div>';
	
	$url = fb_get_option('siteurl');	
	if (isset($_REQUEST['redirect_to']) && $_REQUEST['redirect_to']!=""){
	   $url =$_REQUEST['redirect_to'];
	}
	$url = fb_get_option('siteurl') . "?fbconnect_action=fbconnect_redirect&urlredirect=".urlencode($url);
	$loginbutton = "medium";
	$fb_hide_edit_profile = true;
	$fb_hide_invite_link = true;
	$fb_breaklinesprofile = "off";
	$hidelogintext = true;
	$fb_mainavatarsize = 30;
	include FBCONNECT_PLUGIN_PATH."/fbconnect_widget_login.php";
	
	echo "</div>";	
	?>
	
	<style>
	.fbTabs{
		display:none;
	}
	</style>
	<script type="text/javascript">
		var fb_redirect_login_url = "<?php echo $url;?>";
	</script>
	</div>
<?php
	WPfbConnect_Logic::fbconnect_init_scripts();
}
endif;

/* TODO: Facebook tags
 * 
 */
if(!function_exists('add_fb_tinymce_plugin')):
function add_fb_tinymce_plugin($plugin_array) {
   $plugin_array['fbconnect'] = FBCONNECT_PLUGIN_URL.'/editor_plugin.js';
   return $plugin_array;
}
endif;


/*add_action( 'admin_head', 'fb_admin_head' );

if(!function_exists('fb_admin_head')):
	function fb_admin_head(){
		echo '<link rel="stylesheet" href="'.fb_get_option('siteurl').'/'.WPINC.'/js/thickbox/thickbox.css" type="text/css" media="screen" />';
	}
endif;
*/

add_action('admin_head', array( 'WPfbConnect_Interface', 'style'),1);
add_action('admin_footer', array( 'WPfbConnect_Logic', 'fbconnect_init_scripts'), 1);

add_action( 'wp_footer', array( 'WPfbConnect_Logic', 'fbconnect_init_scripts'), 1);

if(!function_exists('carga_template')):
function carga_template() {
	
	if (isset($_REQUEST['fbconnect_action']) && $_REQUEST["fbconnect_action"]!="postlogout"){
		//set_include_path( TEMPLATEPATH . PATH_SEPARATOR . dirname(__FILE__) .PATH_SEPARATOR. WP_PLUGIN_DIR.'/'.FBCONNECT_PLUGIN_BASENAME. PATH_SEPARATOR . get_include_path() );   
		if($_REQUEST['fbconnect_action']=="fbconnect_ajax"){
		    
			include( FBCONNECT_PLUGIN_PATH.'/fbconnect_ajax.php');
			exit;
            
		}else if($_REQUEST['fbconnect_action']=="fbconnect_redirect"){
		    
			include( FBCONNECT_PLUGIN_PATH.'/fbconnect_redirect.php');
			exit;
            
		}else if($_REQUEST['fbconnect_action']=="community"){
		    
			include( FBCONNECT_PLUGIN_PATH.'/community.php');
            
		}else if($_REQUEST['fbconnect_action']=="redirectoauth"){
		    
            WPfbConnect_Logic::outhRedirectURL($_REQUEST['redirectoauth_net']);
			
		}else if($_REQUEST['fbconnect_action']=="delete_user"){
		    
			fb_delete_user();
            
		}else if($_REQUEST['fbconnect_action']=="register"){
		    
			include( FBCONNECT_PLUGIN_PATH.'/fbconnect_register.php');
            
		}else if($_REQUEST['fbconnect_action']=="register_update"){
		    
			WPfbConnect_Logic::register_update();
			wp_redirect( fb_get_option('siteurl') );
			
		}else if($_REQUEST['fbconnect_action']=="myhome"){
		    
			include( FBCONNECT_PLUGIN_PATH.'/myhome.php');
            
		}else if($_REQUEST['fbconnect_action']=="sidebar"){
		    
			include(FBCONNECT_PLUGIN_PATH.'/fbconnect_sidebar.php');
            
		}else if($_REQUEST['fbconnect_action']=="tab"){
		    
			include(FBCONNECT_PLUGIN_PATH.'/fbconnect_tab.php');
            
		}else if($_REQUEST['fbconnect_action']=="invite"){
		    
			include(FBCONNECT_PLUGIN_PATH.'/invitefriends.php');
            
		}else if($_REQUEST['fbconnect_action']=="userpages"){
			include(FBCONNECT_PLUGIN_PATH.'/fbUserPages.php');
		}else if($_REQUEST['fbconnect_action']=="updatecomments"){
			WPfbConnect_Logic::update_comments();
			exit;
		}else if($_REQUEST['fbconnect_action']=="logout"){
			fb_clearAllPersistentData();
			setcookie('fbsr_' . get_appId(), '', time()-3600, '/', '.'.$_SERVER['SERVER_NAME']);
			if(function_exists('wp_logout')){
				wp_logout();
				//wp_clear_auth_cookie();
			}
			
			if (isset($_SESSION) && isset($_SESSION["sjallcaps"]) ){
				$_SESSION["sjallcaps"] ="";
			}

			session_destroy();
			
			if(function_exists('wp_redirect')){
				if (isset($_SERVER["REQUEST_URI"]) && $_SERVER["REQUEST_URI"]!=""){
					//$urlredir = WPfbConnect_Logic::add_urlParam(fb_get_option('siteurl').$_SERVER["REQUEST_URI"],"fbconnect_action=postlogout");
					$protocol = "http";
					if (is_ssl()){
						$protocol = "https";
					}
					$urlredir = WPfbConnect_Logic::add_urlParam($protocol.'://'. $_SERVER['HTTP_HOST']. $_SERVER['REQUEST_URI'],"fbconnect_action=postlogout");
				}else{
					$urlredir = WPfbConnect_Logic::add_urlParam(fb_get_option('siteurl'),"fbconnect_action=postlogout") ;
				}
				$urlredir = apply_filters('fb_logoutredirecturl', $urlredir);
				wp_redirect( $urlredir );
			}
		}else if($_REQUEST['fbconnect_action']=="checksession"){
			$fb_user = fb_get_loggedin_user();
			$user = wp_get_current_user();
			$response = array();
			$response["fbuser"] = $fb_user;
			$response["wpuser"] = $user->ID;
			if ($user->ID!=0){
				$response["user_email"] = $user->user_email;
				$response["display_name"] = $user->display_name;
			}
			$response = apply_filters('fb_checksession_action', $response);
			echo json_encode($response);
		}else if($_REQUEST['fbconnect_action']=="mainimage"){
			WPfbConnect_Interface::fbconnect_img_selector($_REQUEST["postid"]);		
		}

		//restore_include_path();
		exit;
	}
}
endif;
add_action('template_redirect', 'carga_template');

if(!function_exists('fb_delete_user')):
function fb_delete_user() {

	include_once( ABSPATH.'wp-admin/includes/user.php');
	$url = fb_get_option('siteurl');
	if ( current_user_can('subscriber')){
		/*Reasign posts
		 * $wp_user_search = new WP_User_Search( '', '', "admin");
		$admin = "";
		if ($wp_user_search!="" && $wp_user_search->results!=""){
			$admin = $wp_user_search->results[0];
		}*/
		$user = wp_get_current_user();
		$fb_user = fb_get_loggedin_user();
		
		$url .= "?fbconnect_action=postlogout";
		if (  isset($user) && $user!=""){
			wp_delete_user( $user->ID, $admin );
			if(function_exists('wp_logout')){
				wp_logout();
			}
			//$url =  wp_logout_url( fb_get_option('siteurl') ); 
			
		}
		
	}
	wp_redirect( $url);
}
endif;			
/**
 * If the current comment was submitted with FacebookConnect, return true
 * useful for  <?php echo ( is_comment_fbconnect() ? 'Submitted with FacebookConnect' : '' ); ?>
 */
if(!function_exists('is_comment_fbconnect')):
function is_comment_fbconnect() {
	global $comment;
	return ( $comment->fbconnect == 1 );
}
endif;

/**
 * If the current user registered with FacebookConnect, return true
 */
if(!function_exists('is_user_fbconnect')):
function is_user_fbconnect($id = null) {
	global $current_user;
    $user = $current_user;
	if ($id != null) {
		$user = get_userdata($id);
	}
	if($user!=null && $user->fbconnect_userid){
		return true;
	}else{
		return false;
	}
}
endif;


$fb_unload_widgets = fb_get_option('fb_unload_widgets');

if 	($fb_unload_widgets==""){
	$fb_unload_widgets = array();
}	
						
foreach ($fbconnect_widgets as $keywidget=>$widgetname){
	if( !in_array($keywidget,$fb_unload_widgets) ){
		require_once(FBCONNECT_PLUGIN_PATH.'/Widget_'.$keywidget.'.php');
	}
}

/*require_once(FBCONNECT_PLUGIN_PATH.'/Widget_FacebookConnector.php');
require_once(FBCONNECT_PLUGIN_PATH.'/Widget_FanBox.php');
require_once(FBCONNECT_PLUGIN_PATH.'/Widget_LastFriends.php');
require_once(FBCONNECT_PLUGIN_PATH.'/Widget_FriendsFeed.php');
require_once(FBCONNECT_PLUGIN_PATH.'/Widget_CommentsFeed.php');
require_once(FBCONNECT_PLUGIN_PATH.'/Widget_LastUsers.php');
require_once(FBCONNECT_PLUGIN_PATH.'/Widget_ActivityRecommend.php');
require_once(FBCONNECT_PLUGIN_PATH.'/Widget_Recommend.php');*/



//Update plugin
$api_url = 'http://www.sociable.es/api/';
$plugin_slug = basename(dirname(__FILE__));

	
// Uncomment only for testing
//set_site_transient('update_plugins', null);


add_action( 'init' , 'reserved_term_intercept' );

function reserved_term_intercept(){
	if( isset( $_GET["code"] ) ){
		$newget = array();
		foreach($_GET as $key=>$value){
			if ($key=="code"){
				$newget["fbcode"] = $value;
			}else{
				$newget[$key] = $value;
			}
			
		}
		$_GET = $newget;
	}

 }

// Take over the update check
add_filter('pre_set_site_transient_update_plugins', 'fb_check_for_plugin_update');

function fb_check_for_plugin_update($checked_data) {
	$api_url = "http://www.sociable.es/api/";
	$plugin_slug = "fbconnect";
	//print_r($checked_data);
	$args = array(
		'slug' => $plugin_slug,
		'version' => $checked_data->checked['fbconnect/fbConnectCore.php'],
	);
	$request_string = array(
			'body' => array(
				'action' => 'basic_check', 
				'request' => serialize($args),
				'api-key' => md5(get_bloginfo('url'))
			),
			'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
		);
	
	// Start checking for an update
	$raw_response = wp_remote_post($api_url, $request_string);

	if (!is_wp_error($raw_response) && ($raw_response['response']['code'] == 200))
		$response = unserialize($raw_response['body']);
	
	if (is_object($response) && !empty($response)) // Feed the update data into WP updater
		$checked_data->response['fbconnect/fbConnectCore.php'] = $response;
	//print_r($checked_data);
	
	return $checked_data;
}

// Take over the Plugin info screen
add_filter('plugins_api', 'fb_plugin_api_call', 1000, 3);

function fb_plugin_api_call($def, $action, $args) {
	$api_url = "http://www.sociable.es/api/";
	$plugin_slug = "fbconnect";

	if ($args->slug != $plugin_slug)
		return false;
	
	// Get the current version
	$plugin_info = get_site_transient('update_plugins');
	$current_version = $plugin_info->checked['fbconnect/fbConnectCore.php'];
	$args->version = $current_version;
	
	$request_string = array(
			'body' => array(
				'action' => $action, 
				'request' => serialize($args),
				'api-key' => md5(get_bloginfo('url'))
			),
			'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
		);
	
	$request = wp_remote_post($api_url, $request_string);
	
	if (is_wp_error($request)) {
		$res = new WP_Error('plugins_api_failed', __('An Unexpected HTTP Error occurred during the API request.</p> <p><a href="?" onclick="document.location.reload(); return false;">Try again</a>'), $request->get_error_message());
	} else {
		$res = unserialize($request['body']);
		
		if ($res === false)
			$res = new WP_Error('plugins_api_failed', __('An unknown error occurred'), $request['body']);
	}

	return $res;
}
?>