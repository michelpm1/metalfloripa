<?php
/**
 * @author: Javier Reyes Gomez (http://www.sociable.es)
 * @date: 05/10/2008
 * @license: GPLv2
 */

if (!class_exists('WPpConnect_Logic')):

/**
 * Basic logic for paypal.
 */
class WPpConnect_Logic {


function init(){
	/*$this->paypaloauthclient = new apiClient();
	$this->paypaloauthclient->setApplicationName("Sociable Wordpress Plugin");
	$this->paypaloauthclient->addService("userprofile", "v1");
    $this->paypaloauthclient->addService("useremail", "v1");
	$this->paypaloauthclient->addService("plus", "v1");
	$this->paypalplus = new apiPlusService($this->paypaloauthclient);	*/
}	

function getRedirectUrl(){
		$siteUrl= fb_get_option('siteurl');
		if (!WPfbConnect_Logic::endsWith("/",$siteUrl)){
			$siteUrl = $siteUrl . "/";
		}
		$siteUrl = WPfbConnect_Logic::add_urlParam($siteUrl,"oauth_login=paypal");	
		return $siteUrl;
}


function user_getInfo(){
	  //$getprofileurl = "https://api.paypal.com/v1/identity/openidconnect/userinfo/";
	 $getprofileurl = "https://api.sandbox.paypal.com/v1/identity/openidconnect/userinfo/";

	  
	  $profile_url = sprintf("%s?schema=openid&access_token=%s",
                               $getprofileurl,
                               $_SESSION['access_token']);

       $meprofile = json_decode(WPpConnect_Logic::run_curl($profile_url));

	  $userinfo = array();
	  $urluserid = "https://www.paypal.com/webapps/auth/identity/user/";
	  
	  $userinfo['uid'] = substr($meprofile->user_id,strlen($urluserid));
	  //$userinfo['username'] = $meprofile->email;
	  $userinfo['website'] = "";
	  $userinfo['about_me'] = "";
	  $userinfo['email'] = $meprofile->email;
	  $userinfo['email_verified'] = $meprofile->email_verified;
	  $userinfo['profile_url'] = "";
	  $userinfo['name'] = $meprofile->name;
	  $userinfo['first_name'] = $meprofile->given_name;
	  $userinfo['last_name'] = $meprofile->family_name;
	  $userinfo['sex'] = $meprofile->gender;
	  $userinfo['birthday'] = $meprofile->birthday;
	  $userinfo['locale'] = $meprofile->locale;
	  return $userinfo;

}

function createAuthUrl(){
	//$authpaypalurl = "https://www.paypal.com/webapps/auth/protocol/openidconnect/v1/authorize";
	$authpaypalurl = "https://sandbox.paypal.com/webapps/auth/protocol/openidconnect/v1/authorize";
	$clientid = fb_get_option('p_appId');
	$scopes = "openid profile email address";
	$callback = WPpConnect_Logic::getRedirectUrl();
	$auth_url = sprintf("%s?client_id=%s&response_type=code&scope=%s&redirect_uri=%s&nonce=%s",
                            $authpaypalurl,
                            $clientid,
                            $scopes,
                            urlencode($callback),
                            time() . rand()
                            );
            
     return $auth_url;

}

function run_curl($url, $method = 'GET', $postvals = null){
    $ch = curl_init($url);
    
    //GET request: send headers and return data transfer
    if ($method == 'GET'){
        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => false
        );
        curl_setopt_array($ch, $options);
    //POST / PUT request: send post object and return data transfer
    } else {
        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_POST => 1,
            CURLOPT_VERBOSE => 1,
            CURLOPT_POSTFIELDS => $postvals,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => false
        );
        curl_setopt_array($ch, $options);
    }
    
    $response = curl_exec($ch);

    curl_close($ch);
    
    return $response;
}

public function get_access_token(){
	//$authtokenpaypalurl = "https://api.paypal.com/v1/identity/openidconnect/tokenservice";
	$authtokenpaypalurl = "https://api.sandbox.paypal.com/v1/identity/openidconnect/tokenservice";
	$clientid = fb_get_option('p_appId');
	$secret = fb_get_option('p_api_secret');
    $code = $_GET['code'];
    $postvals = sprintf("client_id=%s&client_secret=%s&grant_type=authorization_code&code=%s",
            $clientid,
            $secret,
            $code);

  	$response = WPpConnect_Logic::run_curl($authtokenpaypalurl, "POST", $postvals);

    $token = json_decode($response);
 
       /* $this->access_token = $token->access_token;
        $this->refresh_token = $token->refresh_token;
        $this->id_token = $token->id_token;*/
 
    return $token;
}

function wp_login() {
		global $wp_version,$new_fb_user;

		if ( isset($_REQUEST["fbconnect_action"]) && ($_REQUEST["fbconnect_action"]=="delete_user" || $_REQUEST["fbconnect_action"]=="postlogout" || $_REQUEST["fbconnect_action"]=="logout")){
			return;
		}
		
		$self = basename( $GLOBALS['pagenow'] );
	
			
		$user = wp_get_current_user();
		if (isset($user) && $user->ID==0){
			$user = "";	
		}
		
		if ( isset($_GET['oauth_login']) && $_GET['oauth_login']=="paypal" && (!is_user_logged_in() || $user->fbconnect_userid == "" || $user->fbconnect_userid == 0)) { //Intenta hacer login estando registrado en facebook
			if (isset($_GET['code'])) {
			  $token = WPpConnect_Logic::get_access_token();
			  $_SESSION['access_token'] = $token->access_token;
			  //header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
			}
			if (isset($_SESSION['access_token'])) {
			   //echo "Tenemos el token".$_SESSION['access_token'];
				
				require_once(ABSPATH . WPINC . '/registration.php');
				$usersinfo = $this->user_getInfo();

				if (!isset($usersinfo) || $usersinfo["uid"]==""){
					WPfbConnect::log("[fbConnectLogic::wp_login_fbconnect] fb_user_getInfo ERROR: ".$fb_user,FBCONNECT_LOG_ERR);
					return;	
				}
				$fb_user = $usersinfo["uid"];
				$_SESSION["facebook_usersinfo"] = $usersinfo;
				$_SESSION["fbconnect_netid"] = "paypal";

				$wpid = "";
				$fbwpuser = WPfbConnect_Logic::get_userbyFBID($usersinfo["uid"],"paypal");
				if ($fbwpuser =="" && $usersinfo["email"]!=""){
					$fbwpuser = WPfbConnect_Logic::get_userbyEmail($usersinfo["email"]);
				}

				//echo "LEER:".$fb_user;
				//print_r($fbwpuser);
				$wpid = "";
				$new_fb_user= false;
		
				if(is_user_logged_in() && $fbwpuser && $user->ID==$fbwpuser->ID && ($user->fbconnect_userid =="" || $user->fbconnect_userid =="0")){ // Encuentra por email el usuario y no está asociado al de FB
					WPfbConnect_Logic::set_userid_fbconnect($user->ID,$fb_user,"paypal");
					$wpid = $user->ID;
				}else if(FBCONNECT_CANVAS!="web" && is_user_logged_in() && $fbwpuser && $user->ID != $fbwpuser->ID){ // El usuario FB está asociado a un usaurio WP distinto al logeado
					$wpid = $fbwpuser->ID;
				}elseif(is_user_logged_in() && !$fbwpuser && ($user->fbconnect_userid =="" || $user->fbconnect_userid =="0")){ // El usuario WP no está asociado al de FB
					WPfbConnect_Logic::set_userid_fbconnect($user->ID,$fb_user,"paypal");
					$wpid = $user->ID;
				}elseif (!is_user_logged_in() && $fbwpuser && ($fbwpuser->fbconnect_userid =="" || $fbwpuser->fbconnect_userid =="0")){
					WPfbConnect_Logic::set_userid_fbconnect($fbwpuser->ID,$fb_user,"paypal");
					$wpid = $fbwpuser->ID;	
				}elseif(!is_user_logged_in() && $fbwpuser && ($fbwpuser->fbconnect_userid ==$fb_user)){
					$wpid = $fbwpuser->ID;	
				}elseif (($fbwpuser && $fbwpuser->fbconnect_userid != $fb_user) || (!is_user_logged_in() && !$fbwpuser) || (!$fbwpuser && is_user_logged_in() && $user->fbconnect_userid != $fb_user)){
					if(isset($usersinfo) && $usersinfo!=""){
						$username = WPfbConnect_Logic::fbusername_generator($usersinfo["uid"],$usersinfo['username'],$usersinfo["first_name"],$usersinfo["last_name"],"P");
		
						$user_data = array();
						$user_data['user_login'] = $username;
						
						$user_data['user_pass'] = substr( md5( uniqid( microtime() ).$_SERVER["REMOTE_ADDR"] ), 0, 15);
	
						$user_data['user_nicename'] = $username;
						$user_data['display_name'] = $usersinfo["name"];
	
						$user_data['user_url'] = $usersinfo["profile_url"];
						//$user_data['user_email'] = $usersinfo["proxied_email"];
						$user_data['user_email'] = "";
						
						if ($usersinfo["email"]!=""){
							$user_data['user_email'] = $usersinfo["email"];
						}
						//Permitir email en blanco y duplicado
						define ( 'WP_IMPORTING', true);

						$wpid = wp_insert_user($user_data);
						if ( !is_wp_error($wpid) ) {
							update_usermeta( $wpid, "first_name", $usersinfo["first_name"] );
							update_usermeta( $wpid, "last_name", $usersinfo["last_name"] );
	
							if (isset($usersinfo["sex"]) && $usersinfo["sex"] != ""){
								update_usermeta( $wpid, "sex", $usersinfo["sex"] );
							}
							WPfbConnect_Logic::set_userid_fbconnect($wpid,$fb_user,"paypal");
							$new_fb_user= true;
						}else{ // no ha podido insertar el usuario
							return;
						}
					}
					
				}else{
					return;
				}
	
				$userdata = WPfbConnect_Logic::get_userbyFBID($fb_user,"paypal");
				WPfbConnect_Logic::set_lastlogin_fbconnect($userdata->ID);
				global $current_user;
	
				$current_user = null;
				
	
				WPfbConnect_Logic::fb_set_current_user($userdata);
	
				global $userdata;
				if (isset($userdata) && $userdata!=""){
					$userdata->fbconnect_userid = $fb_user;
					$userdata->fbconnect_netid = "paypal";
				}
				header('Content-type: text/html');
				if ($new_fb_user){
					$new_fb_usertxt = "true";
				}else{
					$new_fb_usertxt = "false";
				}
				$terms = get_user_meta($userdata->ID, "terms", true);
				?>
				<html>
				<body>
				<script>
					window.opener.fb_refreshlogininfo('paypal','<?php echo $fb_user;?>','<?php echo $userdata->ID;?>',<?php echo $new_fb_usertxt;?>,'<?php echo $terms;?>');
					window.close();
				</script>
				</body>
				</html>
				<?php
				exit;
				
			}else{
				header('Content-type: text/html');
				echo "ERROR NO ACCESS TOKEN";
				exit;
			}
		}
	}
	
} 
endif; // end if-class-exists test

global $pLogic;
$pLogic = new WPpConnect_Logic;
$pLogic->init();
?>
