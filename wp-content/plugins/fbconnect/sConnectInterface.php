<?php
/**
 * @author: Javier Reyes Gomez (http://www.sociable.es)
 * @date: 05/10/2008
 * @license: GPLv2
 */

		global $wp_version, $fbconnect,$fb_reg_formfields;

			if ( isset($_POST['info_update']) ) {
				check_admin_referer('wp-fbconnect-info_update');

				$error = '';
				update_option( 's_login_enabled', $_POST['s_login_enabled'] );
				update_option( 's_permsToRequestOnConnect', $_POST['s_permsToRequestOnConnect'] );
				update_option( 's_api_key', $_POST['s_api_key'] );
				update_option( 's_appId', $_POST['s_appId'] );
				update_option( 's_api_secret', $_POST['s_api_secret'] );
				
				if ($error !== '') {
					echo '<div class="error"><p><strong>'.__('At least one of Spotify Connector options was NOT updated', 'fbconnect').'</strong>'.$error.'</p></div>';
				} else {
					echo '<div class="updated"><p><strong>'.__('Spotify Connector options updated', 'fbconnect').'</strong></p></div>';
				}

			
			}
			
			// Display the options page form
			$siteurl = fb_get_option('home');
			if( substr( $siteurl, -1, 1 ) !== '/' ) $siteurl .= '/';
			?>
			<div class="wrap">
				<h2>
					<img src="<?php echo FBCONNECT_PLUGIN_URL;?>/images/spotify-20.png"/>
					<?php _e('Spotify Configuration', 'fbconnect') ?></h2>

				<form method="post">


					<h3><?php _e('Spotify Application Configuration', 'fbconnect') ?></h3>
     				<table class="form-table" cellspacing="2" cellpadding="5" width="100%">
     					<tr valign="top">
							<th style="width: 33%" scope="row"><?php _e('Enable Spotify login:', 'fbconnect') ?></th>
							<td>
								<p><input type="checkbox" name="s_login_enabled" id="s_login_enabled" <?php
								if( fb_get_option('s_login_enabled')) echo 'checked="checked"'
								?> />
							</td>
						</tr>
						<tr valign="top">
							<th style="width: 33%" scope="row"><label for="s_permsToRequestOnConnect"><?php _e('Perms to request:', 'fbconnect') ?></label></th>
							<td>
							<input type="text" name="s_permsToRequestOnConnect" id="s_permsToRequestOnConnect" size="50" value="<?php echo fb_get_option('s_permsToRequestOnConnect');?>"/>
							<?php _e('Perms to request, comma separated list: (user-read-email,user-read-private,user-library-modify,user-library-read,streaming,playlist-modify-private,playlist-modify-public,playlist-read-private).', 'fbconnect') ?>
							</td>
						</tr>
						<tr valign="top">
							<th style="width: 33%" scope="row"><?php _e('Spotify App. Config.', 'fbconnect') ?></th>
							<td>
							<a href="https://developer.spotify.com/my-applications/" target="_blank"><?php _e('Create a new Spotify Application', 'fbconnect') ?></a><br/>
							<br/><?php _e('Your Spotify redirect URI (copy and paste in the Spotify Application configuration):', 'fbconnect'); ?>
							<br/><input type="text" name="s_callback" id="s_callback" size="50" value="<?php echo WPsConnect_Logic::getRedirectUrl();?>"/>
							</td>
						</tr>

						<tr valign="top">
							<th style="width: 33%" scope="row"><label for="s_appId"><?php _e('Spotify client ID:', 'fbconnect') ?></label></th>
							<td>
							<input type="text" name="s_appId" id="s_appId" size="50" value="<?php echo fb_get_option('s_appId');?>"/>
							</td>
						</tr>
						<tr valign="top">
							<th style="width: 33%" scope="row"><label for="s_api_secret"><?php _e('Spotify client secret:', 'fbconnect') ?></label></th>
							<td>
							<input type="text" name="s_api_secret" size="50" id="s_api_secret" value="<?php echo fb_get_option('s_api_secret');?>"/>
							</td>
						</tr>							

     				</table>

					
					<?php wp_nonce_field('wp-fbconnect-info_update'); ?>
					
     				<p class="submit"><input class="button-primary" type="submit" name="info_update" value="<?php _e('Update Configuration', 'fbconnect') ?> &raquo;" /></p>
     			</form>
				
			</div>
    			