function show_feed(template_id,data,callback,user_message_prompt,user_message_txt){
		user_message = {value: user_message_txt};
		FB.Connect.showFeedDialog(template_id, data,null, null, null , FB.RequireConnect.promptConnect,callback);
}
var fb_refreshURL = unescape(window.location.pathname);

function fb_add_urlParam(url,param){
	if (param != ""){
		pos = url.indexOf("?",0);
		if (pos == -1) {
			return url + "?" + param;
		}else{
			return url + "&" +param;
		}
	}else{
		return url;
	}

}

function set_fb_redirect_login_url(url){
	fb_redirect_login_url = url;
}

function login_facebook(){
	if (fb_track_events){
		var trackvalue; 
		if (fb_status=="connected"){
			trackvalue = 0;
		}else if(fb_status=="notConnected") {
			trackvalue = 1;
		}else{
			trackvalue = 2;
		}
		if (typeof(pageTracker)!=='undefined'){
			pageTracker._trackEvent('Facebook','login', fb_pageurl,trackvalue);
		}else if(typeof(_gaq)!=='undefined'){
			_gaq.push(['_trackEvent', 'Facebook','login', fb_pageurl,trackvalue]);
		}
	}
	
	response = FB.getAuthResponse();
	if (response && response.signedRequest!="") {
        //sessionFacebook+="&session="+JSON.stringify(response.session);
		sessionFacebook = "signed_request="+response.signedRequest;
		if (fb_canvas!="web"){
			document.location = fb_add_urlParam(fb_pageurl,sessionFacebook);
		}else{
	        document.location = fb_pageurl;
		}
	}   
	/*if(url!=null && url!=""){
		document.location = fb_add_urlParam(url,sessionFacebook);;	
	}else{
		if (fb_signed_request!=""){
			top.location = fb_canvas_url;
		}else{
			//window.location.reload(true);
			url = document.location;
			//document.location = url.replace("?fbconnect_action=postlogout", "")+sessionFacebook;
			document.location = fb_add_urlParam(fb_pageurl,sessionFacebook);
		}
	}*/

}

function login_facebookjs(urlredirect){
	 FB.login(function(response) {
		   var trackvalue;
		   if (response.authResponse) {
		   		if (typeof(fb_redirect_login_url) != "undefined" && fb_redirect_login_url!=""){
					document.location = fb_redirect_login_url;	
				}else{
				   trackvalue = "login";
				   if (urlredirect==""){
					   urlredirect = fb_pageurl;
				   }
					
				   fb_signed_request = response.authResponse.signedRequest;
				   fb_userid = response.authResponse.userID;
				   sessionFacebook = "signed_request="+response.authResponse.signedRequest;
					if (fb_canvas!="web"){
						document.location = fb_add_urlParam(urlredirect,sessionFacebook);
					}else{
				        document.location = urlredirect;
					}
				}
		   } else {
			   trackvalue = "cancellogin";
		     //alert('User cancelled login or did not fully authorize.');
		   }
			if (fb_track_events){
				if (typeof(pageTracker)!=='undefined'){
					pageTracker._trackEvent('Facebook',trackvalue, fb_pageurl);
				}else if(typeof(_gaq)!=='undefined'){
					_gaq.push(['_trackEvent', 'Facebook',trackvalue, fb_pageurl]);
				}
			}
		 }, {scope: fb_requestperms});
}

function login_fan(pageid,postid,thick){
	 jQuery('#fanboxlike').fadeOut('slow');
	 FB.login(function(response) {
		   var trackvalue;
		   if (response.authResponse) {
			   fb_signed_request = response.authResponse.signedRequest;
			   fb_userid = response.authResponse.userID;
			   
			   sessionFacebook = "signed_request="+response.authResponse.signedRequest;
				/*if (fb_canvas!="web"){
					document.location = fb_add_urlParam(fb_pageurl,sessionFacebook);
				}else{
			        document.location = fb_pageurl;
				}*/
			   //document.location = fb_add_urlParam(fb_pageurl,sessionFacebook);
			   isfanfbpage(pageid,postid,fb_userid,sessionFacebook,thick);
				
		   } else {
			   jQuery('#fanboxlike').fadeIn('slow');
		   }
		 }, {scope: fb_requestperms});
}

var fb_processing_login_facebook;
function login_facebook2(reload){
	FB.login(function(response) {
		   var trackvalue;
		   if (response.authResponse) {
			   trackvalue = "login";
			   fb_netid="facebook";

				sessionFacebook = "signed_request="+response.authResponse.signedRequest;
				fb_signed_request = response.authResponse.signedRequest;
				fb_userid = response.authResponse.userID;
				jQuery('.fbconnect_widget_divclass').fadeOut('fast');
				var fbimgparams = "";
				if (typeof(window["maxlastusers"]) != "undefined"){
					fbimgparams = '&maxlastusers='+maxlastusers+'&avatarsize='+avatarsize;
				}
				
				fbimgparams = '&fbclientuser='+FB.getUserID();
				fbimgparams = fbimgparams + "&" + sessionFacebook;
				href = fb_add_urlParam(fb_ajax_url,"useajax="+sjws_useajaxcontent);
				href = fb_add_urlParam(href,"fbajaxlogin=facebook");
				href = fb_add_urlParam(href,"refreshpage="+reload);
				href = fb_add_urlParam(href,fbimgparams);
				href = fb_add_urlParam(href,"smpchanneltype="+fb_chaneltype);
				href = fb_add_urlParam(href,"fbpostid="+fb_postid);
				if(sessionPHP!=""){
					href = fb_add_urlParam(href,sessionPHP);
				}

				fb_processing_login_facebook = false;
				jQuery('.fbconnect_widget_divclass').load(href,function(){
					
					if (!fb_processing_login_facebook){
						fb_processing_login_facebook = true;
						if (typeof(fb_redirect_login_url) != "undefined" && fb_redirect_login_url!=""){
								if(typeof(fb_redirect_login_url_thick) != "undefined" && fb_redirect_login_url_thick=="thickbox"){
									tb_show('', fb_redirect_login_url, null);
								}else if(typeof(fb_redirect_login_url_thick) != "undefined" && fb_redirect_login_url_thick=="jscallback"){
									fb_redirect_login_url(fb_userid,wp_userid,fb_isNewUser,fb_user_terms); 
								}else{
									document.location = fb_redirect_login_url;
								}	
						}else if (fb_show_reg_form=="on" && (fb_isNewUser || (fb_reg_form_terms=="y" && fb_user_terms!="y"))){
							if(fb_regform_url==""){
								tb_show("Registration", fb_root_siteurl+ "?fbconnect_action=register&height=390&width=435", "");
							}else{
								document.location = fb_root_siteurl + fb_regform_url;
							}
						}else if (reload){
								document.location = fb_pageurl;
						}
					}
					
						jQuery('.fbconnect_commentsloginclass .fbTabs').remove();
						fb_showTab('fbFirst');
						jQuery(".fbconnect_commentslogin").show();
						jQuery(this).fadeIn('slow');
						jQuery("#commentform #author").val(wp_username);
						jQuery("#commentform #email").val(wp_useremail);
						jQuery("#fbconnectcheckpublish").addClass("fbconnect_sharewith_facebook");
						jQuery("#fbconnectcheckpublish").show();
				
				});
				
		   } else {
			   trackvalue = "cancellogin";
		     //alert('User cancelled login or did not fully authorize.');
		   }
		   if (fb_track_events){
				if (typeof(pageTracker)!=='undefined'){
					pageTracker._trackEvent('Facebook',trackvalue, fb_pageurl);
				}else if(typeof(_gaq)!=='undefined'){
					_gaq.push(['_trackEvent', 'Facebook',trackvalue, fb_pageurl]);
				}
			}
		 }, {scope: fb_requestperms});	
}

function fb_checkerrormsg(data,dataType,showcontainer){
	
	if(typeof(showcontainer)=='undefined'){
		showcontainer = "";
	}

	if(showcontainer == "" && typeof(fb_errormsgcontainer)!=='undefined'){
		showcontainer = fb_errormsgcontainer;
	}
	var errormsg;
	if (dataType=="html"){
		if (data.indexOf('<span id="errormsg"') != -1){
			errormsg = data;
		}
  		//errormsg = jQuery(data).filter('#errormsg');
  	}else{
  		errormsg = data["errormsg"];	
  	}

	if (typeof(errormsg)!=='undefined' && errormsg.length!=0){
		if(showcontainer!=""){
			jQuery(showcontainer).html(errormsg);
			jQuery(showcontainer).show("slow");
		}else{
			alert(errormsg);
		}
		return 0;
	}else{
		var infomsg;
		if (dataType=="html"){
			if (data.indexOf('<span id="infomsg"') != -1){
				infomsg = data;
			}
			//infomsg = jQuery(data).filter('#infomsg');
		}else{
			infomsg = data["infomsg"];
		}
		if (typeof(infomsg)!=='undefined' && infomsg.length!=0){
			if(showcontainer!=""){
				jQuery(showcontainer).html(infomsg);
				jQuery(showcontainer).show("slow");
			}else{
				alert(infomsg);
			}
			return 1;
		}
		return 2;
	}
}

function fb_msgdialog(title,dialoghtml,showclose,showacept,callback,width,height){
	jQuery("#TB_ajaxContent").html("");
	var botonera = "";
	if (width=="" || typeof(width) == "undefined"){
		width = 400;
	}
	if (height=="" || typeof(height) == "undefined"){
		height = 190;
	}
	if (callback=="" || typeof(callback) == "undefined"){
		callback = "fb_callbackdialogmsg";
	}

	if (showclose || showacept){
		botonera = '<div class="fbdialogbuttons" id="fbdialogbuttons">';
		if (showacept){
			botonera += '<input type="button" onclick="tb_remove();'+callback+'(1);" value="'+fbconnect_data.accept+'" name="fbbuttonconfirm" class="wsbutton button">';
		}
		if (showclose){
			botonera += '<input type="button" onclick="tb_remove();'+callback+'(0);" value="'+fbconnect_data.close+'" name="fbbuttoncancel" class="wsbuttonSecondary button">';
		}
		botonera += '</div>';
	}
	dialoghtml = "<div id=\"fbmsgdialoghtml\" style=\"display:none;\"><div class=\"msgdialogcontent\"><div class=\"msgdialogbody\">"+dialoghtml+"</div>"+botonera+"</div></div>";
	jQuery("#fbmsgdialoghtml").remove();
	jQuery("body").append(dialoghtml);
	//fb_scrollto();
		
	if (title==""){
		tb_show(title, "#TB_inline?height="+height+"&width="+width+"&inlineId=fbmsgdialoghtml&modal=true", "");
	}else{
		tb_show(title, "#TB_inline?height="+height+"&width="+width+"&inlineId=fbmsgdialoghtml", "");
	}
	//jQuery("#TB_window").css({top: '50px'});
	//jQuery("#TB_window").css({marginTop: '0px'});
	fb_centerthickbox("#TB_window");

}
function fb_callbackdialogmsg(response){
	//document.location = fb_pageurl;
}
function fb_refreshloginwp(reload,errorcontainer){
	jQuery('.fbconnect_widget_divclass').fadeOut('fast');
	var fbimgparams = "";
	var realredirect = false;
	
	if (typeof(window["maxlastusers"]) != "undefined"){
		fbimgparams = '&maxlastusers='+maxlastusers+'&avatarsize='+avatarsize;
	}
	
	fbimgparams = '&fbclientuser='+FB.getUserID();
	fbimgparams = fbimgparams + "&" + sessionFacebook;
	href = fb_add_urlParam(fb_ajax_url,"useajax="+sjws_useajaxcontent);
	href = fb_add_urlParam(href,"fbajaxlogin=wordpress");
	href = fb_add_urlParam(href,"refreshpage="+reload);
	href = fb_add_urlParam(href,fbimgparams);
	href = fb_add_urlParam(href,"smpchanneltype="+fb_chaneltype);
	href = fb_add_urlParam(href,"fbpostid="+fb_postid);
	if(sessionPHP!=""){
		href = fb_add_urlParam(href,sessionPHP);
	}

    jQuery("#wp-submit").attr("disabled", "disabled");

	jQuery.ajax({
		type: "POST",
		url: href,
		data: jQuery("#fb_loginform").serialize()
		,
		  complete:function() {
			  
		  },
		  error:function() {
			  alert("error ajax");
		  },
		  success: function(data) {
		  		successcall = fb_checkerrormsg(data,"html",errorcontainer);
		  		if (successcall){
		  			jQuery('.fbconnect_widget_divclass').html(data);
					tb_remove();
						
					if (typeof(fb_redirect_login_url) != "undefined" && fb_redirect_login_url!=""){
						if(typeof(fb_redirect_login_url_thick) != "undefined" && fb_redirect_login_url_thick=="thickbox"){
							tb_show('', fb_redirect_login_url, null);
						}else if(typeof(fb_redirect_login_url_thick) != "undefined" && fb_redirect_login_url_thick=="jscallback"){
							fb_redirect_login_url(fb_userid,wp_userid,fb_isNewUser,fb_user_terms); 
						}else{
							realredirect = true;
							document.location = fb_redirect_login_url;
						}	
					}else if (fb_show_reg_form=="on" && fb_reg_form_terms=="y" && fb_user_terms!="y" ){
						if(fb_regform_url==""){
							tb_show("Registration", fb_root_siteurl+ "?fbconnect_action=register&height=390&width=435", "");
						}else{
							realredirect = true;
							document.location = fb_root_siteurl + fb_regform_url;
						}
					}else if(reload){
						realredirect = true;
						//document.location = fb_pageurl;
						document.location = fb_pageurl;
					}
					
				}else{
					jQuery("#wp-submit").removeAttr("disabled");    
				}
				if (!realredirect){
					jQuery('.fbconnect_commentsloginclass .fbTabs').remove();
					fb_showTab('fbFirst');
					jQuery(".fbconnect_commentslogin").show();
					jQuery("#commentform #author").val(wp_username);
					jQuery("#commentform #email").val(wp_useremail);
					jQuery("#fbconnectcheckpublish").addClass("fbconnect_sharewith_facebook");
					jQuery("#fbconnectcheckpublish").show();
					//jQuery(".fbconnect_widget_divclass").fadeIn('slow');
					jQuery(this).fadeIn('slow');
				}
		  }
		}
		  );
	return false;
}


function fb_registeruser(reload){
	var fbimgparams = "";
	if (typeof(window["maxlastusers"]) != "undefined"){
		fbimgparams = '&maxlastusers='+maxlastusers+'&avatarsize='+avatarsize;
	}
	
	fbimgparams = '&fbclientuser='+FB.getUserID();
	fbimgparams = fbimgparams + "&" + sessionFacebook;
	href = fb_add_urlParam(fb_ajax_url,"useajax="+sjws_useajaxcontent);
	href = fb_add_urlParam(href,"fbajaxlogin="+fb_netid);
	href = fb_add_urlParam(href,"refreshpage=false");
	href = fb_add_urlParam(href,fbimgparams);
	href = fb_add_urlParam(href,"smpchanneltype="+fb_chaneltype);
	href = fb_add_urlParam(href,"fbpostid="+fb_postid);
	
	jQuery.ajax({
		type: "POST",
		url: href,
		data: jQuery("#fbregisterform").serialize()
		,
		  complete:function() {
			  
		  },
		  error:function() {
			  alert("error ajax");
		  },
		  success: function(data) {
		  		successcall = fb_checkerrormsg(data,"html","");
		  		if (successcall){
					fb_msgdialog(" ",data,true,false);
					document.location = fb_pageurl; 
				}
		  }
		}
		  );
	return false;
}

function fb_loadaccesstab(taburl){
	jQuery(document).ready(function($) {
		jQuery('#fbaccesstable').html("Loading...");	
		$('#fbaccesstable').load(taburl,function(){
				//alert('cargado');
			});
	});
}

function fb_init_user(userid,username,useremail){
	jQuery(document).ready(function($) {
		wp_userid = userid;
		wp_username = username;
		wp_useremail = useremail;
	});
}

function logout_facebook(){
	//window.location = fb_add_urlParam(fb_pageurl,"fbconnect_action=logout&fbclientuser=0");

	if (typeof(FB)!=='undefined' && FB.getUserID()!=0){
		FB.logout(function(result) {
			window.location = fb_add_urlParam(fb_pageurl,"fbconnect_action=logout&fbclientuser=0&smpchanneltype="+fb_chaneltype);
		});	
	}else{
		window.location = fb_add_urlParam(fb_pageurl,"fbconnect_action=logout&fbclientuser=0&smpchanneltype="+fb_chaneltype);
	}
	/*FB.logout(function(result) { 
		jQuery('.fbconnect_widget_divclass').fadeOut('slow');
		jQuery('.fbconnect_widget_divclass').load(fb_ajax_url+'?fbajaxlogout=true&fbconnect_action=logout&maxlastusers='+maxlastusers+'&avatarsize='+avatarsize,function(){
			jQuery('.fbconnect_commentsloginclass .fbTabs').remove();
			jQuery(this).fadeIn('slow');
			fb_showTab('fbFirst');
			jQuery("#fbconnectcheckpublish").hide();
			jQuery(".logged-in-as").hide();
		});
	
	});
	*/

	
}


var sjws_useajaxcontent = true;
var fb_navhistory = new Array();
var fb_navpointer = -1;
var fb_currenturl = "";
var fb_make_navpush = false;
var fb_manualpushhistory = false;
var fb_tbtimer = "";

window.onpopstate = function (e) {
	if (e.state!=null && typeof(e.state.fbstate) != "undefined"){
		fb_make_navpush = true;
		fb_navpointer = e.state.fbstate;
		cargaCapaAjax.apply(this,fb_navhistory[fb_navpointer]);
	}else{
		//console.log("POP NULL");
	}
	fb_manualpushhistory = false;
};

function fb_push_navurl(args,href) {
	//console.log(args);
	fb_currenturl = href;
	fb_navpointer++;
	//console.log(fb_navpointer);
	fb_navhistory = fb_navhistory.slice(0,fb_navpointer);
	fb_navhistory.push(args);
	fb_manualpushhistory = true;

	var fbstateval = {fbstate:fb_navpointer};
	if (typeof(window.history) != "undefined" && typeof(window.history.pushState) != "undefined"){
		window.history.pushState(fbstateval,"AAAA",href);
	}
}
function fb_back_navigation(){
	if (typeof(window.history) != "undefined" && typeof(window.history.pushState) != "undefined"){
		window.history.back();
	}else if (fb_navpointer>=0){
		fb_navpointer--;
		fb_make_navpush = true;
		cargaCapaAjax.apply(this,fb_navhistory[fb_navpointer]);
	}
}
//////////////////////////////////////////////////////////////////////////////////
function fb_links_canvas(procesarnodo,callback){
	
	if( typeof(procesarnodo) != "undefined" && procesarnodo!=""){
		procesarnodo = procesarnodo +" ";
	}else{
		procesarnodo = "";
	}

		jQuery(procesarnodo+'a').click(
		function () {
			var loadinthickbox = false;
			var thehref = jQuery(this).attr("href");
			var domain = "";
			if (thehref!="" && thehref!="#" && typeof(thehref) != "undefined"){
				domain = thehref.split(/\/+/g)[1];
			}
			var target=jQuery(this).attr("target");
			var classnode=jQuery(this).attr("class");
			var scrolldata = jQuery(this).data('ajaxscroll'); 
			var targetdata = jQuery(this).data('fbtarget'); 
			var cacheurldata = jQuery(this).data('fbcacheurl'); 
			var localreload = false;
			var showloading = "";
			var errorcontainer="";
			var processlinks = true;
			var processlinksfunc ="";
			var navurldata = jQuery(this).data('fbnavigationurl'); 
			
			if (targetdata!=""){
				localreload = true;
			}
			
			var navurl = false;
			if (typeof(navurldata) != "undefined" && navurldata){
				navurl = true;
			}
			
			var scroll = true;
			if (typeof(scrolldata) != "undefined" && !scrolldata){
				scroll = false;
			}
	
			var cacheurl = false;
			if (typeof(cacheurldata) != "undefined" && cacheurldata){
				cacheurl = true;
			}
			
			var classnoprocess = true;

			if ( ( typeof(classnode) != "undefined" && classnode!="" ) && (classnode.indexOf("thickbox")!=-1 || classnode.indexOf("noprocesslink")!=-1) ){
				classnoprocess = false;
			}

			if( ( typeof(classnode) != "undefined" && classnode!="" ) && classnode.indexOf("fbthicktarget")!=-1){
				loadinthickbox = true;
			}
			if ( classnoprocess && target!="_blank" && thehref.indexOf(domain)>-1 && thehref!="#" && thehref.indexOf("channel.php")==-1 && thehref!=""){
				response = cargaCapaAjax(thehref,"",loadinthickbox,targetdata,callback,jQuery(this),"","",scroll,localreload,showloading,errorcontainer,processlinks,processlinksfunc,cacheurl,navurl);
				return false;
			}else{
				return true;
			}
		}
		);
		jQuery(procesarnodo+'form').submit(
		function () {
			var loadinthickbox = false;
			var thehref = jQuery(this).attr("action");
			var domain = thehref.split(/\/+/g)[1];
			
			var classnode=jQuery(this).attr("class");
			var target=jQuery(this).attr("target");
			var classnoprocess = true;
			var scrolldata = jQuery(this).data('ajaxscroll'); 
			var targetdata = jQuery(this).data('fbtarget'); 
			var cacheurldata = jQuery(this).data('fbcacheurl'); 

			var localreload = false;
			var showloading = "";
			var errorcontainer="";
			var processlinks = true;
			var processlinksfunc ="";
			var navurldata = jQuery(this).data('fbnavigationurl'); 
			
			var navurl = false;
			if (typeof(navurldata) != "undefined" && navurldata){
				navurl = true;
			}
			
			if (targetdata!=""){
				localreload = true;
			}
			
			var scroll = true;
			if (typeof(scrolldata) != "undefined"){
				scroll = scrolldata;
			}
			
			var cacheurl = false;
			if (typeof(cacheurldata) != "undefined"){
				cacheurl = cacheurldata;
			}
			
			if ( typeof(target) == "undefined" ){
				target = "";
			}
			if ( ( typeof(classnode) != "undefined" && classnode!="" ) && (classnode.indexOf("thickbox")!=-1 || classnode.indexOf("noprocesslink")!=-1) ){
				classnoprocess = false;
			}
			
			if(( typeof(classnode) != "undefined" && classnode!="" ) && classnode.indexOf("fbthicktarget")!=-1){
				loadinthickbox = true;
			}
			if (classnoprocess && target!="_blank" && thehref.indexOf(domain)>-1){

				response = cargaCapaAjax(thehref,jQuery(this).serialize(),loadinthickbox,targetdata,callback,jQuery(this),"","",scroll,localreload,showloading,errorcontainer,processlinks,processlinksfunc,cacheurl,navurl);
				
				return false;
			}
		}
		);


		jQuery("#fbthickbox_container").fadeOut(500,function () {
			if (typeof(FB)!=='undefined'){
				FB.Canvas.setAutoGrow(100);
			}
		});

}

function fb_loadURLAjax(url,cacheurl,navurl){
	if (typeof(navurl) == "undefined" || navurl==""){
		navurl = false;
	}
	if (typeof(cacheurl) == "undefined" || cacheurl==""){
			cacheurl = false;
	}
	var data="";
	var usethick = false;
	var procesarnodo="";
	var callback = "";
	var domobj = "";
	var title = "";
	var dataType = "";
	var changescroll = true;
	var localreload = false;
	var showloading = "true";
	var errorcontainer = "";
	var processlinks = true;
	var processlinksfunc = "";

	cargaCapaAjax(url,'',false,'','','','',dataType,changescroll,localreload,showloading,errorcontainer,processlinks,processlinksfunc,cacheurl,navurl);
}

function fb_loadURLThick(url,cacheurl,navurl){
	if (typeof(navurl) == "undefined" || navurl==""){
		navurl = false;
	}
	if (typeof(cacheurl) == "undefined" || cacheurl==""){
			cacheurl = false;
	}
	var data="";
	var usethick = true;
	var procesarnodo="";
	var callback = "";
	var domobj = "";
	var title = "";
	var dataType = "";
	var changescroll = true;
	var localreload = false;
	var showloading = "true";
	var errorcontainer = "";
	var processlinks = true;
	var processlinksfunc = "";

	cargaCapaAjax(url,'',usethick,'','','','',dataType,changescroll,localreload,showloading,errorcontainer,processlinks,processlinksfunc,cacheurl,navurl);
}

var fb_countrefresh = 0;

function cargaCapaAjax(href,data,usethick,procesarnodo,callback,domobj,title,dataType,changescroll,localreload,showloading,errorcontainer,processlinks,processlinksfunc,cacheurl,navurl) {
	//console.log(arguments);
	
	if (typeof(navurl) == "undefined"){
		navurl = false;
	}
	
	if (!fb_make_navpush && navurl){
		fb_push_navurl(arguments,href);
	}else{
		fb_make_navpush = false;
	}
	
	if (typeof(cacheurl) == "undefined"){
			cacheurl = false;
	}
	if (typeof(processlinks) == "undefined"){
		processlinks = true;
	}
	if (typeof(processlinksfunc) == "undefined" || processlinksfunc==""){
		processlinksfunc = fb_links_canvas;
	}

	if (typeof(changescroll) == "undefined"){
		changescroll = true;
	}

	if (typeof(dataType) == "undefined" || dataType==""){
		dataType = "html";
	}
	if (typeof(errorcontainer) == "undefined"){
		errorcontainer = "";
	}
	if(errorcontainer == "" && typeof(fb_errormsgcontainer)!=='undefined'){
		errorcontainer = fb_errormsgcontainer;
	}
	var fbthickbox_container = "#fbthickbox_container";
	
	if (typeof(procesarnodo) == "undefined" || procesarnodo==""){
		procesarnodo="#bodycontainer";
	}else if(typeof(localreload) != "undefined" && localreload){
		clonelayer = jQuery('#fbthickbox_container').clone();
		targetid = procesarnodo.replace("#","");
		targetid = "fbthickbox_"+targetid.replace(".","");
		clonelayer.attr("id",targetid);
		clonelayer.attr("class","fbthickbox_container_local");
		clonelayer.appendTo(procesarnodo);
		fbthickbox_container = "#"+targetid;
	}
			
	if (typeof(title) == "undefined" || title==""){
		title = "";
	}
	//href = fb_add_urlParam(href,sessionFacebook);
	//href = fb_add_urlParam(href,"useajax="+sjws_useajaxcontent);
	//href = fb_add_urlParam(href,"smpchanneltype="+fb_chaneltype);
	if (typeof(data) != "undefined" && data!="" ){
		data = data +"&";
	}
	data = data + sessionFacebook;
	if(sessionPHP!=""){
		data = data + "&" + sessionPHP;
	}

	data = data +"&useajax="+sjws_useajaxcontent;
	data = data + "&smpchanneltype="+fb_chaneltype;
	
	if (usethick){
		//fb_scrollto();
		
		if (href.indexOf("width=")<0 && typeof(fb_widththick) != "undefined"){
			href = fb_add_urlParam(href,"width="+fb_widththick);
			href = fb_add_urlParam(href,data);
		}
		
		jQuery("#TB_imageOff").unbind("click");
		jQuery("#TB_closeWindowButton").unbind("click");
		jQuery("#TB_window").remove();
		jQuery('#TB_window,#TB_overlay,#TB_HideSelect').trigger("tb_unload").unbind().remove();
		jQuery("#TB_load").remove();

		tb_show(title, href, null);

		//jQuery("#TB_window").hide();
		jQuery("#TB_window").css({
						    'position':'absolute',
						    'top': -1000,
						    'left': -1000
						});
		
		fb_tbtimer = setInterval(function(){
			fb_countrefresh++;
			//console.log(fb_countrefresh);
		if ( jQuery("#TB_load").is(":visible") && fb_countrefresh<20) {
			//alert("loading");
		}else{
			fb_countrefresh = 0;
			clearInterval(fb_tbtimer); 
			fb_centerthickbox("#TB_window",fb_widththick);
		}
		},100);
		
	}else{
		if (typeof(FB)!=='undefined'){
			FB.Canvas.setAutoGrow(false);
		}	

		if (typeof(showloading) == "undefined" || showloading=="" || showloading=="true"){
			jQuery(fbthickbox_container).fadeIn(250, function () {
				cargaCapaAjax2(fbthickbox_container,href,data,usethick,procesarnodo,callback,domobj,title,dataType,changescroll,localreload,showloading,errorcontainer,processlinks,processlinksfunc,cacheurl);
			});
		}else{
			cargaCapaAjax2(fbthickbox_container,href,data,usethick,procesarnodo,callback,domobj,title,dataType,changescroll,localreload,showloading,errorcontainer,processlinks,processlinksfunc,cacheurl);
		}
	}
}

var fb_cache_pages = new Object();

function cargaCapaAjax2(fbthickbox_container,href,data,usethick,procesarnodo,callback,domobj,title,dataType,changescroll,localreload,showloading,errorcontainer,processlinks,processlinksfunc,cacheurl) {

	jQuery(errorcontainer).hide("fast");
	//console.log(fb_cache_pages);
	
	if ( cacheurl && typeof(fb_cache_pages[encodeURIComponent(href)])!="undefined"){
		fb_cargaCapaAjaxResponse(fb_cache_pages[encodeURIComponent(href)],fbthickbox_container,href,data,usethick,procesarnodo,callback,domobj,title,dataType,changescroll,localreload,showloading,errorcontainer,processlinks,processlinksfunc);		
	}else{
		jQuery.ajax({
				type: "POST",
				url: href,
				data: data ,
				cache: false,
				dataType: dataType,
				error: function(msg){
					/*console.log(href);
					console.log(data);
					console.log(msg);*/
					//alert("No ha sido posible procesar la petición. Por favor, inténtalo más tarde.");
					jQuery(fbthickbox_container).fadeOut(500);
					if (typeof(callback) != "undefined" && callback!=""){
						callback(domobj,msg,false);
					}else{	
						throw("No ha sido posible procesar la petición. Por favor, inténtalo más tarde. "+msg); 
					}
					
				},
				success: function(msg){
					
					if (cacheurl){
						fb_cache_pages[encodeURIComponent(href)] = msg;
						//console.log(fb_cache_pages);
					}
					fb_cargaCapaAjaxResponse(msg,fbthickbox_container,href,data,usethick,procesarnodo,callback,domobj,title,dataType,changescroll,localreload,showloading,errorcontainer,processlinks,processlinksfunc);
					
					}
				});
		}
	return false;
}

function fb_cargaCapaAjaxResponse(msg,fbthickbox_container,href,data,usethick,procesarnodo,callback,domobj,title,dataType,changescroll,localreload,showloading,errorcontainer,processlinks,processlinksfunc) {
	successcall = fb_checkerrormsg(msg,dataType,errorcontainer);

	if (changescroll){
		procesarnodo2 = procesarnodo.replace("#","");
		fb_scrollto(procesarnodo2);
	}
	
	if (successcall==2){
		if (msg!=""){
			if (typeof(showloading) != "undefined" && showloading=="after"){
				jQuery(fbthickbox_container).fadeIn(500, function () {
					jQuery(procesarnodo).html(msg);
				});
				//.next().delay(500).fadeOut(500);
				
			}else{
				jQuery(procesarnodo).html(msg);
			}
			
			if(typeof(FB) != "undefined" && FB!=""){
				FB.XFBML.parse();
			}
			
			//fb_post_load();
			if (processlinks){
				processlinksfunc(procesarnodo);
			}

		}
		//jQuery("#bodycontainer").trigger("fbHandleCargaAjax", [msg]);
	}


	if (typeof(showloading) != "undefined" && showloading=="after"){
	}else{
		jQuery(fbthickbox_container).fadeOut(250);
	}
	if (typeof(callback) != "undefined" && callback!=""){
		callback(domobj,msg,successcall);
	}
}

function fb_scrollto(scrolltotagid,time,offsetplus){
	
	if (typeof(scrolltotagid) == "undefined" || scrolltotagid==""){
		scrolltotagid = "bodycontainer";
	}
	if (typeof(time) == "undefined" || time==""){
		time = 1;
	}
	if (typeof(offsetplus) == "undefined" || offsetplus==""){
		offsetplus = 0;
	}
	
	if (typeof(fb_chaneltype) != "undefined" && fb_chaneltype=="widget"){
			offsety = jQuery("#"+scrolltotagid).offset().top + offsetplus;
			if (offsety<0){
				offsety = 0;
			}
	
			parent.postMessage("smpwidgetcanvasscroll="+offsety,"*");
	}else if(jQuery("#"+scrolltotagid).length && typeof(fb_chaneltype) != "undefined" && fb_chaneltype!='canvas' && fb_chaneltype!='tab') {
		//document.getElementById(scrolltotagid).scrollIntoView();
		offsety = jQuery("#"+scrolltotagid).offset().top + offsetplus;
		if (offsety<0){
			offsety = 0;
		}
		jQuery('html, body').animate({
        scrollTop: offsety
    }, time);
	}else if(typeof(FB) != "undefined" && FB!=""){
		var offset = jQuery("#"+scrolltotagid).offset();
		offsety = 0;
		if (fb_chaneltype=='tab'){
			offsety = 300;
		}
		if (offset!="" && typeof(offset) != "undefined"){
			offsety = offsety + Math.floor(offset.top);
		}
		FB.XFBML.parse();
		FB.Canvas.scrollTo(0,offsety);
	}
}
/*function fb_links_canvas(procesarnodo){
jQuery(document).ready(function($) {

	if (fb_signed_request!=""){
		$('a').click(
		  function () {
		  	var thehref = $(this).attr("href");
		  	var domain = thehref.split(/\/+/g)[1];
		  	
		  	if (thehref.indexOf(domain)>-1){
		  		//alert($(this).attr("href")+"?signed_request="+fb_signed_request);
		  		if (thehref.indexOf('?')>-1){
		  			thehref = thehref+"&signed_request="+fb_signed_request;
		  		}else{
		  			thehref = thehref+"?signed_request="+fb_signed_request;
		  		}
		  		$(this).attr("href",thehref);
		  	}
		  }
		);
		$('form').submit(
				  function () {
				  	var thehref = $(this).attr("action");
				  	var domain = thehref.split(/\/+/g)[1]; 
				  	if (thehref.indexOf(domain)>-1){
				  		if (thehref.indexOf('?')>-1){
				  			thehref = thehref+"&signed_request="+fb_signed_request;
				  		}else{
				  			thehref = thehref+"?signed_request="+fb_signed_request;
				  		}
				  		$(this).attr("action",thehref); 
				  	}
				  }
		);
	}
	
});
}
*/

function login_facebook3(urlajax){
	jQuery(document).ready(function($) {
		$('.fbconnect_login_div').load(urlajax+'?checklogin=true&refreshpage=fbconnect_refresh');
	});
}

function login_facebookForm(){
	jQuery(document).ready(function($) {
		$('#fbconnect_reload2').show(); 
		var fbstatusform = $('#fbstatusform');	
		$('#fbresponse').load(fbstatusform[0].action+'?checklogin=true&login_mode=themeform');
	});
}

function login_facebookNoRegForm(){
	jQuery(document).ready(function($) {
		$('#fbconnect_reload2').show(); 
		$('#fbloginbutton').hide(); 
		var fbstatusform = $('#fbstatusform');	
		$('#fbresponse').load(fbstatusform[0].action+'?checklogin=true&login_mode=themeform&hide_regform=true');
	});
}

function verify(url, text){
		if (text=='')
			text='Are you sure you want to delete this comment?';
		if (confirm(text)){
			document.location = url;
		}
		return void(0);
	}
// setup everything when document is ready
var fb_statusperms = false;	

function facebook_prompt_permission(permission, callbackFunc) {
    //check is user already granted for this permission or not
    FB.Facebook.apiClient.users_hasAppPermission(permission,
     function(result) {
        // prompt offline permission
        if (result == 0) {
            // render the permission dialog
            FB.Connect.showPermissionDialog(permission, callbackFunc,true);
        } else {
            // permission already granted.
			fb_statusperms = true;
            callbackFunc(true);
        }
    });
}

function callback_perms(){
	
	window.location.reload()
}

function fb_showTab(tabName){
	jQuery(".fbtabdiv").hide();
	jQuery(".fbtablink").removeClass("selected");
	jQuery("#"+tabName).show();	
	jQuery("#"+tabName+'A').addClass("selected");	
	return false;
}

function fb_showTabComments(tabName){
	document.getElementById("fbFirstCommentsA").className = '';
	document.getElementById("fbSecondCommentsA").className = '';
	
	document.getElementById("fbFirstComments").style.visibility = 'hidden';
	document.getElementById("fbSecondComments").style.visibility = 'hidden';
	document.getElementById("fbFirstComments").style.display = 'none';
	document.getElementById("fbSecondComments").style.display = 'none';
	document.getElementById(tabName).style.visibility = 'visible';
	document.getElementById(tabName).style.display = 'block';
	document.getElementById(tabName+'A').className = 'selected';
	return false;
}

function fb_show(idname){
	document.getElementById(idname).style.visibility = 'visible';
	document.getElementById(idname).style.display = 'block';
}
function fb_hide(idname){
	document.getElementById(idname).style.visibility = 'hidden';
	document.getElementById(idname).style.display = 'none';
}	
function fb_showComments(tabName){
	document.getElementById("fbAllFriendsComments").style.visibility = 'hidden';
	document.getElementById("fbAllComments").style.visibility = 'hidden';
	document.getElementById("fbAllFriendsComments").style.display = 'none';
	document.getElementById("fbAllComments").style.display = 'none';
	document.getElementById("fbAllFriendsCommentsA").className = '';
	document.getElementById("fbAllCommentsA").className = '';
	document.getElementById(tabName).style.visibility = 'visible';
	document.getElementById(tabName).style.display = 'block';
	document.getElementById(tabName+'A').className = 'selected';
	return false;
}
function pinnedChange(){
	if (document.getElementById('fbconnect_widget_div').className == "") {
		document.getElementById('fbconnect_widget_div').className = "pinned";
	}else{
		document.getElementById('fbconnect_widget_div').className = "";
	}
}

function showCommentsLogin(){
	var comment_form = document.getElementById('commentform');
	if (!comment_form) {
		return;
	}

	commentslogin = document.getElementById('fbconnect_commentslogin');
	var firstChild = comment_form.firstChild;
    comment_form.insertBefore(commentslogin, firstChild);
	//comment_form.appendChild(commentslogin);
}

function login_thickbox(urllike){
	//alert("LOGIN "+urllike);
	var urlthick = urllike;	
		
	if(urllike.indexOf("?") != -1){
			urlthick = urllike + "&fbconnect_action=register&height=400&width=370";
	}else{
			urlthick = urllike + "?fbconnect_action=register&height=400&width=370";			
	}
	tb_show('Registro', urlthick, null); 
}

function isfanfbpage(pageid,postid,uid,sessionFacebook,thick){
	FB.api( {
   		method: 'pages.isFan',
   		page_id: pageid,
		uid: uid }, 
       function(result) { 
			if (result){
				//document.location = fb_add_urlParam(fb_pageurl,sessionFacebook);
				if(thick){
					tb_remove();
				}else{
					jQuery('#fbaccesstable').html("Loading...");
					fb_loadaccesstab(fb_root_siteurl+"/?fbconnect_action=tab&"+sessionFacebook+"&postid="+postid+"&getcontent=true");
				}
			}else{
				if(thick){
					imgLoader = new Image();
					imgLoader.src = tb_pathToImage;
					tb_show("", fb_root_siteurl+"/?fbconnect_action=tab&height="+fb_heightthick+"&width="+fb_widththick+"&"+sessionFacebook+"&postid="+postid+"&modal=true", "");
					jQuery("#TB_window").css({'margin-top':'0px',top: '<?php echo $topthick;?>px','border':'0'});
					jQuery("#TB_overlay").css({'background-color': '#FFFFFF'});
				}else{
					jQuery('#fbaccesstable').html("Loading...");
					fb_loadaccesstab(fb_root_siteurl+"/?fbconnect_action=tab&"+sessionFacebook+"&postid="+postid);
				}
			}

			 });
}

function customHandleSessionResponse(responseInit){

}

var urllike = "";

function post_fb_user_action(url,action,objecttype)
{
    FB.api('/me/' + action + 
                '?'+objecttype+'='+url,'post',
                function(response) {
        if (!response || response.error) {
                alert('Error occured');
        } else {
            alert('Post was successful! Action ID: ' + response.id);
            }
    });
}

//From ga_social_tracking.js
function fb_extractParamFromUri(uri, paramName) {
	  if (!uri) {
	    return;
	  }
	  
	  var query = decodeURI(uri);
	  
	  // Find url param.
	  paramName += '=';
	  var params = query.split('&');
	  for (var i = 0, param; param = params[i]; ++i) {
 
	    if (param.indexOf(paramName) === 0) {
	      return unescape(param.split('=')[1]);
	    }
	  }
	  return;
}
	
function fb_windowopen(pageurl,title){
	newWindow = window.open(pageurl,title,"status,menubar,height=500,width=640,scrollbars=1");
	newWindow.focus( );	
}

function fbshare_tuenti(pageurl){
	newWindow = window.open("http://www.tuenti.com/share?url="+pageurl,"ShareTuenti","status,menubar,height=500,width=640");
	newWindow.focus( );	
}

function fbshare_twitter(status){
	newWindow = window.open("http://twitter.com/home?status="+status,"ShareTwitter","status,menubar,height=500,width=640");
	newWindow.focus( );	
}

function fbshare_twitter_intent(status,url,via,callback,postid){
	twitterurl = "https://twitter.com/intent/tweet?via="+via+"&url="+url+"&text="+status;
	//newWindow = window.open(twitterurl,"ShareTwitter","status,menubar,height=500,width=640");
	//newWindow.focus( );	
	jQuery(window).bind("message", function(event) {
          event = event.originalEvent
          if(event.source == share_window && event.data != "__ready__") {
          	tweetid = "";
          	if (typeof(event.data.tweet_id)!=undefined){
          		tweetid = event.data.tweet_id;
          	}

          	callback(tweetid,status,url,"","",postid,"share_twitter");
            //console.log(event);
          }
        });
    var share_window = window.open(twitterurl,"ShareTwitter","status,menubar,height=500,width=640");
    share_window.focus( );	
}

function fbshare_linkedin(articleUrl,articleTitle,articleSummary,articleSource){
	newWindow = window.open("http://www.linkedin.com/shareArticle?mini=true&url="+articleUrl+"&title="+articleTitle+"&summary="+articleSummary+"&source="+articleSource,"ShareLinkedin","status,menubar,height=500,width=640");
	newWindow.focus( );	
}


function fbshare_facebook(name,caption,description,picture,link,display,redirect,callback,postid){
	//if (navigator.userAgent.indexOf('IEMobile')==-1){
		
	if (navigator.userAgent.indexOf('IEMobile')!=-1 || (navigator.userAgent.indexOf('Android')!=-1 && navigator.userAgent.indexOf('FBAN')!=-1)){ 
		redirect = fb_pageurl;
		urldialog ="https://www.facebook.com/v2.1/dialog/feed?";
		urldialog += "app_id="+fb_appid;
		urldialog += "&caption="+caption;
		urldialog += "&description="+description;
		urldialog += "&link="+link;
		urldialog += "&locale=es_ES";
		urldialog += "&name="+name;
		urldialog += "&picture="+picture;
		urldialog += "&redirect_uri="+fb_pageurl;
		document.location = urldialog;
	}else{
		params = {
					     method: 'feed',
					     name: name,
					     caption: caption,
					     description: description,
					     picture: picture,
					     link: link,
						 display: display
					  };
		if (typeof(redirect) != "undefined" && redirect!=""){
			params["redirect_uri"] = redirect;
		}
	
	 	FB.ui( params ,
		  function(response) {
		    if (response && response.post_id) {
		      callback(response.post_id,name,link,description,picture,postid,"share_facebook");
		    } else {
		       callback("");
		    }
		  });
	}  
}
/*
function fbshare_facebook(name,caption,description,picture,link,display,redirect,callback,postid){
	params = {
				     method: 'share',
				     name: name,
				     caption: caption,
				     description: description,
				     picture: picture,
				     href: link,
				     display: display
				  };

 	FB.ui( params ,
	  function(response) {
	  	//console.log(response);
	    if (response=="") {
	      callback(0,name,link,description,picture,postid,"share_facebook");
	    } else { //response contiene un objeto con el error
	       callback("");
	    }
	  });
}*/

function fbshare_facebook_login(name,caption,description,picture,link,display,redirect){
	if (fb_status=="connected"){
		fbshare_facebook(name,caption,description,picture,link,display,redirect);
	}else{
		FB.login(function(response) {
			   if (response.authResponse) {
				   fbshare_facebook(name,caption,description,picture,link,display,redirect);
			   } else {
				   
			   }
			 }, {scope: fb_requestperms});
	}
}

function fbshare_global(){
	jQuery('#commentform #submit').click(function() {
		comment = jQuery('#commentform #comment').val();
		var checkSend = jQuery('#sendToFacebook').attr("checked");
		if (checkSend=="checked" && fb_userid!="" && fb_userid!="0"){
			if (fb_netid=="facebook"){
				fbshare_facebook(fb_pagetitle,fb_caption,fb_bodypost,fb_postimgurl,fb_pageurl,"popup",fb_closepopup_url);
			}else if (fb_netid=="google"){
				//fb_windowopen("https://m.google.com/app/plus/x/?v=compose&hideloc=1&content="+encodeURI(comment+" "+fb_pagetitle+" "+fb_pageurl),"googleplusshare");
				fb_windowopen("https://plusone.google.com/_/+1/confirm?hl=en&url="+encodeURI(fb_pageurl),"googleplusshare");
			
			}else if(fb_netid=="twitter"){
				fb_windowopen("http://twitter.com/intent/tweet?status="+encodeURI(fb_pagetitle+" "+fb_pageurl),"twittershare");
			}
		}
	});	
}
			 
function fb_registeruserthick(){
	if(urllike.indexOf("?") != -1){
			urlthick = fb_root_siteurl + "&fbconnect_action=register&height=400&width=370";
	}else{
			urlthick = fb_root_siteurl + "?fbconnect_action=register&height=400&width=370";			
	}
	tb_show('Registro', urlthick, null); 
}

function fb_refreshlogininfo(netid,fb_userid,wpuserid,isNewUser,terms){

	fb_netid = netid;
	fb_userid = fb_userid;
	wp_userid = wpuserid;
	if (typeof(fb_loginreload) == "undefined"){
		fb_loginreload = true;
	}
	
	tb_remove();

		jQuery('.fbconnect_widget_divclass').fadeOut('fast');
		var fbimgparams = "";
		if (typeof(window["maxlastusers"]) != "undefined"){
			fbimgparams = '&maxlastusers='+maxlastusers+'&avatarsize='+avatarsize;
		}

		href = fb_add_urlParam(fb_ajax_url,"useajax="+sjws_useajaxcontent);
		href = fb_add_urlParam(href,"fbajaxlogin="+fb_netid);
		href = fb_add_urlParam(href,"refreshpage="+fb_loginreload);
		href = fb_add_urlParam(href,fbimgparams);
		href = fb_add_urlParam(href,"smpchanneltype="+fb_chaneltype);
		href = fb_add_urlParam(href,"fbpostid="+fb_postid);
		
		if (typeof(fb_redirect_login_url) != "undefined" && fb_redirect_login_url!=""){
					if(typeof(fb_redirect_login_url_thick) != "undefined" && fb_redirect_login_url_thick=="thickbox"){
						tb_show('', fb_redirect_login_url, null);
					}else if(typeof(fb_redirect_login_url_thick) != "undefined" && fb_redirect_login_url_thick=="jscallback"){
						fb_redirect_login_url(fb_userid,wpuserid,fb_isNewUser,fb_user_terms); 
					}else{
						document.location = fb_redirect_login_url;
					}	
		}else if (fb_show_reg_form=="on" && (fb_isNewUser || (fb_reg_form_terms=="y" && fb_user_terms!="y"))){
			if(fb_regform_url==""){
				tb_show("Registration", fb_root_siteurl+ "?fbconnect_action=register&height=390&width=435", "");
			}else{
				document.location = fb_root_siteurl + fb_regform_url;
			}
		}else if (fb_loginreload){
				document.location = fb_pageurl;
		}else{
			jQuery('.fbconnect_widget_divclass').load(href,function(){
				
				if (!fb_loginreload){
					jQuery('.fbconnect_commentsloginclass .fbTabs').remove();
					fb_showTab('fbFirst');
					jQuery(".fbconnect_commentslogin").show();
					jQuery(this).fadeIn('slow');
					jQuery("#commentform #author").val(wp_username);
					jQuery("#commentform #email").val(wp_useremail);
					jQuery("#fbconnectcheckpublish").addClass("fbconnect_sharewith_facebook");
					jQuery("#fbconnectcheckpublish").show();
				}
			});
		}
		
	//}
}

function fb_callParentWindow(netid,fbuserid){
    window.opener.fb_refreshlogininfo();
    return false;
}

function fbInviteFriendsCallback(response){
	//alert("callback");
}

function fbInviteFriends(msg) {
        FB.ui({method: 'apprequests',
          message: msg
        }, fbInviteFriendsCallback);
}

function fbMustLoggin(){
	if (wp_userid=="" || wp_userid=="0"){
		var thehref = jQuery(this).attr("href");
		fb_redirect_login_url = thehref;
		urlthick = fb_plugin_url+"/fbconnect_loginpopup.php?height=330&width=540";
		tb_show('Login', urlthick, null);
		return false;
	}
}

function fbCommentsloginClick(){
	randnum = Math.floor((Math.random()*100)+1);
	fb_pageurl = fb_add_urlParam(fb_pageurl,'ramdomparam='+randnum);
	fb_pageurl = fb_add_urlParam(fb_pageurl,'refreshlogin=1')+"#comments";
}

var fbresposicionandoelment;
function fbChangeElementPosition(info){
	var theheight = info.clientHeight - info.offsetTop - fbresposicionandoelment.outerHeight();
  
  if (theheight<0) theheight = 0;
  
  fbresposicionandoelment.css({
    'position':'absolute',
    'top':Math.abs((theheight / 2) + info.scrollTop )
  //  'left':Math.abs(((info.clientWidth - fbresposicionandoelment.outerWidth()) / 2) + info.scrollLeft )
  });
  
fbresposicionandoelment = "";
}

function fb_redirecttopwindow(urlredirect){
	top.location.href = urlredirect;
}


function fb_centerthickbox(idnode,widthcontent) {
	if (typeof(widthcontent) == "undefined" || widthcontent==""){
		widthcontent = 300;
	}
	if (typeof(fb_canvas) != "undefined" && fb_canvas!=""){	
		tbnode = jQuery(idnode);
		tbnode.css({
		    'left': 0
		});
		
		tbnode.wrapInner( "<div class='fbthickboxcontainerpd' />");
		tbnode.wrapInner( "<div class='fbthickboxcontainer' />");
		tbnode.addClass("fbthickbox");
	
		/*widthcontent = jQuery("#TB_ajaxContent").width();
		if (widthcontent<300){
			widthcontent = 300;
		}*/

		jQuery(".fbthickboxcontainer").width(widthcontent);	
		
		if (fb_canvas=="tab" || fb_canvas=="canvas"){	
			fbresposicionandoelment = tbnode;
			FB.Canvas.getPageInfo(fbChangeElementPosition);
			return tbnode;
		}else if (fb_canvas=="widget"){	
			var w = jQuery(window);
			  
			var left;
			var top;
			top = 30;
			left = Math.abs(((w.width() - tbnode.outerWidth()) / 2) + w.scrollLeft());
			
			if (w.width() <= tbnode.outerWidth()){
			  	left = 0;
			}
			tbnode.css({
			    'position':'absolute',
			    'top': top,
			    'margin': '0'
			});
			
			//fb_scrollto();	
			parent.postMessage("smpwidgetcanvasscroll=0","*");
			
			return tbnode;
	
		}else{
			var w = jQuery(window);
	
			var theheight = w.height() - tbnode.outerHeight(false);
			//alert(w.height() +" - "+ tbnode.outerHeight(false));
			if (theheight<0) theheight = 0;
			  
			var left;
			var top;
			top = Math.abs((theheight / 2) + w.scrollTop());
			if (fb_canvas=="widget" && top > 200){
				top = top - 200;
				
			}
			left = Math.abs(((w.width() - tbnode.outerWidth()) / 2) + w.scrollLeft());

			if (w.width() <= tbnode.outerWidth()){
			  	left = 0;
			}
			
			tbnode.hide().css({
			    'position':'absolute',
			    'top': top,
			    'margin': '0'
			}).fadeIn(500);
			
			var doc = jQuery(document);

			jQuery("#TB_overlay").css({
						    'position':'absolute',
						    'top': 0,
						    'left': 0,
						    'width': '100%',
						    'height': doc.height()
						});
			return tbnode;
		}
	}
	// alert( left + " = " + w.width() +" - "+ tbnode.outerWidth() + " - "+jQuery("#TB_window").width()+ " - "+w.scrollLeft());
}

function fb_showthickbox(nodeclick) {
	jQuery("#TB_imageOff").unbind("click");
	jQuery("#TB_closeWindowButton").unbind("click");
	jQuery("#TB_window").remove();
	jQuery('#TB_window,#TB_overlay,#TB_HideSelect').trigger("tb_unload").unbind().remove();
	jQuery("#TB_load").remove();
	jQuery(document).unbind('.thickbox');
	var thehref = jQuery(nodeclick).attr("href");
	var thetitle = jQuery(nodeclick).attr("title");
	//url = fb_add_urlParam(url,"height=450");
	//url = fb_add_urlParam(url,"height=550");
	tb_show(thetitle,thehref,'');
}
function fbshareshowbuttons(postid){
	jQuery("#fbsharelinksmetro-"+postid).fadeIn("slow");
}

function fb_functionName(fun) {
  var ret = fun.toString();
  ret = ret.substr('function '.length);
  ret = ret.substr(0, ret.indexOf('('));
  return ret;
}