<?php 
/**
 * @author: Javier Reyes Gomez (http://www.sociable.es)
 * @date: 05/10/2008
 * @license: GPLv2
 */

if( fb_get_option('fb_disablecommunity')!=""){
	exit;
}

if(!fb_get_option('fb_connect_use_thick')){
	get_header(); 
}
?>


	<div class="fbnarrowcolumn narrowcolumn">
		
<?php

	$fb_user = fb_get_loggedin_user();
	if(isset($fb_user) && $fb_user!=""){
		$user = wp_get_current_user();
		$users = WPfbConnect_Logic::get_friends($user->ID,0,100);
		echo '<h2 class="profileTitleh2">'.__('Community friends', 'fbconnect')."</h2>\n";
		echo '<div class="fbconnect_userpics2">';
		foreach($users as $friend){
			echo get_avatar( $friend->ID,50 );
			//echo "<div><a style=\"border: 2px solid #d5d6d7;\" onclick=\"location.href='./?fbconnect_action=myhome&fbuserid=".$user["uid"]."';\" href=\"./?fbconnect_action=myhome&fbuserid=".$user["uid"]."\"><fb:profile-pic uid=\"".$user["uid"]."\" size=\"square\" linked=\"false\"></fb:profile-pic></a></div>\n";
		}
		echo "</div>\n";
	}else{
		
	}
?> 
		
<?php
		if(fb_get_option('fb_connect_use_thick')){
			$addthickbox= "onclick=\"fb_showthickbox(this);return false;\"";
		}else{
			$addthickbox= "";			
		}
		$users_count = WPfbConnect_Logic::get_count_users();
		echo '<h2 class="profileTitleh2">'.__('Community', 'fbconnect')." (".$users_count." ".__('members', 'fbconnect').")</h2>";
		
		$pos = 0;
		if (isset ($_REQUEST["pos"])){
			$pos= (int)$_REQUEST["pos"];
		}
		
		$fb_communitynumimgs = fb_get_option('fb_communitynumimgs');
		if ($fb_communitynumimgs==""){
			$fb_communitynumimgs="48";
		}
				
		$viewusers = $fb_communitynumimgs;
		$users = WPfbConnect_Logic::get_lastusers_fbconnect($viewusers,$pos);

			echo '<div class="fbconnect_userpics2">';
			foreach($users as $last_user){
				if (isset($user) && $user->ID!=0 && $last_user->ID==$user->ID){
					echo '<a title="'.__("User profile","fbconnect").'" '.$addthickbox.' href="'.WPfbConnect_Logic::get_user_socialurl($user->fbconnect_userid,$user->fbconnect_netid,$user->ID).'">';
					$cierrelink = "</a>";
				}else{
					$cierrelink = "";
				}
				echo get_avatar( $last_user->ID,50 );
				echo $cierrelink;
			}
			echo "</div>";

		if ($pos>=$viewusers){
			$url = fb_get_option('siteurl').'/?fbconnect_action=community&pos='.($pos-$viewusers).'&height=400&width=470';
			echo '<a title="'.__("Community","fbconnect").'" '.$addthickbox.' href="'.$url.'">&laquo; '.__('Previous page', 'fbconnect').'</a>';
		}
		if (($pos+$viewusers)<$users_count){
			$url = fb_get_option('siteurl').'/?fbconnect_action=community&pos='.($pos+$viewusers).'&height=400&width=470';
			echo '<a title="'.__("Community","fbconnect").'" '.$addthickbox.' href="'.$url.'"> '.__('Next page', 'fbconnect').' &raquo;</a>';
		}

?>

</div>


<?php 
if(!fb_get_option('fb_connect_use_thick')){
	get_footer(); 
}else{
?>	
<script type="text/javascript">
	fb_centerthickbox("#TB_window");
	FB.XFBML.parse();
</script>
<?php 
}
?>