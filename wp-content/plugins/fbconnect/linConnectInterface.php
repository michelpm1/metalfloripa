<?php
/**
 * @author: Javier Reyes Gomez (http://www.sociable.es)
 * @date: 05/10/2008
 * @license: GPLv2
 */

		global $wp_version, $fbconnect,$fb_reg_formfields;

			if ( isset($_POST['info_update']) ) {
				check_admin_referer('wp-fbconnect-info_update');

				$error = '';
				update_option( 'lin_login_enabled', $_POST['lin_login_enabled'] );
				update_option( 'lin_api_key', $_POST['lin_api_key'] );
				update_option( 'lin_appId', $_POST['lin_appId'] );
				update_option( 'lin_api_secret', $_POST['lin_api_secret'] );
				update_option('lin_permsToRequestOnConnect',$_POST['lin_permsToRequestOnConnect']);
				if ($error !== '') {
					echo '<div class="error"><p><strong>'.__('At least one of LinkedIn Connector options was NOT updated', 'fbconnect').'</strong>'.$error.'</p></div>';
				} else {
					echo '<div class="updated"><p><strong>'.__('LinkedIn Connector options updated', 'fbconnect').'</strong></p></div>';
				}

			
			}
			
			// Display the options page form
			$siteurl = fb_get_option('home');
			if( substr( $siteurl, -1, 1 ) !== '/' ) $siteurl .= '/';
			?>
			<div class="wrap">
				<h2>
					<img src="<?php echo FBCONNECT_PLUGIN_URL;?>/images/linkedin-20.png"/>
					<?php _e('Linked in Configuration', 'fbconnect') ?></h2>

				<form method="post">


					<h3><?php _e('LinkedIn Application Configuration', 'fbconnect') ?></h3>
     				<table class="form-table" cellspacing="2" cellpadding="5" width="100%">
     					<tr valign="top">
							<th style="width: 33%" scope="row"><?php _e('Enable LinkedIn login:', 'fbconnect') ?></th>
							<td>
								<p><input type="checkbox" name="lin_login_enabled" id="lin_login_enabled" <?php
								if( fb_get_option('lin_login_enabled')) echo 'checked="checked"'
								?> />
							</td>
						</tr>
						<tr valign="top">
							<th style="width: 33%" scope="row"><?php _e('LinkedIn App. Config.', 'fbconnect') ?></th>
							<td>
							<a href="https://www.linkedin.com/secure/developer" target="_blank"><?php _e('Create a new LinkedIn Application', 'fbconnect') ?></a><br/>
							<br/><?php _e('Your LinkedIn redirect URI (copy and paste in the LinkedIn Application configuration):', 'fbconnect') ?>
							<br/><input type="text" name="g_callback" id="g_callback" size="50" value="<?php echo WPlinConnect_Logic::getRedirectUrl();?>"/>
							</td>
						</tr>

						<tr valign="top">
							<th style="width: 33%" scope="row"><label for="lin_appId"><?php _e('LinkedIn API Key:', 'fbconnect') ?></label></th>
							<td>
							<input type="text" name="lin_appId" id="lin_appId" size="50" value="<?php echo fb_get_option('lin_appId');?>"/>
							</td>
						</tr>
						<tr valign="top">
							<th style="width: 33%" scope="row"><label for="lin_api_secret"><?php _e('LinkedIn Secret Key:', 'fbconnect') ?></label></th>
							<td>
							<input type="text" name="lin_api_secret" size="50" id="lin_api_secret" value="<?php echo fb_get_option('lin_api_secret');?>"/>
							</td>
						</tr>	
						<tr valign="top">
							<th style="width: 33%" scope="row"><label for="lin_permsToRequestOnConnect"><?php _e('Perms to request:', 'fbconnect') ?></label></th>
							<td>
							<input type="text" name="lin_permsToRequestOnConnect" id="fb_api_key" size="50" value="<?php echo fb_get_option('lin_permsToRequestOnConnect');?>"/>
							<label for="lin_permsToRequestOnConnect"><?php _e('Perms to request on user first login (comma separated list) (r_fullprofile,r_emailaddress,r_network,r_contactinfo,rw_nus,rw_groups,w_messages).', 'fbconnect') ?></label>
							</td>
						</tr>						

     				</table>

					
					<?php wp_nonce_field('wp-fbconnect-info_update'); ?>
					
     				<p class="submit"><input class="button-primary" type="submit" name="info_update" value="<?php _e('Update Configuration', 'fbconnect') ?> &raquo;" /></p>
     			</form>
				
			</div>
