<?php
/**
 * @author: Javier Reyes Gomez (http://www.sociable.es)
 * @date: 05/10/2008
 * @license: GPLv2
 */

if (!class_exists('WPiConnect_Logic')):

/**
 * Basic logic for Instagram.
 */
class WPiConnect_Logic {
public $instagramoauthclient;
public $instagramperms;	
function init($token=""){
	try{
		$this->instagramoauthclient = new Instagram(array(
      	'apiKey'      => fb_get_option('i_appId'),
      	'apiSecret'   => fb_get_option('i_api_secret'),
      	'apiCallback' => WPiConnect_Logic::getRedirectUrl()
    	));
	
		$perms = fb_get_option('i_permsToRequestOnConnect');
		if ($perms==""){
			$perms = "basic";
		}
		$this->instagramperms = explode(",",$perms);
		
		if($token!=""){
			$this->instagramoauthclient->setAccessToken( $token );
		}else if (isset($_SESSION['i_access_token'])) {
			  $this->instagramoauthclient->setAccessToken($_SESSION['i_access_token']);
		}
	}catch(Exception $e){
		print_r($e);
	}
}	

function getRedirectUrl(){
		$siteUrl= fb_get_option('siteurl');
		if (!WPfbConnect_Logic::endsWith("/",$siteUrl)){
			$siteUrl = $siteUrl . "/";
		}
		$siteUrl = WPfbConnect_Logic::add_urlParam($siteUrl,"oauth_login=instagram");	
		return $siteUrl;
}


function user_getInfo(){
	  //$me = $this->googleplus->people->get('me');
	  $token = json_decode($_SESSION['g_access_token']);
	  $meprofile = $this->instagramoauthclient->getUser();

	  $userinfo = array();
	  $userinfo['uid'] = $meprofile->data->id;
	  $userinfo['username'] = $meprofile->data->username;
	  $userinfo['website'] = $meprofile->data->website;
	  $userinfo['about_me'] = $meprofile->data->bio;
	  $userinfo['email'] = "";
	  $userinfo['profile_url'] = $meprofile->link;
	  $userinfo['name'] = $meprofile->name;
	  $userinfo['first_name'] = $meprofile->data->full_name;
	  $userinfo['last_name'] = "";
	  $userinfo['thumbnail'] = $meprofile->data->profile_picture;
	  $userinfo['sex'] = "";
	  $userinfo['locale'] = "";
	  return $userinfo;

}

function createAuthUrl(){
	$authUrl = $this->instagramoauthclient->getLoginUrl($this->instagramperms);
	return $authUrl; 
}


function wp_login() {
		global $wp_version,$new_fb_user;

		if ( isset($_REQUEST["fbconnect_action"]) && ($_REQUEST["fbconnect_action"]=="delete_user" || $_REQUEST["fbconnect_action"]=="postlogout" || $_REQUEST["fbconnect_action"]=="logout")){
			return;
		}
		
		$self = basename( $GLOBALS['pagenow'] );
	
			
		$user = wp_get_current_user();
		if (isset($user) && $user->ID==0){
			$user = "";	
		}

		if ( isset($_GET['oauth_login']) && $_GET['oauth_login']=="instagram" && (!is_user_logged_in() || $user->fbconnect_userid == "" || $user->fbconnect_userid == 0)) { //Intenta hacer login estando registrado en facebook

			if (isset($_GET['code'])) {
			 // $this->instagramoauthclient->authenticate($_GET['code']);
			 $data = $this->instagramoauthclient->getOAuthToken($_GET['code']);
			  $_SESSION['i_access_token'] = $data;
			  $this->instagramoauthclient->setAccessToken($data);
			  //header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
			}
			if (isset($_SESSION['i_access_token'])) {
			  $this->instagramoauthclient->setAccessToken($_SESSION['i_access_token']);
			
			
				require_once(ABSPATH . WPINC . '/registration.php');
				$usersinfo = $this->user_getInfo();
				if (!isset($usersinfo) || $usersinfo["uid"]==""){
					WPfbConnect::log("[fbConnectLogic::wp_login_fbconnect] fb_user_getInfo ERROR: ".$fb_user,FBCONNECT_LOG_ERR);
					return;	
				}
				$fb_user = $usersinfo["uid"];
				$_SESSION["facebook_usersinfo"] = $usersinfo;
				$_SESSION["fbconnect_netid"] = "instagram";

				$wpid = "";
				$fbwpuser = WPfbConnect_Logic::get_userbyFBID($usersinfo["uid"],"instagram");
				if ($fbwpuser =="" && $usersinfo["email"]!=""){
					$fbwpuser = WPfbConnect_Logic::get_userbyEmail($usersinfo["email"]);
				}

				//echo "LEER:".$fb_user;
				//print_r($fbwpuser);
				$wpid = "";
				$new_fb_user= false;
	
				if(is_user_logged_in() && $fbwpuser && $user->ID==$fbwpuser->ID && ($user->fbconnect_userid =="" || $user->fbconnect_userid =="0")){ // Encuentra por email el usuario y no está asociado al de FB
					WPfbConnect_Logic::set_userid_fbconnect($user->ID,$fb_user,"instagram");
					$wpid = $user->ID;
				}else if(FBCONNECT_CANVAS!="web" && is_user_logged_in() && $fbwpuser && $user->ID != $fbwpuser->ID){ // El usuario FB está asociado a un usaurio WP distinto al logeado
					$wpid = $fbwpuser->ID;
				}elseif(is_user_logged_in() && !$fbwpuser && ($user->fbconnect_userid =="" || $user->fbconnect_userid =="0")){ // El usuario WP no está asociado al de FB
					WPfbConnect_Logic::set_userid_fbconnect($user->ID,$fb_user,"instagram");
					$wpid = $user->ID;
				}elseif (!is_user_logged_in() && $fbwpuser && ($fbwpuser->fbconnect_userid =="" || $fbwpuser->fbconnect_userid =="0")){
					WPfbConnect_Logic::set_userid_fbconnect($fbwpuser->ID,$fb_user,"instagram");
					$wpid = $fbwpuser->ID;	
				}elseif(!is_user_logged_in() && $fbwpuser && ($fbwpuser->fbconnect_userid ==$fb_user)){
					$wpid = $fbwpuser->ID;	
				}elseif (($fbwpuser && $fbwpuser->fbconnect_userid != $fb_user) || (!is_user_logged_in() && !$fbwpuser) || (!$fbwpuser && is_user_logged_in() && $user->fbconnect_userid != $fb_user)){
					if(isset($usersinfo) && $usersinfo!=""){
						$username = WPfbConnect_Logic::fbusername_generator($usersinfo["uid"],$usersinfo['username'],$usersinfo["first_name"],$usersinfo["last_name"],"I");
		
						$user_data = array();
						$user_data['user_login'] = $username;
						
						$user_data['user_pass'] = substr( md5( uniqid( microtime() ).$_SERVER["REMOTE_ADDR"] ), 0, 15);
	
						$user_data['user_nicename'] = $username;
						$user_data['display_name'] = $usersinfo["name"];
	
						$user_data['user_url'] = $usersinfo["profile_url"];
						//$user_data['user_email'] = $usersinfo["proxied_email"];
						$user_data['user_email'] = "";
						
						if ($usersinfo["email"]!=""){
							$user_data['user_email'] = $usersinfo["email"];
						}
						//Permitir email en blanco y duplicado
						define ( 'WP_IMPORTING', true);

						$wpid = wp_insert_user($user_data);
						if ( !is_wp_error($wpid) ) {
							update_usermeta( $wpid, "first_name", $usersinfo["first_name"] );
							update_usermeta( $wpid, "last_name", $usersinfo["last_name"] );
	
							if (isset($usersinfo["sex"]) && $usersinfo["sex"] != ""){
								update_usermeta( $wpid, "sex", $usersinfo["sex"] );
							}
							WPfbConnect_Logic::set_userid_fbconnect($wpid,$fb_user,"instagram");
							$new_fb_user= true;
						}else{ // no ha podido insertar el usuario
							return;
						}
					}
					
				}else{
					return;
				}

	
				$userdata = WPfbConnect_Logic::get_userbyFBID($fb_user,"instagram");
				
				WPfbConnect_Logic::set_lastlogin_fbconnect($userdata->ID);
				global $current_user;
	
				$current_user = null;
		
	
				WPfbConnect_Logic::fb_set_current_user($userdata);
				
				//Update thumbnail
				if($userdata!="" && $usersinfo["thumbnail"]!=""){
					update_usermeta($userdata->ID, 'thumbnail', $usersinfo["thumbnail"]);
				}
	
				global $userdata;
				if (isset($userdata) && $userdata!=""){
					$userdata->fbconnect_userid = $fb_user;
					$userdata->fbconnect_netid = "instagram";
				}
				
				//Store user token
				if (fb_get_option('fb_storeUserAcessToken')!="" ){
					WPfbConnect_Logic::set_useroffline($userdata->ID,serialize($_SESSION['i_access_token']),1);
				}

				header('Content-type: text/html');
				if ($new_fb_user){
					$new_fb_usertxt = "true";
				}else{
					$new_fb_usertxt = "false";
				}
				$terms = get_user_meta($userdata->ID, "terms", true);
				?>
				<html>
				<body>
				<script>
					window.opener.fb_refreshlogininfo('instagram','<?php echo $fb_user;?>','<?php echo $userdata->ID;?>',<?php echo $new_fb_usertxt;?>,'<?php echo $terms;?>');
					window.close();
				</script>
				</body>
				</html>
				<?php
				exit;
				//Cache friends
				/*WPfbConnect_Logic::get_connected_friends();
				if (fb_get_option('fb_permsToRequestOnConnect')!="" ){
					if (strrpos(fb_get_option('fb_permsToRequestOnConnect'),"offline_access")===false){
						//Not found
					}elseif($userdata!=""){
						$token = fb_get_access_token();
						//update_usermeta( $userdata->ID, "access_token", $token );
						WPfbConnect_Logic::set_useroffline($userdata->ID,$token,1);
						
					}
				}*/
				
			}else{
				header('Content-type: text/html');
				echo "ERROR NO ACCESS TOKEN";
				exit;
			}
		}
	}
	
} 
endif; // end if-class-exists test

try{
	require_once FBCONNECT_PLUGIN_PATH.'/instagram/instagram.class.php';
	global $iLogic;
	$iLogic = new WPiConnect_Logic;
	$iLogic->init();
}catch(Exception $e){
	print_r($e);
}
?>
