<?php
/**
 * @author: Javier Reyes Gomez (http://www.sociable.es)
 * @date: 05/10/2008
 * @license: GPLv2
 */

require_once FBCONNECT_PLUGIN_PATH.'/linkedin/linkedin_3.2.0.class.php';

if (!class_exists('WPlinConnect_Logic')):

/**
 * Basic logic for Twitter.
 */
class WPlinConnect_Logic {
public $oauthclient;

function init($token=""){
	$siteUrl= fb_get_option('siteurl');
	$pageurl = $siteUrl."/";
	if (!is_home()){
		global $post;
		if ($post!=""){
			$pageurl = get_permalink($post->ID);
		}
	}
	$pageurl = WPfbConnect_Logic::add_urlParam($pageurl,"oauth_login=linkedin");
	
	$API_CONFIG = array(
  	'appKey'    => trim(fb_get_option('lin_appId')),
  	'appSecret' => trim(fb_get_option('lin_api_secret')),
  	'callbackUrl' => $pageurl
	);
	 $this->oauthclient = new linkedin($API_CONFIG);
	 $this->oauthclient->setResponseFormat(linkedin::_RESPONSE_JSON);
	
	if($token!=""){
		$this->oauthclient->setToken( $token );
	}else{
		if (isset($_REQUEST['fbconnect_action']) && $_REQUEST['fbconnect_action']=="redirectoauthlinkedin"){
			$_SESSION['oauth'] == "";
		}else{
			 if ( isset($_SESSION['oauth']['linkedin']['access'])){
			 	$this->oauthclient->setToken($_SESSION['oauth']['linkedin']['access']);
				//print_r($this->oauthclient->companies("1337",":(id,name,universal-name,email-domains,company-type,ticker,website-url,industries,status,logo-url,square-logo-url,blog-rss-url,twitter-id,employee-count-range,specialties,locations,stock-exchange,founded-year,end-year,num-followers,description)"));
			 }else{
			 	if (fb_get_option('lin_offline_token')!=""){
			 		//if (is_array(fb_get_option('lin_offline_token'))){
			 			$offline = get_option('lin_offline_token');
			 		/*}else{
			 			$offline = unserialize(fb_get_option('lin_offline_token'));
			 		}*/
					//$this->oauthclient->setToken($offline);
				}
			 }
		}
	}
	// print_r($this->get_groupInfo("115357"));
	//"3434719"
	
	//print_r($this->get_groupFeed("115357",20));
	//print_r($this->get_companyFeed("1337"));
	//print_r($this->get_companyFeed("269755"));
	//print_r($this->get_companyInfo(1009));
	//print_r($this->oauthclient->getGroupPost("g-71380-S-210172657"));
	// print_r($this->oauthclient->getGroupPostLikes("g-71380-S-210172657",1));
	//print_r($this->oauthclient->getGroupPostComments("g-115357-S-211042148"));
	//print_r($this->oauthclient->getpostLikes("UNIU-c1009-5702748470805266432-SHARE"));
	//print_r($this->oauthclient->getpostComments("UNIU-c1009-5702748470805266432-SHARE",5));
}	

function getcompanyPostInfo($post){
	$postinfo = array();
	$postinfo["id"] = $post->updateKey;
	$postinfo["timestamp"] = $post->timestamp;
	$postinfo["numLikes"] = $post->numLikes;
	$postinfo["numComments"] = $post->updateComments->_total;
	
	if (isset($post->updateContent->companyStatusUpdate)){
		
		$postinfo["title"] = $post->updateContent->companyStatusUpdate->share->content->title;
		if ($post->updateContent->companyStatusUpdate->share->comment!=""){
			$postinfo["content"] = $post->updateContent->companyStatusUpdate->share->comment;
		}else{
			$postinfo["content"] = $post->updateContent->companyStatusUpdate->share->content->description;
		}
		$postinfo["imgurl"] = $post->updateContent->companyStatusUpdate->share->content->thumbnailUrl;
		$postinfo["imgbigurl"] = $post->updateContent->companyStatusUpdate->share->content->submittedImageUrl;
		$postinfo["url"] = $post->updateContent->companyStatusUpdate->share->content->submittedUrl;
		$postinfo["authorid"] = $post->updateContent->companyStatusUpdate->share->company->id;
		$postinfo["authorname"] = $post->updateContent->companyStatusUpdate->share->company->name;
		if ($postinfo["authorid"] == ""){
			$postinfo["authorid"] = $post->updateContent->company->id;
			$postinfo["authorname"] = $post->updateContent->company->name;
		}
	}else if(isset($post->updateContent->companyProductUpdate)){
		print_r($post);
		$postinfo["title"] = $post->updateContent->companyProductUpdate->product->name;
		$postinfo["content"] = $post->updateContent->companyProductUpdate->product->description;
		
		$postinfo["url"] = "";
		$postinfo["imgurl"] = "";
		$postinfo["authorid"] = $post->updateContent->company->id;
		$postinfo["authorname"] = $post->updateContent->company->name;
		//echo "\r\nno implementado updateContent->companyProductUpdate";
		//exit;
	}else if(isset($post->updateContent->companyPersonUpdate)){
		print_r($post);
		
		echo "\r\nno implementado updateContent->companyPersonUpdate";
		exit;
	}else if(isset($post->updateContent->companyJobUpdate)){
		$postinfo["title"] = $post->updateContent->companyJobUpdate->job->position->title;
		$postinfo["content"] = $post->updateContent->companyJobUpdate->job->description;
		
		$postinfo["url"] = $post->updateContent->companyJobUpdate->job->siteJobRequest->url;
		$postinfo["imgurl"] = "";
		$postinfo["authorid"] = $post->updateContent->companyJobUpdate->job->company->id;
		$postinfo["authorname"] = $post->updateContent->companyJobUpdate->job->company->name;
	}else{
		print_r($post);
		echo "\r\nno implementado";
		exit;
	}
	return $postinfo;
	
}

function getRedirectUrl(){
	$siteUrl= fb_get_option('siteurl');
	if (!WPfbConnect_Logic::endsWith("/",$siteUrl)){
		$siteUrl = $siteUrl . "/";
	}
	$siteUrl = WPfbConnect_Logic::add_urlParam($siteUrl,"oauth_login=linkedin");	
	return $siteUrl;
}
	
function get_groupPostLikes($key,$start = 0, $count = 20){
	//"UNIU-c1009-5702748470805266432-SHARE"
	$response = $this->oauthclient->getGroupPostLikes($key,$start, $count );
	
	if($response['success'] === TRUE) {
        $feed = json_decode($response['linkedin']);
        return $feed;
	} else {
	      // request failed
	      echo "Error retrieving likes information:<br /><br />RESPONSE:<br /><br /><pre>" . print_r($response) . "</pre>";
  	  	return "ERROR";
	} 
}

function get_postLikes($key){
	//"UNIU-c1009-5702748470805266432-SHARE"
	$response = $this->oauthclient->getpostLikes($key);
	if($response['success'] === TRUE) {
        $feed = json_decode($response['linkedin']);
        return $feed;
	} else {
	      // request failed
	      echo "Error retrieving likes information:<br /><br />RESPONSE:<br /><br /><pre>" . print_r($response) . "</pre>";
  	  	return "ERROR";
	} 
}

function get_groupPostComments($key,$start = 0, $count = 20){
	//"UNIU-c1009-5702748470805266432-SHARE"
	$response = $this->oauthclient->getGroupPostComments($key,$start, $count);

	if($response['success'] === TRUE) {
        $feed = json_decode($response['linkedin']);
        return $feed;
	} else {
	      // request failed
	      echo "Error retrieving comments information:<br /><br />RESPONSE:<br /><br /><pre>" . print_r($response) . "</pre>";
  	  	return "ERROR";
	} 
}

function get_postComments($key){
	//"UNIU-c1009-5702748470805266432-SHARE"
	$response = $this->oauthclient->getpostComments($key);

	if($response['success'] === TRUE) {
        $feed = json_decode($response['linkedin']);
        return $feed;
	} else {
	      // request failed
	      echo "Error retrieving comments information:<br /><br />RESPONSE:<br /><br /><pre>" . print_r($response) . "</pre>";
  	  	return "ERROR";
	} 
}

function get_groupFeed($idc,$start=0,$count=20,$eventtype=""){
	//"1337"
	$response = $this->oauthclient->groupFeed($idc,$start,$count,$eventtype);
	
	if($response['success'] === TRUE) {
        $feed = json_decode($response['linkedin']);
        return $feed;
	} else {
	      // request failed
	      echo "Error retrieving feed information:<br /><br />RESPONSE:<br /><br /><pre>" . print_r($response) . "</pre>";
  	  	return "ERROR";
	} 
}

function get_companyFeed($idc,$start=0,$count=20,$eventtype=""){
	//"1337"
	$response = $this->oauthclient->companyFeed($idc,$start,$count,$eventtype);
	if($response['success'] === TRUE) {
        $feed = json_decode($response['linkedin']);
        return $feed;
	} else {
	      // request failed
	      echo "Error retrieving feed information:<br /><br />RESPONSE:<br /><br /><pre>" . print_r($response) . "</pre>";
  	  	return "ERROR";
	} 
}

function get_companyInfo($idc){
	//"1337"
	$response = $this->oauthclient->companies($idc,":(id,name,universal-name,email-domains,company-type,ticker,website-url,industries,status,logo-url,square-logo-url,blog-rss-url,twitter-id,employee-count-range,specialties,locations,stock-exchange,founded-year,end-year,num-followers,description)");
	if($response['success'] === TRUE) {
        $groupprofile = json_decode($response['linkedin']);
        return $groupprofile;
	} else {
	      // request failed
	      echo "Error retrieving company information:<br /><br />RESPONSE:<br /><br /><pre>" . print_r($response) . "</pre>";
  	  	return "ERROR";
	} 
}	

function get_groupInfo($idg){
	//"3434719"
	$response = $this->oauthclient->group($idg,":(id,name,num-members,short-description,description,relation-to-viewer:(membership-state,available-actions),counts-by-category,is-open-to-non-members,category,website-url,locale,location:(country,postal-code),allow-member-invites,site-group-url,small-logo-url,large-logo-url)");
	if($response['success'] === TRUE) {
        $groupprofile = json_decode($response['linkedin']);
        return $groupprofile;
	} else {
	      // request failed
	      echo "Error retrieving group information:<br /><br />RESPONSE:<br /><br /><pre>" . print_r($response) . "</pre>";
  	  	return "ERROR";
	} 
	  
}
function user_getInfo(){
	$response =  $this->oauthclient->profile('~:(id,first-name,last-name,formatted-name,headline,location,industry,num-connections,summary,picture-url,public-profile-url,email-address,date-of-birth)');

    if($response['success'] === TRUE) {
        $meprofile = json_decode($response['linkedin']);
        //echo "<pre>" . print_r($meprofile, TRUE) . "</pre>";
		//exit;
		$userinfo = array();
		$userinfo['uid'] = $meprofile->id;
		$userinfo['id'] = $meprofile->id;
		$userinfo['pictureUrl'] = $meprofile->pictureUrl;
		$userinfo['about_me'] = $meprofile->headline;
		$userinfo['email'] = $meprofile->emailAddress;
		$userinfo['profile_url'] = $meprofile->publicProfileUrl;
		$userinfo['name'] = $meprofile->formattedName;
		$userinfo['first_name'] = $meprofile->firstName;
		$userinfo['last_name'] = $meprofile->lastName;
		//$userinfo['sex'] = $meprofile->gender;
		//$userinfo['locale'] = $meprofile->lang;
	  } else {
	      // request failed
	      echo "Error retrieving profile information:<br /><br />RESPONSE:<br /><br /><pre>" . print_r($response) . "</pre>";
  	  } 
	  return $userinfo;

}

function check_rate_limit($response) {
  $headers = $response['headers'];
  if ($headers['x_ratelimit_remaining'] == 0) :
    $reset = $headers['x_ratelimit_reset'];
    $sleep = time() - $reset;
    echo 'rate limited. reset time is ' . $reset . PHP_EOL;
    echo 'sleeping for ' . $sleep . ' seconds';
    //sleep($sleep);
  endif;
}

function user_getFriends(){
	$ids = array();
	$cursor = '-1';

	while (true) :
	  if ($cursor == '0')
	    break;
	
	  $this->oauthclient->request('GET', $this->oauthclient->url('1/friends/ids'), array(
	    'cursor' => $cursor
	  ));
	
	  // check the rate limit
	  $this->check_rate_limit($this->oauthclient->response);
	
	  if ($this->oauthclient->response['code'] == 200) {  
	    $data = json_decode($this->oauthclient->response['response'], true);
	    $ids += $data['ids'];
	    $cursor = $data['next_cursor_str'];
	  } else {
	    echo $this->oauthclient->response['response'];
	    break;
	  }
	endwhile;
	return $ids;
}

function createAuthUrl(){
	$response = $this->oauthclient->retrieveTokenRequest();

	if($response['success'] === TRUE) {	
	    $_SESSION['oauth']['linkedin']['request'] = $response['linkedin'];
	    $authurl = linkedin::_URL_AUTH .$response['linkedin']['oauth_token'];
	} else {
	    echo "Request token retrieval failed:<br /><br />RESPONSE:<br /><br /><pre>" . print_r($response, TRUE) . "</pre><br /><br />linkedin OBJ:<br /><br /><pre>" . print_r($OBJ_linkedin, TRUE) . "</pre>";
	}
	return $authurl; 
}

function wp_login() {
		global $wp_version,$new_fb_user;

		if ( isset($_REQUEST["fbconnect_action"]) && ($_REQUEST["fbconnect_action"]=="delete_user" || $_REQUEST["fbconnect_action"]=="postlogout" || $_REQUEST["fbconnect_action"]=="logout")){
			return;
		}
		
		$self = basename( $GLOBALS['pagenow'] );
	
			
		$user = wp_get_current_user();
		if (isset($user) && $user->ID==0){
			$user = "";	
		}

		if ( isset($_GET['oauth_login']) && $_GET['oauth_login']=="linkedin" ) { //Intenta hacer login estando registrado en facebook
			if (isset($_GET['oauth_verifier'])) {
		
			  	$response = $this->oauthclient->retrieveTokenAccess($_SESSION['oauth']['linkedin']['request']['oauth_token'], $_SESSION['oauth']['linkedin']['request']['oauth_token_secret'], $_GET['oauth_verifier']);

        		if($response['success'] === TRUE) {
			    	// the request went through without an error, gather user's 'access' tokens
          			$_SESSION['oauth']['linkedin']['access'] = $response['linkedin'];
          
          			// set the user as authorized for future quick reference
          			$_SESSION['oauth']['linkedin']['authorized'] = TRUE;
			  	} else {
			  		unset($_SESSION['oauth']);
			   		 echo "Access token retrieval failed:<br /><br />RESPONSE:<br /><br /><pre>" . print_r($response, TRUE) . "</pre><br /><br />linkedin OBJ:<br /><br /><pre>" . print_r($OBJ_linkedin, TRUE) . "</pre>";
			  	}
			}

			if ($_SESSION['oauth']['linkedin']['authorized']) {
			  	$this->oauthclient->setTokenAccess($_SESSION['oauth']['linkedin']['access']);
				
				require_once(ABSPATH . WPINC . '/registration.php');

				$usersinfo = $this->user_getInfo();

				if (!isset($usersinfo) || $usersinfo["uid"]==""){
					WPfbConnect::log("[fbConnectLogic::wp_login_fbconnect] fb_user_getInfo ERROR: ".$fb_user,FBCONNECT_LOG_ERR);
					return;	
				}
				$fb_user = $usersinfo["uid"];
				$_SESSION["facebook_usersinfo"] = $usersinfo;
				$_SESSION["fbconnect_netid"] = "linkedin";
				
				$wpid = "";
				$fbwpuser = WPfbConnect_Logic::get_userbyFBID($usersinfo["uid"],"linkedin");
				if ($fbwpuser =="" && $usersinfo["email"]!=""){
					$fbwpuser = WPfbConnect_Logic::get_userbyEmail($usersinfo["email"]);
				}
				//echo "LEER:".$fb_user;
				//print_r($fbwpuser);
				$wpid = "";
				$new_fb_user= false;
		
				if(is_user_logged_in() && $fbwpuser && $user->ID==$fbwpuser->ID && ($user->fbconnect_userid =="" || $user->fbconnect_userid =="0")){ // Encuentra por email el usuario y no está asociado al de FB
					WPfbConnect_Logic::set_userid_fbconnect($user->ID,$fb_user,"linkedin");
					$wpid = $user->ID;
				}else if(FBCONNECT_CANVAS!="web" && is_user_logged_in() && $fbwpuser && $user->ID != $fbwpuser->ID){ // El usuario FB está asociado a un usaurio WP distinto al logeado
					$wpid = $fbwpuser->ID;
				}elseif(is_user_logged_in() && !$fbwpuser && ($user->fbconnect_userid =="" || $user->fbconnect_userid =="0")){ // El usuario WP no está asociado al de FB
					WPfbConnect_Logic::set_userid_fbconnect($user->ID,$fb_user,"linkedin");
					$wpid = $user->ID;
				}elseif (!is_user_logged_in() && $fbwpuser && ($fbwpuser->fbconnect_userid =="" || $fbwpuser->fbconnect_userid =="0")){
					WPfbConnect_Logic::set_userid_fbconnect($fbwpuser->ID,$fb_user,"linkedin");
					$wpid = $fbwpuser->ID;	
				}elseif(!is_user_logged_in() && $fbwpuser && ($fbwpuser->fbconnect_userid ==$fb_user)){
					$wpid = $fbwpuser->ID;	
				}elseif (($fbwpuser && $fbwpuser->fbconnect_userid != $fb_user) || (!is_user_logged_in() && !$fbwpuser) || (!$fbwpuser && is_user_logged_in() && $user->fbconnect_userid != $fb_user)){
					if(isset($usersinfo) && $usersinfo!=""){
						$username = WPfbConnect_Logic::fbusername_generator($usersinfo["uid"],"",$usersinfo["first_name"],$usersinfo["last_name"],"LIN");

						$user_data = array();
						$user_data['user_login'] = $username;
						
						$user_data['user_pass'] = substr( md5( uniqid( microtime() ).$_SERVER["REMOTE_ADDR"] ), 0, 15);
	
						$user_data['user_nicename'] = $usersinfo["name"];
						$user_data['display_name'] = $usersinfo["name"];
	
						$user_data['user_url'] = $usersinfo["profile_url"];

						$user_data['user_email'] = "";
						
						if ($usersinfo["email"]!=""){
							$user_data['user_email'] = $usersinfo["email"];
						}
						
						define ( 'WP_IMPORTING', true);
						
						$wpid = wp_insert_user($user_data);
						
						if ( !is_wp_error($wpid) ) {
							update_usermeta( $wpid, "first_name", $usersinfo["first_name"] );
							update_usermeta( $wpid, "last_name", $usersinfo["last_name"] );
							update_usermeta( $wpid, "linkedin_id", $usersinfo["id"] );
							update_usermeta( $wpid, "thumbnail", $usersinfo["pictureUrl"] );
							
							WPfbConnect_Logic::set_userid_fbconnect($wpid,$fb_user,"linkedin");
							$new_fb_user= true;
						}else{ // no ha podido insertar el usuario
							return;
						}
					}
					
				}else{
					return;
				}
				$userdata = WPfbConnect_Logic::get_userbyFBID($fb_user,"linkedin");
				WPfbConnect_Logic::set_lastlogin_fbconnect($userdata->ID);
				global $current_user;
	
				$current_user = null;
				
	
				WPfbConnect_Logic::fb_set_current_user($userdata);
				
				//Store user token
				if (fb_get_option('fb_storeUserAcessToken')!="" ){
					$lintoken = serialize($_SESSION['oauth']['linkedin']['access']);
					WPfbConnect_Logic::set_useroffline($userdata->ID,$lintoken,1);
				}
								
				global $userdata;
				if (isset($userdata) && $userdata!=""){
					$userdata->fbconnect_userid = $fb_user;
					$userdata->fbconnect_netid = "linkedin";
				}
				$siteUrl= fb_get_option('siteurl');
				$urlthick = WPfbConnect_Logic::add_urlParam($siteUrl,"fbconnect_action=register&height=400&width=370");	
				
				header('Content-type: text/html');
				if ($new_fb_user){
					$new_fb_usertxt = "true";
				}else{
					$new_fb_usertxt = "false";
				}
				$terms = get_user_meta($userdata->ID, "terms", true);
				?>
				<html>
				<body>
				<script> 
					//window.opener.fb_registeruserthick();
					window.opener.fb_refreshlogininfo('linkedin','<?php echo $fb_user;?>','<?php echo $userdata->ID;?>',<?php echo $new_fb_usertxt;?>,'<?php echo $terms;?>');
					window.close();
				</script>
				</body>
				</html>
				<?php
				exit;
				//Cache friends
				//$this->user_getFriends();
				/*WPfbConnect_Logic::get_connected_friends();
				if (fb_get_option('fb_permsToRequestOnConnect')!="" ){
					if (strrpos(fb_get_option('fb_permsToRequestOnConnect'),"offline_access")===false){
						//Not found
					}elseif($userdata!=""){
						$token = fb_get_access_token();
						//update_usermeta( $userdata->ID, "access_token", $token );
						WPfbConnect_Logic::set_useroffline($userdata->ID,$token,1);
						
					}
				}*/
				
			}
		}
	}
	
	function outputError($oauth) {
	  echo 'Error: ' . $this->oauthclient->response['response'] . PHP_EOL;
	  tmhUtilities::pr($this->oauthclient);
	}
} 
endif; // end if-class-exists test

global $linLogic;
$linLogic = new WPlinConnect_Logic;
$linLogic->init();
?>